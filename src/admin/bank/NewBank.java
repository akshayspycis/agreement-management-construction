/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package admin.bank;

import datamanager.BankDetails;
import datamanager.Config;
import datamanager.CustomerProfile;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

/**
 *
 * @author akki
 */
public class NewBank extends javax.swing.JDialog {


	public NewBank(java.awt.Frame parent, boolean modal) {
		super(parent, modal);
		initComponents();
                setIconImage(Config.configmgr.getLogo());
		this.setLocationRelativeTo(null);
		// Close the dialog when Esc is pressed
		String cancelName = "cancel";
		InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
		ActionMap actionMap = getRootPane().getActionMap();
		actionMap.put(cancelName, new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
	}

	@SuppressWarnings("unchecked")
 // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
 private void initComponents() {

  btn_save = new javax.swing.JButton();
  btn_cancel = new javax.swing.JButton();
  jPanel1 = new javax.swing.JPanel();
  jPanel2 = new javax.swing.JPanel();
  jLabel1 = new javax.swing.JLabel();
  txt_name = new javax.swing.JTextField();
  jLabel2 = new javax.swing.JLabel();
  txt_address = new javax.swing.JTextField();
  jLabel3 = new javax.swing.JLabel();
  txt_locality = new javax.swing.JTextField();
  jLabel4 = new javax.swing.JLabel();
  txt_city = new javax.swing.JTextField();
  jLabel5 = new javax.swing.JLabel();
  txt_pincode = new javax.swing.JTextField();
  jLabel6 = new javax.swing.JLabel();
  cb_state = new javax.swing.JComboBox();
  btn_reset = new javax.swing.JButton();

  setTitle("New Bank Detail");
  addWindowListener(new java.awt.event.WindowAdapter() {
   public void windowClosing(java.awt.event.WindowEvent evt) {
    closeDialog(evt);
   }
  });

  btn_save.setText("Save");
  btn_save.addActionListener(new java.awt.event.ActionListener() {
   public void actionPerformed(java.awt.event.ActionEvent evt) {
    btn_saveActionPerformed(evt);
   }
  });

  btn_cancel.setText("Cancel");
  btn_cancel.addActionListener(new java.awt.event.ActionListener() {
   public void actionPerformed(java.awt.event.ActionEvent evt) {
    btn_cancelActionPerformed(evt);
   }
  });

  jPanel1.setBackground(new java.awt.Color(255, 255, 255));

  jPanel2.setBackground(new java.awt.Color(255, 255, 255));
  jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Bank Details"));

  jLabel1.setText("Name :");

  txt_name.addActionListener(new java.awt.event.ActionListener() {
   public void actionPerformed(java.awt.event.ActionEvent evt) {
    txt_nameActionPerformed(evt);
   }
  });

  jLabel2.setText("Address :");

  txt_address.addActionListener(new java.awt.event.ActionListener() {
   public void actionPerformed(java.awt.event.ActionEvent evt) {
    txt_addressActionPerformed(evt);
   }
  });

  jLabel3.setText("Locality :");

  jLabel4.setText("City :");

  jLabel5.setText("Pincode :");

  jLabel6.setText("State :");

  cb_state.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "- State -", "Andaman and Nicobar Islands", "Andhra Pradesh", "Arunachal Pradesh", "Assam", "Chandigarh", "Chhattisgarh", "Dadra and Nagar Haveli", "Daman and Diu", "Delhi", "Goa", "Gujarat", "Haryana", "Himachal Pradesh", "Jammu and Kashmir", "Jharkhand", "Karnataka", "Kerala", "Lakshadweep", "Madhya Pradesh", "Maharashtra", "Manipur", "Meghalaya", "Mizoram", "Nagaland", "Orissa", "Puduchery", "Punjab", "Rajasthan", "Sikkim", "Tamil Nadu", "Tripura", "Uttar Pradesh", "Uttaranchal", "West Bengal" }));

  javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
  jPanel2.setLayout(jPanel2Layout);
  jPanel2Layout.setHorizontalGroup(
   jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addGroup(jPanel2Layout.createSequentialGroup()
    .addContainerGap()
    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
     .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
      .addComponent(jLabel4)
      .addComponent(jLabel3)
      .addComponent(jLabel2)
      .addComponent(jLabel1))
     .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
      .addComponent(jLabel6)
      .addComponent(jLabel5)))
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
     .addComponent(txt_pincode, javax.swing.GroupLayout.DEFAULT_SIZE, 219, Short.MAX_VALUE)
     .addComponent(txt_address, javax.swing.GroupLayout.Alignment.LEADING)
     .addComponent(txt_locality, javax.swing.GroupLayout.Alignment.LEADING)
     .addComponent(txt_city, javax.swing.GroupLayout.Alignment.LEADING)
     .addComponent(txt_name)
     .addComponent(cb_state, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
  );
  jPanel2Layout.setVerticalGroup(
   jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addGroup(jPanel2Layout.createSequentialGroup()
    .addGap(17, 17, 17)
    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
     .addComponent(jLabel1)
     .addComponent(txt_name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
    .addGap(18, 18, 18)
    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
     .addComponent(jLabel2)
     .addComponent(txt_address, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
    .addGap(18, 18, 18)
    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
     .addComponent(jLabel3)
     .addComponent(txt_locality, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
    .addGap(18, 18, 18)
    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
     .addComponent(jLabel4)
     .addComponent(txt_city, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
    .addGap(18, 18, 18)
    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
     .addComponent(jLabel5)
     .addComponent(txt_pincode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
    .addGap(18, 18, 18)
    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
     .addComponent(jLabel6)
     .addComponent(cb_state, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
    .addContainerGap(14, Short.MAX_VALUE))
  );

  javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
  jPanel1.setLayout(jPanel1Layout);
  jPanel1Layout.setHorizontalGroup(
   jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addGroup(jPanel1Layout.createSequentialGroup()
    .addContainerGap()
    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
  );
  jPanel1Layout.setVerticalGroup(
   jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addGroup(jPanel1Layout.createSequentialGroup()
    .addContainerGap()
    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
  );

  btn_reset.setText("Reset");
  btn_reset.addActionListener(new java.awt.event.ActionListener() {
   public void actionPerformed(java.awt.event.ActionEvent evt) {
    btn_resetActionPerformed(evt);
   }
  });

  javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
  getContentPane().setLayout(layout);
  layout.setHorizontalGroup(
   layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
    .addContainerGap()
    .addComponent(btn_reset)
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
    .addComponent(btn_save, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
    .addComponent(btn_cancel)
    .addContainerGap())
   .addGroup(layout.createSequentialGroup()
    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
    .addGap(0, 0, Short.MAX_VALUE))
  );

  layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_cancel, btn_save});

  layout.setVerticalGroup(
   layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
     .addComponent(btn_cancel)
     .addComponent(btn_save)
     .addComponent(btn_reset))
    .addContainerGap())
  );

  getRootPane().setDefaultButton(btn_save);

  pack();
 }// </editor-fold>//GEN-END:initComponents

	/**
	 * Closes the dialog
	 */
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
					dispose();
    }//GEN-LAST:event_closeDialog

 private void btn_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelActionPerformed
  dispose();
 }//GEN-LAST:event_btn_cancelActionPerformed

 private void btn_saveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_saveActionPerformed
          String str = checkValidity();
        
        if (str.equals("ok")) {        
            BankDetails cp = new BankDetails();        
            
            cp.setBank_name(txt_name.getText());
            cp.setAddress(txt_address.getText());
            cp.setLocality(txt_locality.getText());
            cp.setCity(txt_city.getText());
            cp.setPincode(txt_pincode.getText());
            cp.setState(cb_state.getSelectedItem().toString());        

            if (Config.bankdetailsmgr.insBankDetails(cp)) {
                Config.productconfiguration.onloadResetBank();
                onloadReset();
                JOptionPane.showMessageDialog(this, "Bank profile created successfully.", "Creation successful", JOptionPane.NO_OPTION);                
            } else {
                JOptionPane.showMessageDialog(this, "Error in profile creation.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(this, str, "Error", JOptionPane.ERROR_MESSAGE);
        }       
 }//GEN-LAST:event_btn_saveActionPerformed

 private void txt_nameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_nameActionPerformed
  // TODO add your handling code here:
 }//GEN-LAST:event_txt_nameActionPerformed

 private void txt_addressActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_addressActionPerformed
  // TODO add your handling code here:
 }//GEN-LAST:event_txt_addressActionPerformed

 private void btn_resetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_resetActionPerformed
  onloadReset();
		
 }//GEN-LAST:event_btn_resetActionPerformed
	
 // Variables declaration - do not modify//GEN-BEGIN:variables
 private javax.swing.JButton btn_cancel;
 private javax.swing.JButton btn_reset;
 private javax.swing.JButton btn_save;
 private javax.swing.JComboBox cb_state;
 private javax.swing.JLabel jLabel1;
 private javax.swing.JLabel jLabel2;
 private javax.swing.JLabel jLabel3;
 private javax.swing.JLabel jLabel4;
 private javax.swing.JLabel jLabel5;
 private javax.swing.JLabel jLabel6;
 private javax.swing.JPanel jPanel1;
 private javax.swing.JPanel jPanel2;
 private javax.swing.JTextField txt_address;
 private javax.swing.JTextField txt_city;
 private javax.swing.JTextField txt_locality;
 private javax.swing.JTextField txt_name;
 private javax.swing.JTextField txt_pincode;
 // End of variables declaration//GEN-END:variables

	public void onloadReset() {
        txt_name.setText(null);
        txt_address.setText(null);
        txt_locality.setText(null);
        txt_city.setText(null);
        txt_pincode.setText(null);
        cb_state.setSelectedIndex(0);
    }

    private String checkValidity() {
        if (txt_name.getText().equals("")) {
            return "Field 'Name' can not be blank.";
        }
        else {
            return "ok";
        }
    }
    
}
