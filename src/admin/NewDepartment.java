/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package admin;

import datamanager.Config;
import datamanager.Department;
import javax.swing.JOptionPane;

/**
 *
 * @author akshay
 */
public class NewDepartment extends javax.swing.JDialog {

     boolean b=true;
    
    public NewDepartment(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        setIconImage(Config.configmgr.getLogo());
        initComponents();
         this.setLocationRelativeTo(null);        
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTextField1 = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txt_department = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txt_abbreviation = new javax.swing.JTextField();
        btn_cancel = new javax.swing.JButton();
        btn_save = new javax.swing.JButton();
        txt_reset = new javax.swing.JButton();

        jTextField1.setText("jTextField1");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("New Department");
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Department Entry", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel1.setText("Department :");

        txt_department.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_departmentCaretUpdate(evt);
            }
        });

        jLabel2.setText("Abbreviation :");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txt_abbreviation, javax.swing.GroupLayout.DEFAULT_SIZE, 408, Short.MAX_VALUE)
                    .addComponent(txt_department))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txt_department, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txt_abbreviation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(19, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btn_cancel.setText("Cancel");
        btn_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelActionPerformed(evt);
            }
        });

        btn_save.setText("Save");
        btn_save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_saveActionPerformed(evt);
            }
        });

        txt_reset.setText("Reset");
        txt_reset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_resetActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txt_reset)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btn_save)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_cancel)
                .addContainerGap())
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_cancel, btn_save, txt_reset});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_cancel)
                    .addComponent(btn_save)
                    .addComponent(txt_reset))
                .addGap(0, 10, Short.MAX_VALUE))
        );

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_cancel, btn_save, txt_reset});

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelActionPerformed
     dispose();
    }//GEN-LAST:event_btn_cancelActionPerformed

    private void txt_resetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_resetActionPerformed
     onloadReset();
    }//GEN-LAST:event_txt_resetActionPerformed

    private void btn_saveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_saveActionPerformed
     String str =checkValidation();
        if (str.equals("ok")) {
        if(b)
                {
                Department d=new Department();    
                d.setDepartmentname(txt_department.getText().toUpperCase());
                d.setAbbreviation(txt_abbreviation.getText());
                if (Config.departmentmgr.insDepartment(d)) {
                Config.productconfiguration.onloadReset();
                Config.adminhome.onloadReset();
                JOptionPane.showMessageDialog(this, "Department Add successfully.", "Creation successful", JOptionPane.NO_OPTION);
                } else {
                JOptionPane.showMessageDialog(this, "Error in Department Add.", "Error", JOptionPane.ERROR_MESSAGE);
                }   
            }else{
            JOptionPane.showMessageDialog(this, "Value already exits.", "Error", JOptionPane.ERROR_MESSAGE);
        }    
        } else {
             JOptionPane.showMessageDialog(this, "Department Name should not be blank.", "Error", JOptionPane.ERROR_MESSAGE);
        }
        
        
    }//GEN-LAST:event_btn_saveActionPerformed

    private void txt_departmentCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_departmentCaretUpdate
        b=true;
        txt_abbreviation.setText("");
        if(!txt_department.getText().equals(""))
          {
              String initial = "";
               int count=0;
               String[] split = txt_department.getText().split(" ");
              
               for(String value : split){
               initial += value.substring(0,1);
               }
                
                for(int i=0 ;i< Config.configdepartment.size() ;i++){
                Department d=Config.configdepartment.get(i);    
                if(txt_department.getText().equalsIgnoreCase(d.getDepartmentname())){
                b =false;
                }
                String str=d.getAbbreviation();
                try{
                        
                if(initial.toUpperCase().equals(str.substring(0,str.indexOf('(')))){
                    count++;
                }
                }catch(Exception e){
                   if(initial.toUpperCase().equals(str.substring(0,str.length()))){
                    count++;
                }
                }
                }
                if(count>0){
                 initial =initial+ "("+count+")";
                 txt_abbreviation.setText( initial.toUpperCase());
                }
              txt_abbreviation.setText( initial.toUpperCase());
          }
    }//GEN-LAST:event_txt_departmentCaretUpdate

   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_cancel;
    private javax.swing.JButton btn_save;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField txt_abbreviation;
    private javax.swing.JTextField txt_department;
    private javax.swing.JButton txt_reset;
    // End of variables declaration//GEN-END:variables

   public void onloadReset() {
        txt_department.setText("");
        txt_abbreviation.setText("");
    }

    private String checkValidation() {
        if(txt_department.getText().equals("")){
        return "department";    
        }else{
         return "ok";   
        }
    }
}
