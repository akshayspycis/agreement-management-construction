package admin;

import com.toedter.calendar.IDateEditor;
import datamanager.Config;
import datamanager.Department;
import datamanager.Division;
import datamanager.TenderEntry;
import java.awt.Color;
import java.awt.Component;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.print.PageFormat;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.text.DefaultEditorKit;

/**
 *
 * @author AMS
 */
public class AdminHome extends javax.swing.JFrame {
	  String departmentid="";	
           String divisionid="";	

    Calendar currentDate; 
    /**
     * Creates new form AdminHome
     */
   SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
   DefaultTableModel tbl_tendermodel;
    public AdminHome() {
      setIconImage(Config.configmgr.getLogo());
        initComponents();
        tbl_tendermodel = (DefaultTableModel) tbl_tender.getModel();
        
        this.setLocationRelativeTo(null);
        setExtendedState(MAXIMIZED_BOTH);        
        //............for date chooser...............
                
        
        }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jToolBar1 = new javax.swing.JToolBar();
        btn_newtender = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JToolBar.Separator();
        btn_agreement1 = new javax.swing.JButton();
        jSeparator14 = new javax.swing.JToolBar.Separator();
        btn_viewtender = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JToolBar.Separator();
        btn_report = new javax.swing.JButton();
        jSeparator7 = new javax.swing.JToolBar.Separator();
        txt_delete = new javax.swing.JButton();
        jSeparator4 = new javax.swing.JToolBar.Separator();
        txt_print = new javax.swing.JButton();
        jSeparator5 = new javax.swing.JToolBar.Separator();
        txt_close = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        btn_Search = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        chb_date = new javax.swing.JCheckBox();
        jLabel6 = new javax.swing.JLabel();
        jdc_fromDate = new com.toedter.calendar.JDateChooser();
        jLabel7 = new javax.swing.JLabel();
        jdc_toDate = new com.toedter.calendar.JDateChooser();
        jLabel13 = new javax.swing.JLabel();
        txt_department = new javax.swing.JTextField();
        txt_division = new javax.swing.JTextField();
        jSeparator8 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        txt_nit_no = new javax.swing.JTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbl_tender = new javax.swing.JTable();
        btn_view = new javax.swing.JButton();
        btn_delete = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        File = new javax.swing.JMenu();
        jSeparator11 = new javax.swing.JPopupMenu.Separator();
        jSeparator6 = new javax.swing.JPopupMenu.Separator();
        btn_Close = new javax.swing.JMenuItem();
        Manage = new javax.swing.JMenu();
        jSeparator10 = new javax.swing.JPopupMenu.Separator();
        jSeparator12 = new javax.swing.JPopupMenu.Separator();
        btn_CustomerProfile = new javax.swing.JMenuItem();
        jSeparator13 = new javax.swing.JPopupMenu.Separator();
        btn_Expenditure = new javax.swing.JMenuItem();
        Configure = new javax.swing.JMenu();
        btn_PrimarySetting = new javax.swing.JMenuItem();
        Utilities = new javax.swing.JMenu();
        jMenu7 = new javax.swing.JMenu();
        btn_ExportFile = new javax.swing.JMenuItem();
        btn_ImportFile = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        jCheckBoxMenuItem1 = new javax.swing.JCheckBoxMenuItem();
        Help = new javax.swing.JMenu();
        btn_ManageAccount = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        btn_HelpContent = new javax.swing.JMenuItem();
        btn_ProductLicense = new javax.swing.JMenuItem();
        jSeparator9 = new javax.swing.JPopupMenu.Separator();
        btn_CompanyProfile = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Shapers Construction - Administrator Home [INFOPARK INNOVATIONS]");

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jPanel3.setBackground(new java.awt.Color(58, 148, 175));

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Welcome, Administrator");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addContainerGap(1191, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/header.png"))); // NOI18N
        jLabel5.setAlignmentY(0.0F);

        jToolBar1.setBackground(new java.awt.Color(255, 255, 255));
        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        btn_newtender.setBackground(new java.awt.Color(255, 255, 255));
        btn_newtender.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_newtender.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/customerentry.jpg"))); // NOI18N
        btn_newtender.setText("New Tender");
        btn_newtender.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btn_newtender.setFocusable(false);
        btn_newtender.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_newtender.setMargin(new java.awt.Insets(0, 8, 0, 8));
        btn_newtender.setPreferredSize(new java.awt.Dimension(107, 55));
        btn_newtender.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btn_newtender.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_newtenderActionPerformed(evt);
            }
        });
        jToolBar1.add(btn_newtender);
        jToolBar1.add(jSeparator2);

        btn_agreement1.setBackground(new java.awt.Color(255, 255, 255));
        btn_agreement1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_agreement1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/agreement.png"))); // NOI18N
        btn_agreement1.setText("Agreement");
        btn_agreement1.setFocusable(false);
        btn_agreement1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_agreement1.setMargin(new java.awt.Insets(0, 8, 0, 8));
        btn_agreement1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btn_agreement1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_agreement1ActionPerformed(evt);
            }
        });
        jToolBar1.add(btn_agreement1);
        jToolBar1.add(jSeparator14);

        btn_viewtender.setBackground(new java.awt.Color(255, 255, 255));
        btn_viewtender.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_viewtender.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/viewprofile.jpg"))); // NOI18N
        btn_viewtender.setText("View Tender");
        btn_viewtender.setFocusable(false);
        btn_viewtender.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_viewtender.setMargin(new java.awt.Insets(0, 8, 0, 8));
        btn_viewtender.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btn_viewtender.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_viewtenderActionPerformed(evt);
            }
        });
        jToolBar1.add(btn_viewtender);
        jToolBar1.add(jSeparator1);

        btn_report.setBackground(new java.awt.Color(255, 255, 255));
        btn_report.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_report.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/save.png"))); // NOI18N
        btn_report.setText("Report");
        btn_report.setFocusable(false);
        btn_report.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_report.setMargin(new java.awt.Insets(0, 8, 0, 8));
        btn_report.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btn_report.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_reportActionPerformed(evt);
            }
        });
        jToolBar1.add(btn_report);
        jToolBar1.add(jSeparator7);

        txt_delete.setBackground(new java.awt.Color(255, 255, 255));
        txt_delete.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txt_delete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/agreement_managemen.png"))); // NOI18N
        txt_delete.setText("Agreement Management");
        txt_delete.setFocusable(false);
        txt_delete.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        txt_delete.setMargin(new java.awt.Insets(0, 8, 0, 8));
        txt_delete.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        txt_delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_deleteActionPerformed(evt);
            }
        });
        jToolBar1.add(txt_delete);
        jToolBar1.add(jSeparator4);

        txt_print.setBackground(new java.awt.Color(255, 255, 255));
        txt_print.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txt_print.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/print.png"))); // NOI18N
        txt_print.setText("Print");
        txt_print.setFocusable(false);
        txt_print.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        txt_print.setMargin(new java.awt.Insets(0, 8, 0, 8));
        txt_print.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        txt_print.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_printActionPerformed(evt);
            }
        });
        jToolBar1.add(txt_print);
        jToolBar1.add(jSeparator5);

        txt_close.setBackground(new java.awt.Color(255, 255, 255));
        txt_close.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txt_close.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/exit.png"))); // NOI18N
        txt_close.setText("Close");
        txt_close.setFocusable(false);
        txt_close.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        txt_close.setMargin(new java.awt.Insets(0, 8, 0, 8));
        txt_close.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        txt_close.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_closeActionPerformed(evt);
            }
        });
        jToolBar1.add(txt_close);

        jPanel7.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jPanel7KeyPressed(evt);
            }
        });

        btn_Search.setText("Search");
        btn_Search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SearchActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(51, 51, 51));
        jLabel4.setText("Department :");

        jLabel6.setText("From :");

        jdc_fromDate.setDateFormatString("dd-MM-yyyy");
        jdc_fromDate.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jdc_fromDateFocusLost(evt);
            }
        });
        jdc_fromDate.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jdc_fromDatePropertyChange(evt);
            }
        });
        jdc_fromDate.addVetoableChangeListener(new java.beans.VetoableChangeListener() {
            public void vetoableChange(java.beans.PropertyChangeEvent evt)throws java.beans.PropertyVetoException {
                jdc_fromDateVetoableChange(evt);
            }
        });

        jLabel7.setText("To :");

        jdc_toDate.setDateFormatString("dd-MM-yyyy");

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(51, 51, 51));
        jLabel13.setText("Division :");

        txt_department.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_departmentActionPerformed(evt);
            }
        });
        txt_department.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_departmentKeyPressed(evt);
            }
        });

        txt_division.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_divisionCaretUpdate(evt);
            }
        });
        txt_division.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_divisionActionPerformed(evt);
            }
        });
        txt_division.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_divisionKeyPressed(evt);
            }
        });

        jSeparator8.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Nit No :");

        txt_nit_no.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_nit_noCaretUpdate(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txt_department)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel13)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txt_division, javax.swing.GroupLayout.PREFERRED_SIZE, 249, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txt_nit_no, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(chb_date)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jdc_fromDate, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jdc_toDate, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_Search)
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jSeparator8, javax.swing.GroupLayout.DEFAULT_SIZE, 24, Short.MAX_VALUE)
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(btn_Search, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(txt_department, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel13)
                            .addComponent(txt_division, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(jdc_fromDate, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jdc_toDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(chb_date, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txt_nit_no)
                                .addComponent(jLabel2)))))
                .addGap(6, 6, 6))
        );

        jPanel7Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_Search, chb_date, jLabel13, jLabel4, jLabel6, jLabel7, jdc_fromDate, jdc_toDate, txt_department, txt_division});

        tbl_tender.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbl_tender.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "NIT NO", "NIT DATE", "NAME OF WORK", "PAC AMOUNT", "EMRS.", "FDR NO.", "NAME OF BANK", "DATE", "REMARK", ""
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_tender.setRowHeight(20);
        tbl_tender.getTableHeader().setReorderingAllowed(false);
        tbl_tender.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_tenderMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tbl_tender);
        if (tbl_tender.getColumnModel().getColumnCount() > 0) {
            tbl_tender.getColumnModel().getColumn(0).setResizable(false);
            tbl_tender.getColumnModel().getColumn(0).setPreferredWidth(50);
            tbl_tender.getColumnModel().getColumn(0).setCellRenderer(new CustomTableCellRenderer());
            tbl_tender.getColumnModel().getColumn(1).setResizable(false);
            tbl_tender.getColumnModel().getColumn(1).setPreferredWidth(50);
            tbl_tender.getColumnModel().getColumn(1).setCellRenderer(new CustomTableCellRenderer());
            tbl_tender.getColumnModel().getColumn(2).setResizable(false);
            tbl_tender.getColumnModel().getColumn(2).setPreferredWidth(200);
            tbl_tender.getColumnModel().getColumn(2).setCellRenderer(new CustomTableCellRenderer());
            tbl_tender.getColumnModel().getColumn(3).setResizable(false);
            tbl_tender.getColumnModel().getColumn(3).setPreferredWidth(50);
            tbl_tender.getColumnModel().getColumn(3).setCellRenderer(new CustomTableCellRenderer());
            tbl_tender.getColumnModel().getColumn(4).setResizable(false);
            tbl_tender.getColumnModel().getColumn(4).setPreferredWidth(150);
            tbl_tender.getColumnModel().getColumn(4).setCellRenderer(new CustomTableCellRenderer());
            tbl_tender.getColumnModel().getColumn(5).setResizable(false);
            tbl_tender.getColumnModel().getColumn(5).setCellRenderer(new CustomTableCellRenderer()

            );
            tbl_tender.getColumnModel().getColumn(6).setResizable(false);
            tbl_tender.getColumnModel().getColumn(6).setCellRenderer(new CustomTableCellRenderer()

            );
            tbl_tender.getColumnModel().getColumn(7).setResizable(false);
            tbl_tender.getColumnModel().getColumn(7).setCellRenderer(new CustomTableCellRenderer()

            );
            tbl_tender.getColumnModel().getColumn(8).setResizable(false);
            tbl_tender.getColumnModel().getColumn(8).setPreferredWidth(100);
            tbl_tender.getColumnModel().getColumn(8).setCellRenderer(new CustomTableCellRenderer());
            tbl_tender.getColumnModel().getColumn(9).setMinWidth(0);
            tbl_tender.getColumnModel().getColumn(9).setPreferredWidth(0);
            tbl_tender.getColumnModel().getColumn(9).setMaxWidth(0);
        }
        popupMenu = new JPopupMenu();

        JMenuItem viewtender = new JMenuItem("View Tender");
        JMenuItem newagreement = new JMenuItem("New Agreement");
        JMenuItem viewagreement = new JMenuItem("View Agreement");
        JMenuItem cut = new JMenuItem("Cut");
        JMenuItem copy = new JMenuItem("Copy");
        JMenuItem delete = new JMenuItem("Delete");

        popupMenu.add(viewtender);
        popupMenu.add(newagreement);
        popupMenu.add(viewagreement);
        popupMenu.add(new javax.swing.JPopupMenu.Separator());
        popupMenu.add(cut);
        popupMenu.add(copy);
        popupMenu.add(delete);
        tbl_tender.setComponentPopupMenu(popupMenu);

        viewtender.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewtenderActionPerformed(evt);
            }
        });
        viewagreement.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewagreementActionPerformed(evt);
            }
        });
        cut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cutActionPerformed(evt);
            }
        });
        copy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                copyActionPerformed(evt);
            }
        });
        delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteActionPerformed(evt);
            }
        });
        newagreement.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newagreementActionPerformed(evt);
            }
        });

        btn_view.setText("View");
        btn_view.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_viewActionPerformed(evt);
            }
        });

        btn_delete.setText("Delete");
        btn_delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_deleteActionPerformed(evt);
            }
        });

        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(102, 102, 102));
        jLabel11.setText("Copyright © 2013, All rights reserved.");

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(51, 51, 51));
        jLabel12.setText("|    Contact : +91 - 9039788847, +91 - 7566092796, +91 - 9685004050");

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/companyname.jpg"))); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel11)
                .addGap(18, 18, 18)
                .addComponent(jLabel12)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel11)
                        .addComponent(jLabel12))
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel1, jLabel11, jLabel12});

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, 884, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5))
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btn_delete)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_view)))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_delete, btn_view});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 472, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn_view, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btn_delete, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_delete, btn_view});

        File.setMnemonic('F');
        File.setText("File");
        File.add(jSeparator11);
        File.add(jSeparator6);

        btn_Close.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F4, java.awt.event.InputEvent.ALT_MASK));
        btn_Close.setMnemonic('e');
        btn_Close.setText("Close");
        btn_Close.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_CloseActionPerformed(evt);
            }
        });
        File.add(btn_Close);

        jMenuBar1.add(File);

        Manage.setMnemonic('M');
        Manage.setText("Manage");
        Manage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ManageActionPerformed(evt);
            }
        });
        Manage.add(jSeparator10);
        Manage.add(jSeparator12);

        btn_CustomerProfile.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_U, java.awt.event.InputEvent.CTRL_MASK));
        btn_CustomerProfile.setMnemonic('u');
        btn_CustomerProfile.setText("Party Profiles");
        btn_CustomerProfile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_CustomerProfileActionPerformed(evt);
            }
        });
        Manage.add(btn_CustomerProfile);
        Manage.add(jSeparator13);

        btn_Expenditure.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.CTRL_MASK));
        btn_Expenditure.setMnemonic('E');
        btn_Expenditure.setText("Report Management");
        btn_Expenditure.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ExpenditureActionPerformed(evt);
            }
        });
        Manage.add(btn_Expenditure);

        jMenuBar1.add(Manage);

        Configure.setMnemonic('C');
        Configure.setText("Configure");

        btn_PrimarySetting.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_MASK));
        btn_PrimarySetting.setMnemonic('r');
        btn_PrimarySetting.setText("Product Configuration");
        btn_PrimarySetting.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_PrimarySettingActionPerformed(evt);
            }
        });
        Configure.add(btn_PrimarySetting);

        jMenuBar1.add(Configure);

        Utilities.setMnemonic('U');
        Utilities.setText("Utilities");
        Utilities.setEnabled(false);

        jMenu7.setMnemonic('u');
        jMenu7.setText("Backup");

        btn_ExportFile.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_L, java.awt.event.InputEvent.CTRL_MASK));
        btn_ExportFile.setMnemonic('l');
        btn_ExportFile.setText("Export File");
        btn_ExportFile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ExportFileActionPerformed(evt);
            }
        });
        jMenu7.add(btn_ExportFile);

        btn_ImportFile.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_I, java.awt.event.InputEvent.CTRL_MASK));
        btn_ImportFile.setMnemonic('I');
        btn_ImportFile.setText("Import File");
        btn_ImportFile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ImportFileActionPerformed(evt);
            }
        });
        jMenu7.add(btn_ImportFile);

        Utilities.add(jMenu7);

        jMenuItem1.setText("Cleanup");
        Utilities.add(jMenuItem1);

        jCheckBoxMenuItem1.setSelected(true);
        jCheckBoxMenuItem1.setText("Generate Logs");
        jCheckBoxMenuItem1.setEnabled(false);
        Utilities.add(jCheckBoxMenuItem1);

        jMenuBar1.add(Utilities);

        Help.setMnemonic('H');
        Help.setText("Help");
        Help.setEnabled(false);

        btn_ManageAccount.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        btn_ManageAccount.setMnemonic('o');
        btn_ManageAccount.setText("Manage Account");
        btn_ManageAccount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ManageAccountActionPerformed(evt);
            }
        });
        Help.add(btn_ManageAccount);
        Help.add(jSeparator3);

        btn_HelpContent.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_H, java.awt.event.InputEvent.CTRL_MASK));
        btn_HelpContent.setMnemonic('H');
        btn_HelpContent.setText("Help Contents");
        Help.add(btn_HelpContent);

        btn_ProductLicense.setMnemonic('P');
        btn_ProductLicense.setText("Product License");
        btn_ProductLicense.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ProductLicenseActionPerformed(evt);
            }
        });
        Help.add(btn_ProductLicense);
        Help.add(jSeparator9);

        btn_CompanyProfile.setMnemonic('C');
        btn_CompanyProfile.setText("Company Profile");
        Help.add(btn_CompanyProfile);

        jMenuBar1.add(Help);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        setSize(new java.awt.Dimension(1404, 742));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btn_newtenderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_newtenderActionPerformed
                Config.newtenderentry.onloadReset();
                Config.newtenderentry.setVisible(true);
    }//GEN-LAST:event_btn_newtenderActionPerformed

    private void btn_viewtenderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_viewtenderActionPerformed
        viewtenderActionPerformed(null);
    }//GEN-LAST:event_btn_viewtenderActionPerformed

    private void btn_reportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_reportActionPerformed
//Config.report_management.onloadPayment(divisionid);
        Config.report_management.onloadReport();
        Config.report_management.setVisible(true);
        
//        try {
//            Config.sql = "commit";
//            Config.pstmt = Config.conn.prepareStatement(Config.sql);
//            Config.pstmt.executeUpdate();
//        } catch (AdminHomexception ex) {
//            Logger.getLogger(Home.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }//GEN-LAST:event_btn_reportActionPerformed

    private void txt_deleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_deleteActionPerformed
        Config.agreement_management.onloadReset();
        Config.agreement_management.setVisible(true);
    }//GEN-LAST:event_txt_deleteActionPerformed

    private void txt_printActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_printActionPerformed
//        PrinterJob job = PrinterJob.getPrinterJob();
//        job.setPrintable(null);
//        boolean ok = job.printDialog();
//        if (ok) {
//            try {
//                job.print();
//            } catch (PrinterException ex) {
//                System.out.println("In Print cache");
//            }
//        }
    }//GEN-LAST:event_txt_printActionPerformed

    private void txt_closeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_closeActionPerformed
        System.exit(0);
    }//GEN-LAST:event_txt_closeActionPerformed

    private void btn_CloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_CloseActionPerformed
        System.exit(0);
    }//GEN-LAST:event_btn_CloseActionPerformed

    private void btn_PrimarySettingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_PrimarySettingActionPerformed
//        JPasswordField pf = new JPasswordField();
//        int option = JOptionPane.showConfirmDialog(null,pf, "Enter Password", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
//        if (option == JOptionPane.OK_OPTION) {
//            if (new String(pf.getPassword()).equals(Config.configuserprofile.get(0).getPassword())) {
               Config.productconfiguration.onloadReset();
                Config.productconfiguration.setVisible(true);
//            } else {
//                JOptionPane.showMessageDialog(this,"Provided password is incorrect.","Error",JOptionPane.ERROR_MESSAGE);
//            }
//        }
        
    }//GEN-LAST:event_btn_PrimarySettingActionPerformed

    private void btn_ExportFileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ExportFileActionPerformed
//        JPasswordField pf = new JPasswordField();
//        int option = JOptionPane.showConfirmDialog(null,pf, "Enter Password", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
//        if (option == JOptionPane.OK_OPTION ) {
//            if (new String(pf.getPassword()).equals(Config.password)) {
//
//            } else {
//                JOptionPane.showMessageDialog(this,"Provided password is incorrect.","Error",JOptionPane.ERROR_MESSAGE);
//            }
//        }
    }//GEN-LAST:event_btn_ExportFileActionPerformed

    private void btn_ImportFileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ImportFileActionPerformed
//        JPasswordField pf = new JPasswordField();
//        int option = JOptionPane.showConfirmDialog(null,pf, "Enter Password", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
//        if (option == JOptionPane.OK_OPTION ) {
//            if (new String(pf.getPassword()).equals(Config.configuserprofile.get(0).getPassword())) {
//
//            } else {
//                JOptionPane.showMessageDialog(this,"Provided password is incorrect.","Error",JOptionPane.ERROR_MESSAGE);
//            }
//        }
    }//GEN-LAST:event_btn_ImportFileActionPerformed

    private void btn_ManageAccountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ManageAccountActionPerformed
//        Config.manageaccount.onloadReset();
//        Config.manageaccount.setVisible(true);
    }//GEN-LAST:event_btn_ManageAccountActionPerformed

    private void btn_ProductLicenseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ProductLicenseActionPerformed
//        Config.productlicense.onloadReset();
//        Config.productlicense.setVisible(true);
    }//GEN-LAST:event_btn_ProductLicenseActionPerformed

    private void tbl_tenderMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_tenderMouseClicked
        if (evt.getClickCount()==2) {
        btn_viewtenderActionPerformed(null);    
        }        // TODO add your handling code here:
    }//GEN-LAST:event_tbl_tenderMouseClicked

    private void btn_SearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SearchActionPerformed
        if(txt_department.getText().equals("") && txt_division.getText().equals("")){
            departmentid="";
            divisionid="";
        }
        String sql="";
        String FromDate="";
        String ToDate="";        
         try{
            ToDate=sdf.format(jdc_toDate.getDate());
            }catch(Exception e){
            ToDate="";
            }
         try{
            FromDate=sdf.format(jdc_fromDate.getDate());
            }catch(Exception e){
            FromDate="";
            }

            if(departmentid.equals("") && divisionid.equals("") && ToDate.equals("") && FromDate.equals("") && txt_nit_no.getText().equals("")){
                
                if (!txt_nit_no.getText().equals("")) {
                    sql = "select t.*,a.agreementid from tenderentry t left JOIN agreement a ON t.tenderentryid=a.agreementid and t.nitno  like'"+txt_nit_no.getText().trim()+"%'"; 
                } else {
                    sql = "select t.*,a.agreementid from tenderentry t left JOIN agreement a ON t.tenderentryid=a.agreementid"; 
                }
            }else if(departmentid.equals("") && divisionid.equals("") ){
                if(!txt_nit_no.getText().equals("")){
                    if(!ToDate.equals("") && FromDate.equals("") ){
                            sql = "select t.*,a.agreementid from tenderentry t left JOIN agreement a ON t.tenderentryid=a.agreementid where  STR_TO_DATE(tenderdate,'%d-%m-%Y')<=STR_TO_DATE('"+ToDate+"','%d-%m-%Y') and t.nitno  like'"+txt_nit_no.getText().trim()+"%'";        
                        }else if(ToDate.equals("") && !FromDate.equals("")){
                            sql = "select t.*,a.agreementid from tenderentry t left JOIN agreement a ON t.tenderentryid=a.agreementid where  STR_TO_DATE(tenderdate,'%d-%m-%Y')>=STR_TO_DATE('"+FromDate+"','%d-%m-%Y') and t.nitno  like'"+txt_nit_no.getText().trim()+"%'";        
                        }else{
                            sql = "select t.*,a.agreementid from tenderentry t left JOIN agreement a ON t.tenderentryid=a.agreementid where STR_TO_DATE(tenderdate,'%d-%m-%Y')>=STR_TO_DATE('"+FromDate+"','%d-%m-%Y') and STR_TO_DATE(tenderdate,'%d-%m-%Y')<=STR_TO_DATE('"+ToDate+"','%d-%m-%Y') and t.nitno  like'"+txt_nit_no.getText().trim()+"%'";        
                        }
                }else{
                    if(!ToDate.equals("") && FromDate.equals("") ){
                            sql = "select t.*,a.agreementid from tenderentry t left JOIN agreement a ON t.tenderentryid=a.agreementid where  STR_TO_DATE(tenderdate,'%d-%m-%Y')<=STR_TO_DATE('"+ToDate+"','%d-%m-%Y')";        
                        }else if(ToDate.equals("") && !FromDate.equals("")){
                            sql = "select t.*,a.agreementid from tenderentry t left JOIN agreement a ON t.tenderentryid=a.agreementid where  STR_TO_DATE(tenderdate,'%d-%m-%Y')>=STR_TO_DATE('"+FromDate+"','%d-%m-%Y')";        
                        }else{
                            sql = "select t.*,a.agreementid from tenderentry t left JOIN agreement a ON t.tenderentryid=a.agreementid where STR_TO_DATE(tenderdate,'%d-%m-%Y')>=STR_TO_DATE('"+FromDate+"','%d-%m-%Y') and STR_TO_DATE(tenderdate,'%d-%m-%Y')<=STR_TO_DATE('"+ToDate+"','%d-%m-%Y')";        
                        }
                    
                }
            }  else if(!departmentid.equals("") && divisionid.equals("")){
                
                if(!txt_nit_no.getText().equals("")){
                        if(ToDate.equals("") && FromDate.equals("")){
                            sql = "select t.*,a.agreementid from tenderentry t left JOIN agreement a ON t.tenderentryid=a.agreementid where departmentid ='"+departmentid+"' and t.nitno  like'"+txt_nit_no.getText().trim()+"%'";        
                        }else if(!ToDate.equals("") && FromDate.equals("")){
                            sql = "select t.*,a.agreementid from tenderentry t left JOIN agreement a ON t.tenderentryid=a.agreementid where departmentid ='"+departmentid+"' and STR_TO_DATE(tenderdate,'%d-%m-%Y')<=STR_TO_DATE('"+ToDate+"','%d-%m-%Y') and t.nitno  like'"+txt_nit_no.getText().trim()+"%'";        
                        }else if(ToDate.equals("") && !FromDate.equals("")){
                            sql = "select t.*,a.agreementid from tenderentry t left JOIN agreement a ON t.tenderentryid=a.agreementid where departmentid ='"+departmentid+"' and STR_TO_DATE(tenderdate,'%d-%m-%Y')>=STR_TO_DATE('"+FromDate+"','%d-%m-%Y') and t.nitno  like'"+txt_nit_no.getText().trim()+"%'";        
                        }else{
                            sql = "select t.*,a.agreementid from tenderentry t left JOIN agreement a ON t.tenderentryid=a.agreementid where departmentid ='"+departmentid+"' and STR_TO_DATE(tenderdate,'%d-%m-%Y')>=STR_TO_DATE('"+FromDate+"','%d-%m-%Y') and STR_TO_DATE(tenderdate,'%d-%m-%Y')<=STR_TO_DATE('"+ToDate+"','%d-%m-%Y') and t.nitno  like'"+txt_nit_no.getText().trim()+"%'";        
                        }
                }else{
                        if(ToDate.equals("") && FromDate.equals("")){
                            sql = "select t.*,a.agreementid from tenderentry t left JOIN agreement a ON t.tenderentryid=a.agreementid where departmentid ='"+departmentid+"'";        
                        }else if(!ToDate.equals("") && FromDate.equals("")){
                            sql = "select t.*,a.agreementid from tenderentry t left JOIN agreement a ON t.tenderentryid=a.agreementid where departmentid ='"+departmentid+"' and STR_TO_DATE(tenderdate,'%d-%m-%Y')<=STR_TO_DATE('"+ToDate+"','%d-%m-%Y')";        
                        }else if(ToDate.equals("") && !FromDate.equals("")){
                            sql = "select t.*,a.agreementid from tenderentry t left JOIN agreement a ON t.tenderentryid=a.agreementid where departmentid ='"+departmentid+"' and STR_TO_DATE(tenderdate,'%d-%m-%Y')>=STR_TO_DATE('"+FromDate+"','%d-%m-%Y')";        
                        }else{
                            sql = "select t.*,a.agreementid from tenderentry t left JOIN agreement a ON t.tenderentryid=a.agreementid where departmentid ='"+departmentid+"' and STR_TO_DATE(tenderdate,'%d-%m-%Y')>=STR_TO_DATE('"+FromDate+"','%d-%m-%Y') and STR_TO_DATE(tenderdate,'%d-%m-%Y')<=STR_TO_DATE('"+ToDate+"','%d-%m-%Y')";        
                        }
                }
            }else{      
                if(!txt_nit_no.getText().equals("")){
                        if(ToDate.equals("") && FromDate.equals("")){
                            sql = "select t.*,a.agreementid from tenderentry t left JOIN agreement a ON t.tenderentryid=a.agreementid where departmentid ='"+departmentid+"' and divisionid ='"+divisionid+"' and t.nitno  like'"+txt_nit_no.getText().trim()+"%'";        
                        }else if(!ToDate.equals("") && FromDate.equals("")){
                            sql = "select t.*,a.agreementid from tenderentry t left JOIN agreement a ON t.tenderentryid=a.agreementid where departmentid ='"+departmentid+"' and divisionid ='"+divisionid+"' and STR_TO_DATE(tenderdate,'%d-%m-%Y')<=STR_TO_DATE('"+ToDate+"','%d-%m-%Y') and t.nitno  like'"+txt_nit_no.getText().trim()+"%'";        
                        }else if(ToDate.equals("") && !FromDate.equals("")){
                            sql = "select t.*,a.agreementid from tenderentry t left JOIN agreement a ON t.tenderentryid=a.agreementid where departmentid ='"+departmentid+"' and divisionid ='"+divisionid+"' and STR_TO_DATE(tenderdate,'%d-%m-%Y')>=STR_TO_DATE('"+FromDate+"','%d-%m-%Y') and t.nitno  like'"+txt_nit_no.getText().trim()+"%'";        
                        }else{
                            sql = "select t.*,a.agreementid from tenderentry t left JOIN agreement a ON t.tenderentryid=a.agreementid where departmentid ='"+departmentid+"' and divisionid ='"+divisionid+"' and STR_TO_DATE(tenderdate,'%d-%m-%Y')>=STR_TO_DATE('"+FromDate+"','%d-%m-%Y') and STR_TO_DATE(tenderdate,'%d-%m-%Y')<=STR_TO_DATE('"+ToDate+"','%d-%m-%Y')  and t.nitno  like'"+txt_nit_no.getText().trim()+"%'";        
                        }
                }else{
                    if(ToDate.equals("") && FromDate.equals("")){
                            sql = "select t.*,a.agreementid from tenderentry t left JOIN agreement a ON t.tenderentryid=a.agreementid where departmentid ='"+departmentid+"' and divisionid ='"+divisionid+"'";        
                        }else if(!ToDate.equals("") && FromDate.equals("")){
                            sql = "select t.*,a.agreementid from tenderentry t left JOIN agreement a ON t.tenderentryid=a.agreementid where departmentid ='"+departmentid+"' and divisionid ='"+divisionid+"' and STR_TO_DATE(tenderdate,'%d-%m-%Y')<=STR_TO_DATE('"+ToDate+"','%d-%m-%Y')";        
                        }else if(ToDate.equals("") && !FromDate.equals("")){
                            sql = "select t.*,a.agreementid from tenderentry t left JOIN agreement a ON t.tenderentryid=a.agreementid where departmentid ='"+departmentid+"' and divisionid ='"+divisionid+"' and STR_TO_DATE(tenderdate,'%d-%m-%Y')>=STR_TO_DATE('"+FromDate+"','%d-%m-%Y')";        
                        }else{
                            sql = "select t.*,a.agreementid from tenderentry t left JOIN agreement a ON t.tenderentryid=a.agreementid where departmentid ='"+departmentid+"' and divisionid ='"+divisionid+"' and STR_TO_DATE(tenderdate,'%d-%m-%Y')>=STR_TO_DATE('"+FromDate+"','%d-%m-%Y') and STR_TO_DATE(tenderdate,'%d-%m-%Y')<=STR_TO_DATE('"+ToDate+"','%d-%m-%Y')";        
                        }
                }
            }
            view(sql);
            
    }//GEN-LAST:event_btn_SearchActionPerformed

    private void btn_viewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_viewActionPerformed

      //  Config.sql="select * from agreement group by tenderid having tenderid='"+Config.configtenderentry.get(tbl_tender.getSelectedRow()) +"'";
     
    }//GEN-LAST:event_btn_viewActionPerformed

    private void btn_agreement1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_agreement1ActionPerformed
        newagreementActionPerformed(null);
    }//GEN-LAST:event_btn_agreement1ActionPerformed

    private void btn_ExpenditureActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ExpenditureActionPerformed
            btn_reportActionPerformed(null);
//        Config.expenditure.onloadreset();
        //        Config.expenditure.setVisible(true);
    }//GEN-LAST:event_btn_ExpenditureActionPerformed

    private void btn_CustomerProfileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_CustomerProfileActionPerformed
               Config.customermanagement.onloadReset();
               Config.customermanagement.setVisible(true);
    }//GEN-LAST:event_btn_CustomerProfileActionPerformed

    private void btn_deleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_deleteActionPerformed
        try {
            if(Config.tenderentrymgr.delTenderEntryid(tbl_tendermodel.getValueAt(tbl_tender.getSelectedRow(),8 ).toString())){
                refresh();
                JOptionPane.showMessageDialog(this,"Delete file Successfully", "Success", JOptionPane.NO_OPTION);
            }else{
                JOptionPane.showMessageDialog(this,"Error in Deletion", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
		JOptionPane.showMessageDialog(this,"Select Any rows", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_deleteActionPerformed

 private void txt_departmentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_departmentActionPerformed
                Config.department_search.onloadReset(4, 1,txt_department.getText().trim());
		Config.department_search.setVisible(true);
 }//GEN-LAST:event_txt_departmentActionPerformed

 private void txt_divisionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_divisionActionPerformed
                Config.division_search.onloadReset(3, departmentid,txt_division.getText().trim());
		Config.division_search.setVisible(true);
 }//GEN-LAST:event_txt_divisionActionPerformed

 private void txt_divisionCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_divisionCaretUpdate
      if(txt_division.getText().equals("")){
             divisionid="";
      }
 }//GEN-LAST:event_txt_divisionCaretUpdate
 
 private void txt_departmentKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_departmentKeyPressed
    if(txt_department.getText().equals("")){
        if(Character.isLetter(evt.getKeyChar())){
            Config.department_search.onloadReset(4,1,String.valueOf(evt.getKeyChar()));
            Config.department_search.setVisible(true);
         }
    }else{
        if(Character.isLetter(evt.getKeyChar())||evt.getKeyCode()==KeyEvent.VK_BACK_SPACE ){
            Config.department_search.onloadReset(4, 1,txt_department.getText().trim());
            Config.department_search.setVisible(true);
        }
    }
 }//GEN-LAST:event_txt_departmentKeyPressed

 private void jPanel7KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jPanel7KeyPressed

 }//GEN-LAST:event_jPanel7KeyPressed

 private void txt_divisionKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_divisionKeyPressed
    if(txt_division.getText().equals("")){
        if(Character.isLetter(evt.getKeyChar())){
            Config.division_search.onloadReset(3, departmentid,String.valueOf(evt.getKeyChar()));
            Config.division_search.setVisible(true);
        }
    }else{
        if(Character.isLetter(evt.getKeyChar())||evt.getKeyCode()==KeyEvent.VK_BACK_SPACE ){
            Config.division_search.onloadReset(3, departmentid,txt_division.getText().trim());
            Config.division_search.setVisible(true);
        }
    }
 }//GEN-LAST:event_txt_divisionKeyPressed

    private void ManageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ManageActionPerformed
        
    }//GEN-LAST:event_ManageActionPerformed

    private void jdc_fromDateFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jdc_fromDateFocusLost
           
    }//GEN-LAST:event_jdc_fromDateFocusLost

    private void jdc_fromDatePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jdc_fromDatePropertyChange
        
    }//GEN-LAST:event_jdc_fromDatePropertyChange

    private void jdc_fromDateVetoableChange(java.beans.PropertyChangeEvent evt)throws java.beans.PropertyVetoException {//GEN-FIRST:event_jdc_fromDateVetoableChange
        // TODO add your handling code here:
    }//GEN-LAST:event_jdc_fromDateVetoableChange

    private void txt_nit_noCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_nit_noCaretUpdate
        btn_SearchActionPerformed(null);
    }//GEN-LAST:event_txt_nit_noCaretUpdate

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu Configure;
    private javax.swing.JMenu File;
    private javax.swing.JMenu Help;
    private javax.swing.JMenu Manage;
    private javax.swing.JMenu Utilities;
    private javax.swing.JMenuItem btn_Close;
    private javax.swing.JMenuItem btn_CompanyProfile;
    private javax.swing.JMenuItem btn_CustomerProfile;
    private javax.swing.JMenuItem btn_Expenditure;
    private javax.swing.JMenuItem btn_ExportFile;
    private javax.swing.JMenuItem btn_HelpContent;
    private javax.swing.JMenuItem btn_ImportFile;
    private javax.swing.JMenuItem btn_ManageAccount;
    private javax.swing.JMenuItem btn_PrimarySetting;
    private javax.swing.JMenuItem btn_ProductLicense;
    private javax.swing.JButton btn_Search;
    private javax.swing.JButton btn_agreement1;
    private javax.swing.JButton btn_delete;
    private javax.swing.JButton btn_newtender;
    private javax.swing.JButton btn_report;
    private javax.swing.JButton btn_view;
    private javax.swing.JButton btn_viewtender;
    private javax.swing.JCheckBox chb_date;
    private javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JMenu jMenu7;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JToolBar.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator10;
    private javax.swing.JPopupMenu.Separator jSeparator11;
    private javax.swing.JPopupMenu.Separator jSeparator12;
    private javax.swing.JPopupMenu.Separator jSeparator13;
    private javax.swing.JToolBar.Separator jSeparator14;
    private javax.swing.JToolBar.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JToolBar.Separator jSeparator4;
    private javax.swing.JToolBar.Separator jSeparator5;
    private javax.swing.JPopupMenu.Separator jSeparator6;
    private javax.swing.JToolBar.Separator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JPopupMenu.Separator jSeparator9;
    private javax.swing.JToolBar jToolBar1;
    private com.toedter.calendar.JDateChooser jdc_fromDate;
    private com.toedter.calendar.JDateChooser jdc_toDate;
    private javax.swing.JTable tbl_tender;
    private javax.swing.JPopupMenu popupMenu;
    private javax.swing.JButton txt_close;
    private javax.swing.JButton txt_delete;
    private javax.swing.JTextField txt_department;
    private javax.swing.JTextField txt_division;
    private javax.swing.JTextField txt_nit_no;
    private javax.swing.JButton txt_print;
    // End of variables declaration//GEN-END:variables
    boolean b=false;
    public void onloadReset(){
    currentDate = Calendar.getInstance();
     try {
        jdc_toDate.setDate(currentDate.getTime());    
        } catch (Exception e) {
        jdc_toDate.setDate(null);    
        }    
        txt_department.setText("");
        txt_division.setText("");
        
//            Thread t2 = new Thread() {
//            public void run() {
//           while(true){
//               try {
//                        Thread.sleep(Config.time);
//                   if(b){
//                   loadAutoData();    
//                   }else{
//                     break;  
//                   }    
//                    } catch (InterruptedException ex) {
//                    
//                    }
//                }
//            }
//
//           private void loadAutoData() {
//               
//           }
//        };
//        b=true;
//        t2.start();
								refresh();
     }
    
    
    public void view(String sql)
    {
     tbl_tendermodel.setRowCount(0); 
     String dpt=null;   
     String div=null;
        try {
            System.out.println(sql+" ORDER BY tenderentryid DESC");
            Config.rs = Config.stmt.executeQuery(sql+" ORDER BY tenderentryid DESC");
            
            while (Config.rs.next()) {
                if(Config.rs.getString("agreementid")==null){ 
//                        || Config.rs.getString("payement_status").equals("false") ){
                tbl_tendermodel.addRow(new Object [] {
                Config.rs.getString("nitno"),
                Config.rs.getString("nitdate"),
                Config.rs.getString("nameofwork"),
                Config.rs.getString("pac"),
                Config.rs.getString("emrs"),
                Config.rs.getString("eminshapeof"),
                Config.rs.getString("embankname"),
                Config.rs.getString("emdate"),
                Config.rs.getString("status"),
                Config.rs.getString("tenderentryid")
                });
                }
            }
            }catch(Exception e){
                e.printStackTrace();
            }

      
    
    }
    
    public void refresh(){
        btn_SearchActionPerformed(null);    
    }
    
    private void viewtenderActionPerformed(java.awt.event.ActionEvent evt) {
    if(!(tbl_tender.getSelectedRow()==-1)){
       try {    
             Config.newviewtenderentry.onloadReset(tbl_tendermodel.getValueAt(tbl_tender.getSelectedRow(),9).toString());
             Config.newviewtenderentry.setVisible(true);
        } catch (Exception e) {
          e.printStackTrace();  
        }
       }
       else{
       JOptionPane.showMessageDialog(this, "Please select any rows", "Error", JOptionPane.ERROR_MESSAGE);    
       }
    }
     
    
    private void viewagreementActionPerformed(java.awt.event.ActionEvent evt) {
       if(!(tbl_tender.getSelectedRow()==-1)){
           Config.sql = "select * from agreement where agreementid='"+tbl_tendermodel.getValueAt(tbl_tender.getSelectedRow(),9 ).toString()+"' ";
                try {
                    Config.rs = Config.stmt.executeQuery(Config.sql);
                if(Config.rs.next()) {
                    Config.viewagreement.onloadReset(tbl_tendermodel.getValueAt(tbl_tender.getSelectedRow(),8 ).toString());
                    Config.viewagreement.setVisible(true);
                }
                else{
                  JOptionPane.showMessageDialog(this, "Agreement does not exits", "Error", JOptionPane.ERROR_MESSAGE);      
                }
                } catch (SQLException ex) {
                    Logger.getLogger(AdminHome.class.getName()).log(Level.SEVERE, null, ex);
                }
           
           
           
           
           
       
       }
       else{
       JOptionPane.showMessageDialog(this, "Please select any rows", "Error", JOptionPane.ERROR_MESSAGE);    
       }
       
    }
     
 
    private void cutActionPerformed(java.awt.event.ActionEvent evt) {
     if(!(tbl_tender.getSelectedRow()==-1)){
       
       }
       else{
       JOptionPane.showMessageDialog(this, "Please select any rows", "Error", JOptionPane.ERROR_MESSAGE);    
       }
    }
    
       
    private void copyActionPerformed(java.awt.event.ActionEvent evt) {
   if(!(tbl_tender.getSelectedRow()==-1)){
       
       }
       else{
       JOptionPane.showMessageDialog(this, "Please select any rows", "Error", JOptionPane.ERROR_MESSAGE);    
       }
    }
         
   
    private void deleteActionPerformed(java.awt.event.ActionEvent evt) {
     if(!(tbl_tender.getSelectedRow()==-1)){
						btn_deleteActionPerformed(null);
       }
       else{
       JOptionPane.showMessageDialog(this, "Please select any rows", "Error", JOptionPane.ERROR_MESSAGE);    
       };
    }
       
   
    private void newagreementActionPerformed(java.awt.event.ActionEvent evt) {
     if(!(tbl_tender.getSelectedRow()==-1)){
                try {
                    Config.sql = "select * from agreement where agreementid='"+tbl_tendermodel.getValueAt(tbl_tender.getSelectedRow(),9).toString() +"' ";
                    Config.rs = Config.stmt.executeQuery(Config.sql);
                if(!Config.rs.next()) {
                   Config.newagreement.onloadReset(tbl_tendermodel.getValueAt(tbl_tender.getSelectedRow(),9).toString() );
                   Config.newagreement.setVisible(true);
                }
                else{
                  JOptionPane.showMessageDialog(this, "Agreement already exits", "Error", JOptionPane.ERROR_MESSAGE);      
                }
                } catch (SQLException ex) {
                    
                }
       }
       else{
       JOptionPane.showMessageDialog(this, "Please select any rows", "Error", JOptionPane.ERROR_MESSAGE);    
       };
    }
    
        public void setData(String n) {
            txt_department.setText("");
            try {
                for (int i = 0; i < Config.configdepartment.size(); i++) {
                        try {
                                if(Config.configdepartment.get(i).getDepartmentname().equals(n) || Config.configdepartment.get(i).getAbbreviation().equals(n)){
                                        txt_department.setText(Config.configdepartment.get(i).getDepartmentname());
                                        departmentid = Config.configdepartment.get(i).getDepartmentid();
                                        break;
                                }
                        } catch (Exception e) {
                        }
                }
                txt_division.setText("");
                divisionid ="";
            } catch (Exception e) {
            }
            btn_SearchActionPerformed(null);
	}

	public void setDivision(String n) {
	txt_division.setText("");
		try {
			for (int i = 0; i < Config.configdivision.size(); i++) {
				try {
					if(Config.configdivision.get(i).getDepartmentid().equals(departmentid) && Config.configdivision.get(i).getDivisionname().equals(n)){
						txt_division.setText(Config.configdivision.get(i).getDivisionname());
						divisionid = Config.configdivision.get(i).getDivisionid();
						break;
					}
				} catch (Exception e) {
				}
			}
			
		} catch (Exception e) {
		}
		btn_SearchActionPerformed(null);
	}

}
class CustomTableCellRenderer extends DefaultTableCellRenderer {
        public Component getTableCellRendererComponent (JTable table, Object obj, boolean isSelected, boolean hasFocus, int row, int column) {
            Component cell = super.getTableCellRendererComponent(table, obj, isSelected, hasFocus, row, column);            
            try {
                if (isSelected) {
                    cell.setBackground(new java.awt.Color(96,118,159));                    
                } else {
                    if (table.getValueAt(row, 8).toString().equals("Pending")) {
                        cell.setBackground(new java.awt.Color(255,204,255));
                    } else if (table.getValueAt(row, 8).toString().equals("Lowest")) {
                        cell.setBackground(new java.awt.Color(204,255,204));
                    } else if (table.getValueAt(row, 8).toString().equals("Asp")) {
                        cell.setBackground(new java.awt.Color(255,255,153));
                    }else{
                        cell.setBackground(Color.WHITE);
                    }
                }
            } catch (Exception e) {
                cell.setBackground(Color.WHITE);
            }
            return cell;

        }

    }

