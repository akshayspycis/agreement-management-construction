package classmanager;

import datamanager.Config;
import datamanager.BankDetails;

public class BankDetailsMgr {
    
    //method to insert bankdetails in database
    public boolean insBankDetails(BankDetails bankdetails) {
        try {          
            Config.sql = "insert into bankdetails ("                   
                    + "bank_name,"
                    + "address ,"
                    + "locality,"
                    + "city,"                    
                    + "pincode,"
                    + "state)"
                    
                    + "values (?,?,?,?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, bankdetails.getBank_name());
            Config.pstmt.setString(2, bankdetails.getAddress());
            Config.pstmt.setString(3, bankdetails.getLocality());
            Config.pstmt.setString(4, bankdetails.getCity());
            Config.pstmt.setString(5, bankdetails.getPincode());
            Config.pstmt.setString(6, bankdetails.getState());
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.configmgr.loadBankDetails();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to BankDetails in database 
   public boolean updBankDetails(BankDetails bankdetails) {
        try {
            Config.sql = "update bankdetails set"
                    + " bank_name = ?, "
                    + " address = ?, "
                    + " locality = ?, "
                    + " city = ?, "
                    + " pincode = ?, "
                    + " state = ? "
                    + " where bankdetails_id = '"+bankdetails.getBankdetails_id()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, bankdetails.getBank_name());
            Config.pstmt.setString(2, bankdetails.getAddress());
            Config.pstmt.setString(3, bankdetails.getLocality());
            Config.pstmt.setString(4, bankdetails.getCity());
            Config.pstmt.setString(5, bankdetails.getPincode());
            Config.pstmt.setString(6, bankdetails.getState());
            
            int x = Config.pstmt.executeUpdate();
            
            if (x>0) {
                Config.configmgr.loadBankDetails();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete coustomerprofile in database
   public boolean delBankDetails(String bankdetails_id)
   {
        try {          
            Config.sql = "delete from bankdetails where bankdetails_id = '"+bankdetails_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.configmgr.loadBankDetails();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
}
