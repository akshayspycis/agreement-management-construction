/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package classmanager;

import datamanager.Config;
import datamanager.PaymentSheetFormula;

/**
 *
 * @author akki
 */
public class PaymentSheetFormulaMgr {

	//method to department in database 
   public boolean updPaymentSheetFormula(PaymentSheetFormula payment_sheet_formula) {
        try {
            Config.sql = "update payment_sheet_formula set"
                    + " sd_cash = ?, "
																				+ " sd_bg = ?, "
																				+ " pg_cash = ?, "
																				+ " pg_bg = ?, "
																				+ " income_tax = ?, "
																				+ " cost_of_bill = ?, "
																				+ " commercial_tax = ?, "
																				+ " labour_welfare = ? ";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            Config.pstmt.setString(1, payment_sheet_formula.getSd_cash());
            Config.pstmt.setString(2, payment_sheet_formula.getSd_bg());
												Config.pstmt.setString(3, payment_sheet_formula.getPg_cash());
            Config.pstmt.setString(4, payment_sheet_formula.getPg_bg());
												Config.pstmt.setString(5, payment_sheet_formula.getIncome_tax());
            Config.pstmt.setString(6, payment_sheet_formula.getCost_of_bill());
												Config.pstmt.setString(7, payment_sheet_formula.getCommercial_tax());
            Config.pstmt.setString(8, payment_sheet_formula.getLabour_welfare());
												int x = Config.pstmt.executeUpdate();
            if (x>0) {
                Config.configmgr.loadPaymentSheetFormula();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------

}
