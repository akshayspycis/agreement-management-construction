/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package classmanager;

import datamanager.BOQEntry;
import datamanager.Config;
import datamanager.PartyRate;
import datamanager.TenderEntry;
import java.util.ArrayList;


/**
 *
 * @author Home
 */
public class TenderentryMgr {
    
    
    
      
    //method to insert tenderentry in database
    public boolean insTenderEntry(TenderEntry tenderentry,ArrayList<BOQEntry> boqentry,ArrayList<PartyRate> partyrate) {
        try {          
            Config.sql = "insert into tenderentry ("                   
                    + "departmentid,"
                    + "divisionid ,"
                    + "contactno,"
                    + "nitno,"
                    + "nitdate,"
                    + "issuedby ,"
                    + "pac,"
                    + "totalamount,"
                    + "emrs,"
                    + "eminshapeof,"
                    + "emno,"
                    + "emdate,"
                    + "embankname,"
                    + "emperiod,"
                    + "purchaseoftender,"
                    + "submissionoftender,"
                    + "openingoftender,"
                    + "nameofwork,"
                    + "timealloted,"
                    + "percentage,"
                    + "tenderdate,"
                    + "contactno_r,"
                    + "rs,"
                    + "status)"
                    
                    + "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, tenderentry.getDepartmentid());
            Config.pstmt.setString(2, tenderentry.getDivisionid());
            Config.pstmt.setString(3, tenderentry.getContactno());
            Config.pstmt.setString(4, tenderentry.getNitno());
            Config.pstmt.setString(5, tenderentry.getNitdate());
            Config.pstmt.setString(6, tenderentry.getIssuedby());
            Config.pstmt.setString(7, tenderentry.getPac());
            Config.pstmt.setString(8, tenderentry.getTotalamount());
            Config.pstmt.setString(9, tenderentry.getEmrs());
            Config.pstmt.setString(10, tenderentry.getEminshapeof());
            Config.pstmt.setString(11, tenderentry.getEmno());
            Config.pstmt.setString(12, tenderentry.getEmdate());
            Config.pstmt.setString(13, tenderentry.getEmbankname());            
            Config.pstmt.setString(14, tenderentry.getEmperiod());  
            Config.pstmt.setString(15, tenderentry.getPurchaseoftender());            
            Config.pstmt.setString(16, tenderentry.getSubmissionoftender());            
            Config.pstmt.setString(17, tenderentry.getOpeningoftender());            
            Config.pstmt.setString(18, tenderentry.getNameofwork());            
            Config.pstmt.setString(19, tenderentry.getTimealloted());            
            Config.pstmt.setString(20, tenderentry.getPercentage());            
            Config.pstmt.setString(21, tenderentry.getTenderdate());            
            Config.pstmt.setString(22, tenderentry.getContactno_r());      
            Config.pstmt.setString(23, tenderentry.getRs());      
            Config.pstmt.setString(24, tenderentry.getStatus());      

            int x = Config.pstmt.executeUpdate();
            
            if(x>0){
                String id=null;
            Config.sql="select max(tenderentryid) from tenderentry";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while(Config.rs.next()){
             id=String.valueOf(Config.rs.getInt(1));    
            }
            if(Config.boqentrymgr.insBOQEntry(boqentry, id) && Config.partyratemgr.insPartyRate(partyrate, id)){
            return true;
            }else{
            return false;   
            }
            }else{
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to tenderentry in database 
   public boolean updTenderEntry(TenderEntry tenderentry,ArrayList<BOQEntry> boqentry,ArrayList<PartyRate> partyrate) {
        try {
            Config.sql = "update tenderentry set"
                    + " departmentid = ?, "
                    + " divisionid = ?, "
                    + " contactno = ?, "
                    + " nitno = ?, "
                    + " nitdate = ?, "
                    + " issuedby = ?, "
                    + " pac = ?, "
                    + " totalamount = ?, "
                    + " emrs = ?, "
                    + " eminshapeof = ?, "
                    + " emno = ?, "
                    + " emdate = ?, "
                    + " embankname = ?, "
                    + " emperiod = ?, "
                    + " purchaseoftender = ?, "
                    + " submissionoftender = ?, "
                    + " openingoftender = ?, "
                    + " nameofwork = ?, "
                    + " timealloted = ? ,"
                    + " percentage = ? ,"
                    + " tenderdate = ? ,"
                    + " contactno_r = ?, "
                    + " rs = ?, "
                    + " status = ? "
                    + " where tenderentryid = '"+tenderentry.getTenderentryid()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, tenderentry.getDepartmentid());
            Config.pstmt.setString(2, tenderentry.getDivisionid());
            Config.pstmt.setString(3, tenderentry.getContactno());
            Config.pstmt.setString(4, tenderentry.getNitno());
            Config.pstmt.setString(5, tenderentry.getNitdate());
            Config.pstmt.setString(6, tenderentry.getIssuedby());
            Config.pstmt.setString(7, tenderentry.getPac());
            Config.pstmt.setString(8, tenderentry.getTotalamount());
            Config.pstmt.setString(9, tenderentry.getEmrs());
            Config.pstmt.setString(10, tenderentry.getEminshapeof());
            Config.pstmt.setString(11, tenderentry.getEmno());
            Config.pstmt.setString(12, tenderentry.getEmdate());
            Config.pstmt.setString(13, tenderentry.getEmbankname());            
            Config.pstmt.setString(14, tenderentry.getEmperiod());  
            Config.pstmt.setString(15, tenderentry.getPurchaseoftender());            
            Config.pstmt.setString(16, tenderentry.getSubmissionoftender());            
            Config.pstmt.setString(17, tenderentry.getOpeningoftender());            
            Config.pstmt.setString(18, tenderentry.getNameofwork());            
            Config.pstmt.setString(19, tenderentry.getTimealloted());            
            Config.pstmt.setString(20, tenderentry.getPercentage());            
            Config.pstmt.setString(21, tenderentry.getTenderdate());            
            Config.pstmt.setString(22, tenderentry.getContactno_r());      
            Config.pstmt.setString(23, tenderentry.getRs());      
            Config.pstmt.setString(24, tenderentry.getStatus());                 
            
            int x = Config.pstmt.executeUpdate();
            
            if(x>0){
            if(Config.boqentrymgr.insBOQEntry(boqentry, tenderentry.getTenderentryid()) && Config.partyratemgr.insPartyRate(partyrate, tenderentry.getTenderentryid())){
            return true;
            }else{
            return false;   
            }
            }else{
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete tenderentryid in database
   public boolean delTenderEntryid(String tenderentryid)
   {
        try {          
            Config.sql = "delete from tenderentry where tenderentryid = '"+tenderentryid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                if(Config.boqentrymgr.delBOQEntry(tenderentryid)){
                return true;
                }else{
                    return true;
                }
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
    
}
