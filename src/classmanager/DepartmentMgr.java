package classmanager;

import datamanager.BOQEntry;
import datamanager.Config;
import datamanager.Department;
import datamanager.Division;
import datamanager.TenderEntry;
import java.util.ArrayList;

/**
 *
 * @author akshay
 */
public class DepartmentMgr {
    
           
      //method to insert department in database
    public boolean insDepartment(Department department) {
        try {          
            Config.sql = "insert into department ("                   
                    + "departmentname,"
                    + "abbreviation)"
                    + "values (?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, department.getDepartmentname());
            Config.pstmt.setString(2, department.getAbbreviation());
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.configmgr.loadDepartment();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to department in database 
   public boolean updDepartment(Department department) {
        try {
            Config.sql = "update department set"
                    + " departmentname = ?, "
                    + " abbreviation = ? "
                    + " where departmentid = '"+department.getDepartmentid()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            Config.pstmt.setString(1, department.getDepartmentname());
            Config.pstmt.setString(2, department.getAbbreviation());
            
            int x = Config.pstmt.executeUpdate();
            
            if (x>0) {
                Config.configmgr.loadDepartment();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete department in database
   public boolean delDepartment(String departmentid)
   {
        try {          
            Config.sql = "delete from department where departmentid= '"+departmentid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.configmgr.loadDepartment();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
    
}
