package classmanager;

import datamanager.Config;
import datamanager.CustomerProfile;

public class CustomerProfileMgr {
    
    //method to insert coustomerprofile in database
    public boolean insCustomerProfile(CustomerProfile customerprofile) {
        try {          
            Config.sql = "insert into customerprofile ("                   
                    + "customername,"
                    + "gender ,"
                    + "contactno,"
                    + "organization,"
                    + "tinno,"
                    + "address ,"
                    + "locality,"
                    + "city,"                    
                    + "pincode,"
                    + "state,"
                    + "email,"
                    + "fax,"
                    + "website,"
                    + "other)"
                    
                    + "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, customerprofile.getCustomername());
            Config.pstmt.setString(2, customerprofile.getGender());
            Config.pstmt.setString(3, customerprofile.getContactno());
            Config.pstmt.setString(4, customerprofile.getOrganization());
            Config.pstmt.setString(5, customerprofile.getTinno());
            Config.pstmt.setString(6, customerprofile.getAddress());
            Config.pstmt.setString(7, customerprofile.getLocality());
            Config.pstmt.setString(8, customerprofile.getCity());
            Config.pstmt.setString(9, customerprofile.getPincode());
            Config.pstmt.setString(10, customerprofile.getState());
            Config.pstmt.setString(11, customerprofile.getEmail());
            Config.pstmt.setString(12, customerprofile.getFax());
            Config.pstmt.setString(13, customerprofile.getWebsite());
            Config.pstmt.setString(14, customerprofile.getOther());            
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.configmgr.loadCustomerProfile();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to coustomerprofile in database 
   public boolean updCustomerProfile(CustomerProfile customerprofile) {
        try {
            Config.sql = "update customerprofile set"
                    + " customername = ?, "
                    + " gender = ?, "
                    + " contactno = ?, "
                    + " organization = ?, "
                    + " tinno = ?, "
                    + " address = ?, "
                    + " locality = ?, "
                    + " city = ?, "
                    + " pincode = ?, "
                    + " state = ?, "
                    + " email = ?, "
                    + " fax = ?, "
                    + " website = ?, "
                    + " other = ? "
                    
                    + " where customerid = '"+customerprofile.getCustomerid()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, customerprofile.getCustomername());
            Config.pstmt.setString(2, customerprofile.getGender());
            Config.pstmt.setString(3, customerprofile.getContactno());
            Config.pstmt.setString(4, customerprofile.getOrganization());
            Config.pstmt.setString(5, customerprofile.getTinno());
            Config.pstmt.setString(6, customerprofile.getAddress());
            Config.pstmt.setString(7, customerprofile.getLocality());
            Config.pstmt.setString(8, customerprofile.getCity());
            Config.pstmt.setString(9, customerprofile.getPincode());
            Config.pstmt.setString(10, customerprofile.getState());
            Config.pstmt.setString(11, customerprofile.getEmail());
            Config.pstmt.setString(12, customerprofile.getFax());
            Config.pstmt.setString(13, customerprofile.getWebsite());
            Config.pstmt.setString(14, customerprofile.getOther());
            
            int x = Config.pstmt.executeUpdate();
            
            if (x>0) {
                Config.configmgr.loadCustomerProfile();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete coustomerprofile in database
   public boolean delCustomerProfile(String customerid)
   {
        try {          
            Config.sql = "delete from customerprofile where customerid = '"+customerid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.configmgr.loadCustomerProfile();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
}
