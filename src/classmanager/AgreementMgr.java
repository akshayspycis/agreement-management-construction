
package classmanager;

import datamanager.Agreement;
import datamanager.Config;
import javax.swing.JOptionPane;


public class AgreementMgr {
    
      
    //method to insert agreement in database
    public boolean insAgreement(Agreement agreement) {
        try {          

            Config.sql = "insert into agreement ("                   
                    + "agreementid,"
                    + "agreementno,"
                    + "agreementdate ,"
                    + "workorderno,"
                    + "workordernodate,"
                    + "sdrs,"
                    + "shapeof ,"
                    + "shapeofdate,"
                    + "nameofbank,"
                    + "period,"
                    + "formoftender,"
                    + "negotiation,"
                    + "acceptance,"
                    + "letterno,"
                    + "dt,"
                    + "actualdateofworkstarted,"
                    + "completionofworkdate,"
                    + "stipulateddate,"
                    + "timeextention1,"
                    + "timeextention2,"
                    + "payement_status)"
                    
                    + "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            Config.pstmt.setString(1, agreement.getAgreementid());
            Config.pstmt.setString(2, agreement.getAgreementno());
            Config.pstmt.setString(3, agreement.getAgreementdate());
            Config.pstmt.setString(4, agreement.getWorkorderno());
            Config.pstmt.setString(5, agreement.getWorkordernodate());
            Config.pstmt.setString(6, agreement.getSdrs());
            Config.pstmt.setString(7, agreement.getShapeof());
            Config.pstmt.setString(8, agreement.getShapeofdate());
            Config.pstmt.setString(9, agreement.getNameofbank());
            Config.pstmt.setString(10, agreement.getPeriod());
            Config.pstmt.setString(11, agreement.getFormoftender());
            Config.pstmt.setString(12, agreement.getNegotiation());
            Config.pstmt.setString(13, agreement.getAcceptance());
            Config.pstmt.setString(14, agreement.getLetterno());            
            Config.pstmt.setString(15, agreement.getDt());  
            Config.pstmt.setString(16, agreement.getActualdateofworkstarted());            
            Config.pstmt.setString(17, agreement.getCompletionofworkdate());            
            Config.pstmt.setString(18, agreement.getStipulateddate());            
            Config.pstmt.setString(19, agreement.getTimeextention1());            
            Config.pstmt.setString(20, agreement.getTimeextention2());            
            Config.pstmt.setString(21, agreement.getPayement_status());            
            

            int x = Config.pstmt.executeUpdate();
            
            if(x>0){
            return true;
            }else{
            return false;   
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to agreement in database 
   public boolean updAgreement(Agreement agreement) {
        try {
            Config.sql = "update agreement set"
                    + " agreementno = ?, "
                    + " agreementdate = ?, "
                    + " workorderno = ?, "
                    + " workordernodate = ?, "
                    + " sdrs = ?, "
                    + " shapeof = ?, "
                    + " shapeofdate = ?, "
                    + " nameofbank = ?, "
                    + " period = ?, "
                    + " formoftender = ?, "
                    + " negotiation = ?, "
                    + " acceptance = ?, "
                    + " letterno = ?, "
                    + " dt = ?, "
                    + " actualdateofworkstarted = ?, "
                    + " completionofworkdate = ?, "
                    + " stipulateddate = ?, "
                    + " timeextention1 = ?, "
                    + " timeextention2 = ?, "
                    + " payement_status = ? "
                    + " where agreementid = '"+agreement.getAgreementid()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            Config.pstmt.setString(1, agreement.getAgreementno());
            Config.pstmt.setString(2, agreement.getAgreementdate());
            Config.pstmt.setString(3, agreement.getWorkorderno());
            Config.pstmt.setString(4, agreement.getWorkordernodate());
            Config.pstmt.setString(5, agreement.getSdrs());
            Config.pstmt.setString(6, agreement.getShapeof());
            Config.pstmt.setString(7, agreement.getShapeofdate());
            Config.pstmt.setString(8, agreement.getNameofbank());
            Config.pstmt.setString(9, agreement.getPeriod());
            Config.pstmt.setString(10, agreement.getFormoftender());
            Config.pstmt.setString(11, agreement.getNegotiation());
            Config.pstmt.setString(12, agreement.getAcceptance());
            Config.pstmt.setString(13, agreement.getLetterno());            
            Config.pstmt.setString(14, agreement.getDt());  
            Config.pstmt.setString(15, agreement.getActualdateofworkstarted());            
            Config.pstmt.setString(16, agreement.getCompletionofworkdate());            
            Config.pstmt.setString(17, agreement.getStipulateddate());            
            Config.pstmt.setString(18, agreement.getTimeextention1());            
            Config.pstmt.setString(19, agreement.getTimeextention2());            
            Config.pstmt.setString(20, agreement.getPayement_status());            
          
            int x = Config.pstmt.executeUpdate();
            if(x>0){
            return true;
            }else{
            return false;   
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete agreementid in database
   public boolean delAgreement(String agreementid)
   {
        try {          
            Config.sql = "delete from agreement where agreementid = '"+agreementid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();

                if (x>0) {
                return true;
                }else{
                    return false;
                }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
//..........................update close list status...........................................
   
   public boolean updClose(String agreementid) {
        try {
            Config.sql = "update agreement set"
                    + " payement_status = ? "
                    + " where agreementid = '"+agreementid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            Config.pstmt.setString(1, "true");            
            int x = Config.pstmt.executeUpdate();
                if(x>0){
                    return true;
                }else{
                    return false;   
                }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
   public boolean updSetFalse(String agreementid) {
        try {
            Config.sql = "update agreement set"
                    + " payement_status = ? "
                    + " where agreementid = '"+agreementid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            Config.pstmt.setString(1, "false");            
            int x = Config.pstmt.executeUpdate();
                if(x>0){
                    return true;
                }else{
                    return false;   
                }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
}
