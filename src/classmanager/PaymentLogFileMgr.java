/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package classmanager;

import datamanager.Config;
import datamanager.PaymentLogFile;

/**
 *
 * @author akshay
 */
public class PaymentLogFileMgr {
    
    public boolean insPaymentLogFile(PaymentLogFile payment_log_file) {
        try {          
            Config.sql = "insert into payment_log_file ("                   
                    + "new_payment,"
                    + "old_payment,"
                    + "payment_type,"
                    + "agreementid,"
                    + "payment_sheet_id,"
                    + "_date)"
                    + "values (?,?,?,?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, payment_log_file.getNew_payment());
            Config.pstmt.setString(2, payment_log_file.getOld_payment());
            Config.pstmt.setString(3, payment_log_file.getPayment_type());
            Config.pstmt.setString(4, payment_log_file.getAgreementid());
            Config.pstmt.setString(5, payment_log_file.getPayment_sheet_id());
            Config.pstmt.setString(6, payment_log_file.getDate());
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                if(Config.paymentsheetmgr.updPaymentAmount(payment_log_file)){
                    return true;
                }else{
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to department in database 
   public boolean updPaymentLogFile(PaymentLogFile payment_log_file) {
        try {
            Config.sql = "update payment_log_file set"
                    + " new_payment = ?, "
                    + " old_payment = ? "
                    + " where log_file_id = '"+payment_log_file.getLog_file_id()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, payment_log_file.getNew_payment());
            Config.pstmt.setString(2, payment_log_file.getOld_payment());
            
            int x = Config.pstmt.executeUpdate();
            
            if (x>0) {
                if(Config.paymentsheetmgr.updPaymentAmount(payment_log_file)){
                    return true;
                }else{
                    return false;
                }    
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete department in database
   public boolean delPaymentLogFile(String log_file_id)
   {
        try {          
            Config.sql = "delete from payment_log_file where log_file_id= '"+log_file_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }

}
