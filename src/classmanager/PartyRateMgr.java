/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package classmanager;

import datamanager.PartyRate;
import datamanager.Config;
import java.util.ArrayList;

/**
 *
 * @author akki
 */
public class PartyRateMgr {
       //method to insert Boqentry in database
    public boolean insPartyRate(ArrayList<PartyRate> partyrate,String id) {
        try {   
            
            Config.sql = "Delete from partyrate where tenderentryid='"+id+"'";
												
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            int y = Config.pstmt.executeUpdate();
            int i = 0;
            int[]arr={};
            if (y>=0) {
            Config.sql = "insert into partyrate ("                   
                    + "customerid,"
                    + "rate,"
                    + "per,"
                    + "type,"
                    + "total,"
                    + "tenderentryid )"
                    + " values (?,?,?,?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            for(int j = 0; j < partyrate.size(); j++) {
            PartyRate rs = partyrate.get(j);
            Config.pstmt.setString(1, rs.getCustomerid());
            Config.pstmt.setString(2, rs.getRate());
            Config.pstmt.setString(3, rs.getPer());
            Config.pstmt.setString(4, rs.getType());
            Config.pstmt.setString(5, rs.getTotal());
            Config.pstmt.setString(6, id);
            Config.pstmt.addBatch();
            }
            arr=Config.pstmt.executeBatch();
            Config.pstmt.close();
            }
                            
             if(arr.length==partyrate.size()) {
                return true;
            } else {
                return false;
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }    
    } 
    
     //method to delete partyrate in database
   public boolean delPartyRate(String party_rate_id)
   {
        try {          
            Config.sql = "delete from partyrate where party_rate_id = '"+party_rate_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }    
}
