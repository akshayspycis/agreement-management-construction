/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package classmanager;

import datamanager.Agreement;
import datamanager.Config;
import datamanager.PaymentLogFile;
import datamanager.PaymentSheet;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author akshay
 */
public class PaymentSheetMgr {
    
      //method to insert payment_sheet in database
    public boolean insPaymentSheet(PaymentSheet payment_sheet) {
        try {          
            Config.sql = "insert into payment_sheet ("                   
                    + "running_bills,"
                    + "_date ,"
                    + "work_done,"
                    + "sd_cash,"
                    + "sd_bg,"
                    + "pg_cash ,"
                    + "pg_bg,"
                    + "income_tax,"
                    + "cost_of_bill,"
                    + "commercial_tax,"
                    + "labour_welfare,"
                    + "royalty_deduction,"
                    + "advance_deduction,"
                    + "time_dedution,"
                    + "time_extention,"
                    + "withheld_dedution,"
                    + "net_cheque,"
                    + "cheque_no,"
                    + "cheque_date,"
                    + "name_of_bank,"
                    + "remarks,"
                    + "status,"
                    + "agreementid)"
                    
                    + "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, payment_sheet.getRunning_bills());
            Config.pstmt.setString(2, payment_sheet.getDate());
            Config.pstmt.setString(3, payment_sheet.getWork_done());
            Config.pstmt.setString(4, payment_sheet.getSd_cash());
            Config.pstmt.setString(5, payment_sheet.getSd_bg());
            Config.pstmt.setString(6, payment_sheet.getPg_cash());
            Config.pstmt.setString(7, payment_sheet.getPg_bg());
            Config.pstmt.setString(8, payment_sheet.getIncome_tax());
            Config.pstmt.setString(9, payment_sheet.getCost_of_bill());
            Config.pstmt.setString(10, payment_sheet.getCommercial_tax());
            Config.pstmt.setString(11, payment_sheet.getLabour_welfare());
            Config.pstmt.setString(12, payment_sheet.getRoyalty_deduction());
            Config.pstmt.setString(13, payment_sheet.getAdvance_deduction());            
            Config.pstmt.setString(14, payment_sheet.getTime_deduction());  
            Config.pstmt.setString(15, payment_sheet.getTime_extention());  
            Config.pstmt.setString(16, payment_sheet.getWithheld_dedution());            
            Config.pstmt.setString(17, payment_sheet.getNet_cheque());            
            Config.pstmt.setString(18, payment_sheet.getCheque_no());            
            Config.pstmt.setString(19, payment_sheet.getCheque_date());            
            Config.pstmt.setString(20, payment_sheet.getName_of_bank());            
            Config.pstmt.setString(21, payment_sheet.getRemarks());            
            Config.pstmt.setString(22, payment_sheet.getStatus());            
            Config.pstmt.setString(23, payment_sheet.getAgreementid());            
                

            int x = Config.pstmt.executeUpdate();
            if(x>0){
                if(payment_sheet.getBg()!=null){
                    if(Config.bg_details_mgr.insBGDetails(payment_sheet.getBg(),null)){
                        return true;
                    }else{
                        return false;
                    }
                }else{
                    return true;
                }
            }else{
                    return false;   
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to payment_sheet in database 
   public boolean updPaymentSheet(PaymentSheet payment_sheet) {
        try {
            Config.sql = "update payment_sheet set"
                    + " running_bills = ?, "
                    + " _date = ?, "
                    + " work_done = ?, "
                    + " sd_cash = ?, "
                    + " sd_bg = ?, "
                    + " pg_cash = ?, "
                    + " pg_bg = ?, "
                    + " income_tax = ?, "
                    + " cost_of_bill = ?, "
                    + " commercial_tax = ?, "
                    + " labour_welfare = ?, "
                    + " royalty_deduction = ?, "
                    + " advance_deduction = ?, "
                    + " time_dedution = ?, "
                    + " time_extention = ?, "
                    + " withheld_dedution = ?, "
                    + " net_cheque = ?, "
                    + " cheque_no = ?, "
                    + " cheque_date = ?, "
                    + " name_of_bank = ? ,"
                    + " remarks = ? ,"
                    + " status = ? "
                    + " where payment_sheet_id = '"+payment_sheet.getPayment_sheet_id()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, payment_sheet.getRunning_bills());
            Config.pstmt.setString(2, payment_sheet.getDate());
            Config.pstmt.setString(3, payment_sheet.getWork_done());
            Config.pstmt.setString(4, payment_sheet.getSd_cash());
            Config.pstmt.setString(5, payment_sheet.getSd_bg());
            Config.pstmt.setString(6, payment_sheet.getPg_cash());
            Config.pstmt.setString(7, payment_sheet.getPg_bg());
            Config.pstmt.setString(8, payment_sheet.getIncome_tax());
            Config.pstmt.setString(9, payment_sheet.getCost_of_bill());
            Config.pstmt.setString(10,payment_sheet.getCommercial_tax());
            Config.pstmt.setString(11,payment_sheet.getLabour_welfare());
            Config.pstmt.setString(12,payment_sheet.getRoyalty_deduction());
            Config.pstmt.setString(13,payment_sheet.getAdvance_deduction());            
            Config.pstmt.setString(14,payment_sheet.getTime_deduction());  
            Config.pstmt.setString(15,payment_sheet.getTime_extention());  
            Config.pstmt.setString(16,payment_sheet.getWithheld_dedution());            
            Config.pstmt.setString(17,payment_sheet.getNet_cheque());            
            Config.pstmt.setString(18,payment_sheet.getCheque_no());            
            Config.pstmt.setString(19,payment_sheet.getCheque_date());            
            Config.pstmt.setString(20,payment_sheet.getName_of_bank());            
            Config.pstmt.setString(21,payment_sheet.getRemarks());            
            Config.pstmt.setString(22,payment_sheet.getStatus());            
                
            
            int x = Config.pstmt.executeUpdate();
            
            if(x>0){
                if (payment_sheet.getBg()!=null) {
                    if(Config.bg_details_mgr.insBGDetails(payment_sheet.getBg(),payment_sheet.getPayment_sheet_id())){
                        return true;
                    }else{
                        return false;
                    }
                } else {
                    return true;
                }
                 
            }else{
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete payment_sheetid in database
   public boolean delPaymentSheet(String payment_sheet_id)
   {
        try {          
            Config.sql = "delete from payment_sheet where payment_sheet_id = '"+payment_sheet_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
    
public boolean updPaymentAmount(PaymentLogFile payment_log_file) {
        try {

            String amount="";
            if(payment_log_file.getPayment_type().equals("S.D. CASH")){
                amount="sd_cash";
            }
            if(payment_log_file.getPayment_type().equals("S.D. BG")){
                amount="sd_bg";
            }
            if(payment_log_file.getPayment_type().equals("P.G. CASH")){
                amount="pg_cash";
            }
            if(payment_log_file.getPayment_type().equals("P.G. BG")){
                amount="pg_bg";
            }
            if(payment_log_file.getPayment_type().equals("ROYALTY DEDUCTION")){
                amount="royalty_deduction";
            }
            if(payment_log_file.getPayment_type().equals("TIME DEDUCTION")){
                amount="time_dedution";
            }
            if(payment_log_file.getPayment_type().equals("WITHHELD DEDUCTION")){
                amount="withheld_dedution";
            }
            System.out.println(amount);
            Config.sql = "update payment_sheet set "
                    + amount+" = ? "
                    + " where payment_sheet_id = '"+payment_log_file.getPayment_sheet_id()+"'";
            System.out.println(Config.sql);
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            Config.pstmt.setString(1, String.valueOf(Double.parseDouble(payment_log_file.getOld_payment())-Double.parseDouble(payment_log_file.getNew_payment())));
            int x = Config.pstmt.executeUpdate();
            if(x>0){
                if (checkCloseList(payment_log_file.getAgreementid()) && Config.agreementmgr.updClose(payment_log_file.getAgreementid())) {
                    return true;
                } else {
                    Config.agreementmgr.updSetFalse(payment_log_file.getAgreementid());
                    return true;
                }
            }else{
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------    

    public boolean checkCloseList(String agreementid) {
        try {
                Config.sql = "select * from payment_sheet where agreementid='"+agreementid+"' ";
                Config.rs = Config.stmt.executeQuery(Config.sql);
                System.out.println(Config.sql);
            while (Config.rs.next()) {                
                if(Double.parseDouble(Config.rs.getString("sd_cash"))!=0.0){
                   return false;
                }else if(Double.parseDouble(Config.rs.getString("sd_bg"))!=0.0){
                   return false;
                }else if(Double.parseDouble(Config.rs.getString("pg_cash"))!=0.0){
                   return false;
                }else if(Double.parseDouble(Config.rs.getString("pg_bg"))!=0.0){
                   return false;
                }else if(Double.parseDouble(Config.rs.getString("royalty_deduction"))!=0.0){
                   return false;
                }else if(Double.parseDouble(Config.rs.getString("time_dedution"))!=0.0){
                   return false;
                }else if(Double.parseDouble(Config.rs.getString("withheld_dedution"))!=0.0){
                   return false;
                }
            }
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }

    
        
}
