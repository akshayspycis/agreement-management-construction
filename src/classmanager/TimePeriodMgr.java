/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package classmanager;

import datamanager.Config;

/**
 *
 * @author akki
 */
public class TimePeriodMgr {
	    //method to insert time_period in database
    public boolean insTimePeriod(String time) {
        try {          
            Config.sql = "insert into time_period ("                   
                    + "time"
																				+")"
                    + "values (?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, time);
            int x = Config.pstmt.executeUpdate();
            if(x>0){
													Config.configmgr.loadTimePeriod();
												    return true;
            }else{
                return false;
            }
        } catch (Exception ex) {
          return false;
        }
    }
    
   //----------------------------------------------------------------------------------------------
   //method to delete time_periodid in database
   public boolean delTimePeriodid(String time)
   {
        try {          
            Config.sql = "delete from time_period where time = '"+time+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
													Config.configmgr.loadTimePeriod();
                return true;
                }else{
                    return true;
                }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }

}
