/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package classmanager;

import datamanager.Config;
import datamanager.Department;
import datamanager.Division;
import java.util.ArrayList;

/**
 *
 * @author akshay
 */
public class DivisionMgr {
    
        //method to insert division in database
    public boolean insDivision(Division division) {
        try {          
            Config.sql = "insert into division ("                   
                    + "departmentid,"
                    + "divisionname)"
                    + "values (?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, division.getDepartmentid());
            Config.pstmt.setString(2, division.getDivisionname());
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.configmgr.loadDivision();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to division in database 
   public boolean updDivision(Division division) {
        try {
            Config.sql = "update division set"
                    + " departmentid = ?, "
                    + " divisionname = ? "
                    + " where divisionid = '"+division.getDivisionid()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
          
            Config.pstmt.setString(1, division.getDepartmentid());
            Config.pstmt.setString(2, division.getDivisionname());            
            int x = Config.pstmt.executeUpdate();
            
            if (x>0) {
                Config.configmgr.loadDivision();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete division in database
   public boolean delDivision(String divisionid)
   {
        try {          
            Config.sql = "delete from division where divisionid= '"+divisionid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.configmgr.loadDivision();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
}