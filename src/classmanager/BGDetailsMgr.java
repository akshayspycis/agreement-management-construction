/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package classmanager;

import datamanager.BGDetails;
import datamanager.Config;
import java.util.ArrayList;

/**
 *
 * @author akshay
 */
public class BGDetailsMgr {
           //method to insert Boqentry in database
    public boolean insBGDetails(ArrayList<BGDetails> bgdetails,String id) {
        try {   
            if (id==null) {
                Config.sql = "Delete from bg_details where payment_sheet_id=(select max(payment_sheet_id) from payment_sheet)";
            } else {
                Config.sql = "Delete from bg_details where payment_sheet_id='"+id+"'";
            }
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            int y = Config.pstmt.executeUpdate();

            if(id==null){
                Config.sql = "insert into bg_details ("                   
                    + "payment_sheet_id,"
                    + "bg_no,"
                    + "bg_date,"
                    + "name_of_bank,"
                    + "amount,"
                    + "valid_date,"
                    + "amount_type,"
                    + "agreement_id)"
                    + " values ((select max(payment_sheet_id) from payment_sheet),?,?,?,?,?,?,?)";
            
            }else{
                Config.sql = "insert into bg_details ("                   
                    + "payment_sheet_id,"
                    + "bg_no,"
                    + "bg_date,"
                    + "name_of_bank,"
                    + "amount,"
                    + "valid_date,"
                    + "amount_type,"
                    + "agreement_id)"
                    + " values ('"+id+"',?,?,?,?,?,?,?)";
            }
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            for(int i = 0; i < bgdetails.size(); i++) {
            Config.pstmt.setString(1, bgdetails.get(i).getBg_no());
            Config.pstmt.setString(2, bgdetails.get(i).getBg_date());
            Config.pstmt.setString(3, bgdetails.get(i).getName_of_bank());
            Config.pstmt.setString(4, bgdetails.get(i).getAmount());
            Config.pstmt.setString(5, bgdetails.get(i).getValid_date());
            Config.pstmt.setString(6, bgdetails.get(i).getAmount_type());
            Config.pstmt.setString(7, bgdetails.get(i).getAgreement_id());
            Config.pstmt.addBatch();
            }
                System.out.println("size of"+bgdetails.size());
            int[]arr=Config.pstmt.executeBatch();
            Config.pstmt.close();
            if(arr.length==bgdetails.size()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }    
    } 
}
