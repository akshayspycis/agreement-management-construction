package classmanager;

import admin.AdminHome;
import admin.NewDepartment;
import admin.NewDivision;
import admin.ProductConfiguration;
import admin.ViewDepartment;
import admin.ViewDivision;
import admin.bank.NewBank;
import admin.bank.ViewBank;
import admin.customer.CustomerManagement;
import admin.customer.NewCustomer;
import admin.customer.ViewCustomer;
import datamanager.BankDetails;
import datamanager.Config;
import datamanager.CustomerProfile;
import datamanager.Department;
import datamanager.Division;
import datamanager.PaymentLogFile;
import datamanager.PaymentSheetFormula;
import java.awt.Image;
import java.awt.Toolkit;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import modules.Report.ReportManagement;
import modules.Search.BankSearch;
import modules.Search.DepartmentSearch;
import modules.Search.DivisionSearch;
import modules.Search.Search;
import modules.Search.TimeSearch;
import modules.agreement.AgreementDetails;
import modules.agreement.AgreementManagement;
import modules.agreement.NewAgreement;
import modules.agreement.ViewAgreement;
import modules.agreement.payment.ManualPayment;
import modules.agreement.payment.NewPayment;
import modules.agreement.payment.ReturnPayment;
import modules.agreement.payment.ViewPayment;
import modules.agreement.payment.ViewReturnPayment;
import modules.agreement.payment.bg.BG;
import modules.agreement.payment.bg.BGDetailsMgmt;
import modules.tenderentry.NewBOQEntry;
import modules.tenderentry.NewPartyNameEntry;
import modules.tenderentry.NewTenderEntry;
import modules.tenderentry.ViewBOQEntry;
import modules.tenderentry.ViewPartyNameEntry;
import modules.tenderentry.newViewTenderEntry;

/**
 *
 * @author AMS
 */
public class ConfigMgr {    

    public boolean loadDatabase() {
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            String userName = "root";
            String password = "root";
            String url = "jdbc:mysql://localhost:3307/shapersconstruction1";
            Config.conn = DriverManager.getConnection(url, userName, password);
            Config.stmt = Config.conn.createStatement();            
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public Image getLogo() {
        try {
          return Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/logo copy.png"));
        } catch (Exception ex) {
            ex.printStackTrace();
          return null;  
        }
    }
    
//    public boolean loadUserProfile() {
//        Config.configuserprofile = new ArrayList<UserProfile>();
//        try {
//            Config.sql = "select * from userprofile";
//            Config.rs = Config.stmt.executeQuery(Config.sql);
//            while (Config.rs.next()) {
//                UserProfile up = new UserProfile();
//                up.setUserid(Config.rs.getString("userid"));
//                up.setUsername(Config.rs.getString("username"));
//                up.setPassword(Config.rs.getString("password"));
//                up.setOwnername(Config.rs.getString("ownername"));
//                up.setGender(Config.rs.getString("gender"));
//                up.setContactno(Config.rs.getString("contactno"));
//                up.setOrganization(Config.rs.getString("organization"));
//                up.setTinno(Config.rs.getString("tinno"));
//                up.setAddress(Config.rs.getString("address"));
//                up.setLocality(Config.rs.getString("locality"));
//                up.setCity(Config.rs.getString("city"));
//                up.setPincode(Config.rs.getString("pincode"));
//                up.setState(Config.rs.getString("state"));
//                up.setEmail(Config.rs.getString("email"));
//                up.setFax(Config.rs.getString("fax"));
//                up.setWebsite(Config.rs.getString("website"));
//                up.setSequrityquestion(Config.rs.getString("sequrityquestion"));
//                up.setAnswer(Config.rs.getString("answer"));
//                up.setOther(Config.rs.getString("other"));
//                
//                Config.configuserprofile.add(up);
//            }                
//        } catch (SQLException ex) {
//            ex.printStackTrace();
//            return false;
//        }
//        return true;
//    }
//    
    public boolean loadDepartment() {
        Config.configdepartment = new ArrayList<Department>();
        try {
            Config.sql = "select * from department";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
                Department p = new Department();                
                p.setDepartmentid(Config.rs.getString("departmentid"));     
                p.setDepartmentname(Config.rs.getString("departmentname"));     
                p.setAbbreviation(Config.rs.getString("abbreviation"));     
                
                Config.configdepartment.add(p);
            }                
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
        Config.thread_dep = Config.configdepartment.size();
        return true;
    }
    
    public boolean loadDivision() {
        Config.configdivision = new ArrayList<Division>();
        try {
            Config.sql = "select * from division";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
                Division d = new Division();
                d.setDivisionid(Config.rs.getString("divisionid"));
                d.setDepartmentid(Config.rs.getString("departmentid"));
                d.setDivisionname(Config.rs.getString("divisionname"));
                
                Config.configdivision.add(d);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
        Config.thread_div = Config.configdivision.size();
        return true;
    }
     
     
    
    public boolean loadCustomerProfile() {
        Config.customerprofile = new ArrayList<CustomerProfile>();
        try {            
            Config.sql = "select * from customerprofile";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {                    
                
                CustomerProfile cp = new CustomerProfile();
                cp.setCustomerid(Config.rs.getString("customerid"));                
                cp.setCustomername(Config.rs.getString("customername"));
                cp.setGender(Config.rs.getString("gender"));
                cp.setContactno(Config.rs.getString("contactno"));
                cp.setOrganization(Config.rs.getString("organization"));
                cp.setTinno(Config.rs.getString("tinno"));
                cp.setAddress(Config.rs.getString("address"));
                cp.setLocality(Config.rs.getString("locality"));
                cp.setCity(Config.rs.getString("city"));
                cp.setPincode(Config.rs.getString("pincode"));
                cp.setState(Config.rs.getString("state"));
                cp.setEmail(Config.rs.getString("email"));
                cp.setFax(Config.rs.getString("fax"));
                cp.setWebsite(Config.rs.getString("website"));
                cp.setOther(Config.rs.getString("other"));                
                
                Config.customerprofile.add(cp);                
            }                
        } catch (SQLException ex) {
          ex.printStackTrace();
            return false;
        }
        return true;
    }
				
				
				    public boolean loadBankDetails() {
        Config.bank_details = new ArrayList<BankDetails>();
        try {            
            Config.sql = "select * from bankdetails";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {                    
                
                BankDetails cp = new BankDetails();
                cp.setBankdetails_id(Config.rs.getString("bankdetails_id"));                
                cp.setBank_name(Config.rs.getString("bank_name"));
                cp.setAddress(Config.rs.getString("address"));
                cp.setLocality(Config.rs.getString("locality"));
                cp.setCity(Config.rs.getString("city"));
                cp.setPincode(Config.rs.getString("pincode"));
                cp.setState(Config.rs.getString("state"));
                Config.bank_details.add(cp);                
            }                
        } catch (SQLException ex) {
          ex.printStackTrace();
            return false;
        }
        return true;
    }
								
								public boolean loadPaymentSheetFormula() {
        Config.payment_sheet_formula = new ArrayList<PaymentSheetFormula>();
        try {            
            Config.sql = "select * from payment_sheet_formula";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {                    
                PaymentSheetFormula cp = new PaymentSheetFormula();
                cp.setSd_cash(Config.rs.getString("sd_cash"));                
                cp.setSd_bg(Config.rs.getString("sd_bg"));
                cp.setPg_cash(Config.rs.getString("pg_cash"));
                cp.setPg_bg(Config.rs.getString("pg_bg"));
                cp.setIncome_tax(Config.rs.getString("income_tax"));
                cp.setCost_of_bill(Config.rs.getString("cost_of_bill"));
                cp.setCommercial_tax(Config.rs.getString("commercial_tax"));
																cp.setLabour_welfare(Config.rs.getString("labour_welfare"));
                Config.payment_sheet_formula.add(cp);                
            }                
        } catch (SQLException ex) {
          ex.printStackTrace();
            return false;
        }
        return true;
    }
								
								
				public boolean loadTimePeriod() {
        Config.time_period = new ArrayList<String>();
        try {            
            Config.sql = "select * from time_period";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {                    
																Config.time_period.add(Config.rs.getString("time"));
            }                
        } catch (SQLException ex) {
          ex.printStackTrace();
            return false;
        }
        return true;
    }
								
//    
//    public boolean loadSupplierProfile() {
//        Config.configsupplierprofile = new ArrayList<SupplierProfile>();
//        try {            
//            Config.sql = "select * from supplierprofile";
//            Config.rs = Config.stmt.executeQuery(Config.sql);
//            while (Config.rs.next()) {                    
//                
//                SupplierProfile cp = new SupplierProfile();
//                cp.setSupplierid(Config.rs.getString("supplierid"));                
//                cp.setSuppliername(Config.rs.getString("suppliername"));
//                cp.setGender(Config.rs.getString("gender"));
//                cp.setContactno(Config.rs.getString("contactno"));
//                cp.setOrganization(Config.rs.getString("organization"));
//                cp.setTinno(Config.rs.getString("tinno"));
//                cp.setAddress(Config.rs.getString("address"));
//                cp.setLocality(Config.rs.getString("locality"));
//                cp.setCity(Config.rs.getString("city"));
//                cp.setState(Config.rs.getString("state"));
//                cp.setPincode(Config.rs.getString("pincode"));
//                cp.setEmail(Config.rs.getString("email"));
//                cp.setFax(Config.rs.getString("fax"));
//                cp.setWebsite(Config.rs.getString("website"));
//                cp.setOther(Config.rs.getString("other"));
//                
//                Config.configsupplierprofile.add(cp);                
//            }                
//        } catch (SQLException ex) {
//          ex.printStackTrace();
//            return false;
//        }
//        return true;
//    }
//    
//    public boolean loadEmployeeProfile() {
//        Config.configemployeeprofile = new ArrayList<EmployeeProfile>();
//        try {            
//            Config.sql = "select * from employeeprofile";
//            Config.rs = Config.stmt.executeQuery(Config.sql);
//            while (Config.rs.next()) {
//                
//                EmployeeProfile ep = new EmployeeProfile();
//                ep.setEmployeeid(Config.rs.getString("employeeid"));
//                ep.setEmployeename(Config.rs.getString("employeename"));
//                ep.setGender(Config.rs.getString("gender"));
//                ep.setDob(Config.rs.getString("dob"));
//                ep.setAge(Config.rs.getString("age"));
//                ep.setFathername(Config.rs.getString("fathername"));
//                ep.setMaritalstatus(Config.rs.getString("maritalstatus"));
//                ep.setIdentitytype(Config.rs.getString("identitytype"));
//                ep.setIdentityno(Config.rs.getString("identityno"));
//                ep.setContactno(Config.rs.getString("contactno"));
//                ep.setAltcontactno(Config.rs.getString("altcontactno"));
//                ep.setAddress(Config.rs.getString("address"));
//                ep.setLocality(Config.rs.getString("locality"));
//                ep.setCity(Config.rs.getString("city"));
//                ep.setPincode(Config.rs.getString("pincode"));
//                ep.setState(Config.rs.getString("state"));
//                ep.setEmail(Config.rs.getString("email"));
//                ep.setSalary(Config.rs.getString("salary"));
//                ep.setJoiningdate(Config.rs.getString("joiningdate"));
//                ep.setLeavingdate(Config.rs.getString("leavingdate"));
//                ep.setOther(Config.rs.getString("other"));
//                
//                Config.configemployeeprofile.add(ep);                
//            }                
//        } catch (SQLException ex) {
//            ex.printStackTrace();
//            return false;
//        }
//        return true;
//    } 
//    
//     public boolean loadPayroll() {
//        Config.configpayroll = new ArrayList<PayRoll>();
//        try {            
//            Config.sql = "select * from payroll";
//            Config.rs = Config.stmt.executeQuery(Config.sql);
//            while (Config.rs.next()) {
//                
//                PayRoll ep = new PayRoll();
//                ep.setPayrollid(Config.rs.getString("payrollid"));
//                ep.setEmployeeid(Config.rs.getString("employeeid"));
//                ep.setMonth(Config.rs.getString("month"));
//                ep.setYear(Config.rs.getString("year"));
//                ep.setPayment(Config.rs.getString("payment"));
//                ep.setDate(Config.rs.getString("date"));
//                ep.setTime(Config.rs.getString("time"));
//                
//                Config.configpayroll.add(ep);                
//            }                
//        } catch (SQLException ex) {
//            ex.printStackTrace();
//            return false;
//        }
//        return true;
//    } 
//  
                                
                                
    public boolean loadClassManager() {
        try {
            Config.departmentmgr = new DepartmentMgr();
            Config.divisionmgr = new DivisionMgr();
            Config.tenderentrymgr = new TenderentryMgr();
            Config.boqentrymgr = new BOQEntryMgr();
            Config.agreementmgr = new AgreementMgr();
            Config.paymentsheetmgr = new PaymentSheetMgr();
            Config.customerprofilemgr = new CustomerProfileMgr();
            Config.partyratemgr = new PartyRateMgr();
            Config.bankdetailsmgr = new BankDetailsMgr();
            Config.payment_sheet_formula_mgr = new PaymentSheetFormulaMgr();
            Config.time_period_mgr = new TimePeriodMgr();
            Config.payment_log_file_mgr = new PaymentLogFileMgr();
            Config.bg_details_mgr = new BGDetailsMgr();
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean loadForms() {
        try {
            Config.adminhome = new AdminHome();
            Config.productconfiguration = new ProductConfiguration(null, true);
            Config.newtenderentry = new NewTenderEntry(null, true);
            Config.viewboqentry =new ViewBOQEntry(null, true);
            Config.newviewtenderentry =new newViewTenderEntry(null, true);
            Config.newboqentry =new NewBOQEntry(null, true);
            Config.viewboqentry =new ViewBOQEntry(null, true);
            Config.newdepartment =new NewDepartment(null, true);
            Config.viewdepartment =new ViewDepartment(null, true);
            Config.newdivision =new NewDivision(null, true);
            Config.viewdivision =new ViewDivision(null, true);
            Config.newagreement =new NewAgreement(null, true);
            Config.viewagreement =new ViewAgreement(null, true);
            Config.customermanagement = new CustomerManagement(null, true);
            Config.newcustomer = new NewCustomer(null, true);
            Config.viewcustomer = new ViewCustomer(null, true);
            Config.newpartynameentry = new NewPartyNameEntry(null, true);
            Config.viewpartynameentry = new ViewPartyNameEntry(null, true);
            Config.search = new Search(null, true);
            Config.bank_search = new BankSearch(null, true);
            Config.time_search = new TimeSearch(null, true);
            Config.department_search = new DepartmentSearch(null, true);
            Config.division_search = new DivisionSearch(null, true);
            Config.newbank = new NewBank(null, true);
            Config.viewbank = new ViewBank(null, true);
            Config.new_payment = new NewPayment(null, true);
            Config.view_payment = new ViewPayment(null, true);
            Config.manual_payment = new ManualPayment(null, true);
            Config.report_management = new ReportManagement(null, true);
            Config.return_payment = new ReturnPayment(null, true);
            Config.view_return_payment = new ViewReturnPayment(null, true);
            Config.agreement_management = new AgreementManagement(null, true);
            Config.agreement_details = new AgreementDetails(null, true);
            Config.bg_detials_mgmt = new BGDetailsMgmt(null, true);
            Config.bg = new BG(null, true);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }        
    }
      
}


