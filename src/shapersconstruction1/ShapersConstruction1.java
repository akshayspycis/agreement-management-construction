package shapersconstruction1;

import classmanager.ConfigMgr;
import datamanager.Config;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author AMS
 */
public class ShapersConstruction1 {

    Banner banner;

    public ShapersConstruction1() {
        banner = new Banner();
        Config.configmgr = new ConfigMgr();
    }    
    
    private void showbanner() {        
        banner.setVisible(true);
        banner.setBannerLabel("Checking System Configuration...");
    }
    
    private void connserver() {
        banner.setBannerLabel("Connecting To Server...");        
    }
    
    private void conndatabase() {
        banner.setBannerLabel("Checking Databse Connection...");
        if (!Config.configmgr.loadDatabase()) {
            JOptionPane.showMessageDialog(banner,"Database connectivity problem, product can not be launch.","Error : 10068.",JOptionPane.ERROR_MESSAGE);            
            System.exit(0);
        }
    }
    
    private void initcomponent() {
        banner.setBannerLabel("Initialising Components...");        
        int x=0;
//        if (Config.configmgr.loadUserProfile()){x++;} else {System.out.println("Error : 1, UserProfile loading problem.");}
          if (Config.configmgr.loadDepartment()){x++;} else {System.out.println("Error : 2, Department loading problem.");}
          if (Config.configmgr.loadDivision()){x++;} else {System.out.println("Error : 3, Division loading problem.");}
          if (Config.configmgr.loadCustomerProfile()){x++;} else {System.out.println("Error : 4, CustomerProfile loading problem.");}
          if (Config.configmgr.loadBankDetails()){x++;} else {System.out.println("Error : 6, Bank Details loading problem.");}
          if (Config.configmgr.loadPaymentSheetFormula()){x++;} else {System.out.println("Error : 7, Formula Details loading problem.");}
          if (Config.configmgr.loadTimePeriod()){x++;} else {System.out.println("Error : 7, Time Period loading problem.");}
          if (Config.configmgr.loadClassManager()){x++;} else {System.out.println("Error : 8, ClassManager loading problem.");}
          if (Config.configmgr.loadForms()){x++;} else {System.out.println("Error : 9, Forms loading problem.");}
          Config.check=true;
        if (x!=8) {
            JOptionPane.showMessageDialog(banner,"Product initialisation problem, product can not be launch.","Error.",JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
        
    }
    
    private void hidebanner() {
        banner.dispose();
        Config.adminhome.onloadReset();
        Config.adminhome.setVisible(true);
       
//        Config.login.setVisible(true);        
    }
     public void onLoadDate(){
//        Config.crm_management.onloadCrm();
//        Config.crm_management.onloadUserProfile();
    }
    
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ShapersConstruction1.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(ShapersConstruction1.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(ShapersConstruction1.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(ShapersConstruction1.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        ShapersConstruction1 obj = new ShapersConstruction1();
                try {            
            obj.showbanner();        
            Thread.sleep(1000);
            obj.connserver();
            Thread.sleep(1000);
            obj.conndatabase();
            Thread.sleep(1000);
            obj.initcomponent();
            Thread.sleep(1000);
            obj.hidebanner();            
        } catch (InterruptedException ex) {
            Logger.getLogger(ShapersConstruction1.class.getName()).log(Level.SEVERE, null, ex);
        } 
        
    }
}
