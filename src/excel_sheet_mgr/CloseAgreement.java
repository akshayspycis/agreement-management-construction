/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package excel_sheet_mgr;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.JFileChooser;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author akshay
 */
public class CloseAgreement {
       XSSFWorkbook workbook=null;
    XSSFSheet sheet =null;
    FileOutputStream out =null;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    public CloseAgreement() {
        //Blank workbook
        workbook = new XSSFWorkbook();
        //Create a blank sheet
        sheet= workbook.createSheet("Close Agreement");
        //This data needs to be written (Object[])
        try {
            JFileChooser chooser = new JFileChooser();
                        chooser.setCurrentDirectory(new java.io.File("."));
                        chooser.setDialogTitle("choosertitle");
                        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                        chooser.setAcceptAllFileFilterUsed(false);
                        if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                        System.out.println("getSelectedFile() : " + chooser.getSelectedFile());
                        }                                
                        File file = new File(chooser.getSelectedFile()+"\\"+sdf.format(Calendar.getInstance().getTime()));
                        System.out.println(file.toString());
                        if (!file.exists()) {
                            file.mkdirs();                
                        }
                String filename = file.toString()+"\\close_agreement_"+sdf.format(Calendar.getInstance().getTime())+".xlsx";
                out = new FileOutputStream(new File(filename));
        } catch (Exception e) {
        }
         
    }

    public boolean createReport( Object[][]object,String[]other_details){
                 Row row = null;
            Cell cell = null;
                //for row 1 .....................................
                    row = sheet.createRow(0);
                        //for cell 1........................
                              cell = (Cell) row.createCell(1);
                              cell.setCellValue("DEPARTMENT NAME");
                              cell.setCellStyle(setStyle());
                        //for cell 2........................
                              cell = (Cell) row.createCell(2);
                              cell.setCellValue(other_details[0]);
                              cell.setCellStyle(setTileNameStyle());
                              
                //....................................................

                //for row 2.....................................
                    row = sheet.createRow(1);
                        //for cell 1........................
                              cell = (Cell) row.createCell(1);
                              cell.setCellValue("DIVISION NAME");
                              cell.setCellStyle(setStyle());
                        //for cell 2........................
                              cell = (Cell) row.createCell(2);
                              cell.setCellValue(other_details[1]);
                              cell.setCellStyle(setTileNameStyle());
                //....................................................
                
                //for row 3.....................................
                    row = sheet.createRow(2);
                        //for cell 1........................
                              cell = (Cell) row.createCell(1);
                              cell.setCellValue("FORM DATE");
                              cell.setCellStyle(setStyle());
                        //for cell 2........................
                              cell = (Cell) row.createCell(2);
                              cell.setCellValue(other_details[2]);
                              cell.setCellStyle(setTileNameStyle());
                //....................................................              
                              
                //for row 4.....................................
                    row = sheet.createRow(3);
                        //for cell 1........................
                              cell = (Cell) row.createCell(1);
                              cell.setCellValue("TO DATE");
                              cell.setCellStyle(setStyle());
                        //for cell 2........................
                              cell = (Cell) row.createCell(2);
                              cell.setCellValue(other_details[3]);
                              cell.setCellStyle(setTileNameStyle());
                //....................................................                            
        
                //for row 5.....................................
                    row = sheet.createRow(4);
                //....................................................        
                    row = sheet.createRow(5);

                    cell = (Cell) row.createCell(0);
                    cell.setCellValue( "AGREEMENT NO.");
                    cell.setCellStyle(setStyle());
                    
                    cell = (Cell) row.createCell(1);
                    cell.setCellValue("AGREEMENT DATE");
                    cell.setCellStyle(setStyle());
                    
                    cell = (Cell) row.createCell(2);
                    cell.setCellValue("WORK ORDER NO.");
                    cell.setCellStyle(setStyle());
                    
                    cell = (Cell) row.createCell(3);
                    cell.setCellValue("WORK ORDER DATE");
                    cell.setCellStyle(setStyle());
                    
                    cell = (Cell) row.createCell(4);
                    cell.setCellValue("S.D.DIPOSITE ");
                    cell.setCellStyle(setStyle());
                
                //for all row
                for (int i = 0; i < object.length; i++) {
                    row = sheet.createRow(i+6);
                        cell = (Cell) row.createCell(0);
                        cell.setCellValue((String)object[i][0]);
                        sheet.autoSizeColumn(0);     
                    for (int j = 2; j < 7; j++) {
                            cell = (Cell) row.createCell(j-2);
                            cell.setCellValue((String)object[i][j]);
                            sheet.autoSizeColumn(j-2);     
                            cell.setCellStyle(setRowStyle());
                        }
                }        
            //Write the workbook in file system
        try
        {
            workbook.write(out);
            out.close();
            System.out.println("howtodoinjava_demo.xlsx written successfully on disk.");
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
        }
    
    public CellStyle setStyle() {
        CellStyle style = workbook.createCellStyle();
                XSSFFont fontHeader = workbook.createFont();
                fontHeader.setBoldweight((short)5);
                fontHeader.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
                fontHeader.setFontHeightInPoints((short)12);
                fontHeader.setColor(HSSFColor.BLACK.index);
                fontHeader.setFontName("GE Inspira Pitch"); 
                style.setBorderBottom((short)1);
                style.setBorderTop((short)1);
                style.setBorderLeft((short)1);
                style.setBorderRight((short)1);
                style.setFont(fontHeader);
                style.setAlignment(CellStyle.ALIGN_CENTER); 
                style.setWrapText(true);
        return style;
    }
    
    public CellStyle setTileNameStyle() {
        CellStyle style = workbook.createCellStyle();
                XSSFFont fontHeader = workbook.createFont();
                fontHeader.setBoldweight((short)5);
                fontHeader.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
                fontHeader.setFontHeightInPoints((short)10);
                fontHeader.setColor(HSSFColor.BLACK.index);
                fontHeader.setFontName("GE Inspira Pitch"); 
                style.setBorderBottom((short)1);
                style.setBorderTop((short)1);
                style.setBorderLeft((short)1);
                style.setBorderRight((short)1);
                style.setFont(fontHeader);
                style.setAlignment(CellStyle.ALIGN_CENTER); 
                style.setWrapText(true);
        return style;
    }
    
    
    public CellStyle setColumnStyle() {
        CellStyle style = workbook.createCellStyle();
                XSSFFont fontHeader = workbook.createFont();
                fontHeader.setBoldweight((short)5);
                fontHeader.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
                fontHeader.setFontHeightInPoints((short)10);
                fontHeader.setColor(HSSFColor.BLACK.index);
                fontHeader.setFontName("GE Inspira Pitch"); 
                style.setBorderBottom((short)1);
                style.setBorderTop((short)1);
                style.setBorderLeft((short)1);
                style.setBorderRight((short)1);
                style.setFont(fontHeader);
                style.setAlignment(CellStyle.ALIGN_CENTER); 
                style.setWrapText(true);
        return style;
    }
    
    public CellStyle setRowStyle() {
        CellStyle style = workbook.createCellStyle();
                XSSFFont fontHeader = workbook.createFont();
                fontHeader.setBoldweight((short)5);
                fontHeader.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
                fontHeader.setFontHeightInPoints((short)9);
                fontHeader.setColor(HSSFColor.BLACK.index);
                fontHeader.setFontName("GE Inspira Pitch"); 
                style.setBorderBottom((short)1);
                style.setBorderTop((short)1);
                style.setBorderLeft((short)1);
                style.setBorderRight((short)1);
                style.setFont(fontHeader);
                style.setAlignment(CellStyle.ALIGN_CENTER); 
                style.setWrapText(true);
        return style;
    }
        
}


