/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package excel_sheet_mgr;

import java.awt.Font;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.swing.JFileChooser;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author akshay
 */
public class WholeAgreemntReport {
    
    XSSFWorkbook workbook=null;
    XSSFSheet sheet =null;
    FileOutputStream out =null;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    public WholeAgreemntReport() {
        //Blank workbook
        workbook = new XSSFWorkbook();
        //Create a blank sheet
        sheet= workbook.createSheet("Agreement Report with Payment Bill");
        //This data needs to be written (Object[])
        try {
            JFileChooser chooser = new JFileChooser();
                        chooser.setCurrentDirectory(new java.io.File("."));
                        chooser.setDialogTitle("choosertitle");
                        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                        chooser.setAcceptAllFileFilterUsed(false);
                        if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                        System.out.println("getSelectedFile() : " + chooser.getSelectedFile());
                        }                                
                        File file = new File(chooser.getSelectedFile()+"\\"+sdf.format(Calendar.getInstance().getTime()));
                        System.out.println(file.toString());
                        if (!file.exists()) {
                            file.mkdirs();                
                        }
                String filename = file.toString()+"\\Agreement_with_bill_no_"+sdf.format(Calendar.getInstance().getTime())+".xlsx";
                out = new FileOutputStream(new File(filename));
        } catch (Exception e) {
        }
         
    }

    public boolean createReport( Object[][]object,String [] col, String [] other_details){
Row row = null;
            Cell cell = null;
                //for row 1 .....................................
                    row = sheet.createRow(0);
                        //for cell 1........................
                              cell = (Cell) row.createCell(1);
                              cell.setCellValue("DEPARTMENT NAME");
                              cell.setCellStyle(setStyle());
                              sheet.autoSizeColumn(2);     
                        //for cell 2........................
                              cell = (Cell) row.createCell(2);
                              cell.setCellValue(other_details[0]);
                              cell.setCellStyle(setTileNameStyle());
                              sheet.autoSizeColumn(3);     
                //....................................................

                //for row 2.....................................
                    row = sheet.createRow(1);
                        //for cell 1........................
                              cell = (Cell) row.createCell(1);
                              cell.setCellValue("DIVISION NAME");
                              cell.setCellStyle(setStyle());
                              sheet.autoSizeColumn(2);     
                        //for cell 2........................
                              cell = (Cell) row.createCell(2);
                              cell.setCellValue(other_details[1]);
                              cell.setCellStyle(setTileNameStyle());
                              sheet.autoSizeColumn(3);     
                //....................................................
                
                //for row 3.....................................
                    row = sheet.createRow(2);
                        //for cell 1........................
                              cell = (Cell) row.createCell(1);
                              cell.setCellValue("AGREEMNT NO");
                              cell.setCellStyle(setStyle());
                              sheet.autoSizeColumn(2);
                        //for cell 2........................
                              cell = (Cell) row.createCell(2);
                              cell.setCellValue(other_details[2]);
                              cell.setCellStyle(setTileNameStyle());
                              sheet.autoSizeColumn(3);
                //....................................................              
                              
                //for row 4.....................................
                    row = sheet.createRow(3);
                        //for cell 1........................
                              cell = (Cell) row.createCell(1);
                              cell.setCellValue("AMOUNT TYPE");
                              cell.setCellStyle(setStyle());
                              sheet.autoSizeColumn(2);
                        //for cell 2........................
                              cell = (Cell) row.createCell(2);
                              cell.setCellValue(other_details[3]);
                              cell.setCellStyle(setTileNameStyle());
                              sheet.autoSizeColumn(3);
                //....................................................                            
                
                //for row 5.....................................
                row = sheet.createRow(4);
                //....................................................        
                
                //for row 6.....................................
                    row = sheet.createRow(5);
                            for (int i = 2; i < col.length-2; i++) {
                                cell = (Cell) row.createCell(i-2);
                                cell.setCellValue((String)col[i]);
                                cell.setCellStyle(setColumnStyle());
                            }
                //....................................................                            
                              
                //for all row
                for (int i = 0; i < object.length; i++) {
                    row = sheet.createRow(i+6);
                    for (int j = 2; j < col.length-2; j++) {
                        if (j!=4) {
                            cell = (Cell) row.createCell(j-2);
                            cell.setCellValue((String)object[i][j]);
                            sheet.autoSizeColumn(j-2);     
                        } else {
                            cell = (Cell) row.createCell(j-2);
                            cell.setCellValue((String)object[i][j]);
                            sheet.setColumnWidth(j-2,10000);     
                        }
                            cell.setCellStyle(setRowStyle());
                        }
                }        
        //Write the workbook in file system
        try
        {
            workbook.write(out);
            out.close();
            System.out.println("howtodoinjava_demo.xlsx written successfully on disk.");
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
        }
    
 public CellStyle setStyle() {
        CellStyle style = workbook.createCellStyle();
                XSSFFont fontHeader = workbook.createFont();
                fontHeader.setBoldweight((short)5);
                fontHeader.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
                fontHeader.setFontHeightInPoints((short)12);
                fontHeader.setColor(HSSFColor.BLACK.index);
                fontHeader.setFontName("GE Inspira Pitch"); 
                style.setBorderBottom((short)1);
                style.setBorderTop((short)1);
                style.setBorderLeft((short)1);
                style.setBorderRight((short)1);
                style.setFont(fontHeader);
                style.setAlignment(CellStyle.ALIGN_CENTER); 
                style.setWrapText(true);
        return style;
    }
    
    public CellStyle setTileNameStyle() {
        CellStyle style = workbook.createCellStyle();
                XSSFFont fontHeader = workbook.createFont();
                fontHeader.setBoldweight((short)5);
                fontHeader.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
                fontHeader.setFontHeightInPoints((short)10);
                fontHeader.setColor(HSSFColor.BLACK.index);
                fontHeader.setFontName("GE Inspira Pitch"); 
                style.setBorderBottom((short)1);
                style.setBorderTop((short)1);
                style.setBorderLeft((short)1);
                style.setBorderRight((short)1);
                style.setFont(fontHeader);
                style.setAlignment(CellStyle.ALIGN_CENTER); 
                style.setWrapText(true);
        return style;
    }
    
    
    public CellStyle setColumnStyle() {
        CellStyle style = workbook.createCellStyle();
                XSSFFont fontHeader = workbook.createFont();
                fontHeader.setBoldweight((short)5);
                fontHeader.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
                fontHeader.setFontHeightInPoints((short)10);
                fontHeader.setColor(HSSFColor.BLACK.index);
                fontHeader.setFontName("GE Inspira Pitch"); 
                style.setBorderBottom((short)1);
                style.setBorderTop((short)1);
                style.setBorderLeft((short)1);
                style.setBorderRight((short)1);
                style.setFont(fontHeader);
                style.setAlignment(CellStyle.ALIGN_CENTER); 
                style.setWrapText(true);
        return style;
    }
    
    public CellStyle setRowStyle() {
        CellStyle style = workbook.createCellStyle();
                XSSFFont fontHeader = workbook.createFont();
                fontHeader.setBoldweight((short)5);
                fontHeader.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
                fontHeader.setFontHeightInPoints((short)9);
                fontHeader.setColor(HSSFColor.BLACK.index);
                fontHeader.setFontName("GE Inspira Pitch"); 
                style.setBorderBottom((short)1);
                style.setBorderTop((short)1);
                style.setBorderLeft((short)1);
                style.setBorderRight((short)1);
                style.setFont(fontHeader);
                style.setAlignment(CellStyle.ALIGN_CENTER); 
                style.setWrapText(true);
        return style;
    }
        
}


