/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modules.agreement;

import datamanager.Agreement;
import datamanager.Config;
import datamanager.TenderEntry;
import java.awt.Dimension;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import static javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

/**
 *
 * @author akshay
 */
public class ViewAgreement extends javax.swing.JDialog {
    
    int no=0;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    DefaultTableModel tbl_payment_sheet_model = null; 
    public ViewAgreement(java.awt.Frame parent, boolean modal) {
        
        super(parent, modal);
        initComponents();
         this.setLocationRelativeTo(null);
         setIconImage(Config.configmgr.getLogo());
         tbl_payment_sheet_model = (DefaultTableModel) tbl_paymentsheet.getModel();
         tbl_paymentsheet.setAutoResizeMode( JTable.AUTO_RESIZE_OFF );
         
    }

  
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel8 = new javax.swing.JPanel();
        jPanel131 = new javax.swing.JPanel();
        jSplitPane42 = new javax.swing.JSplitPane();
        jPanel132 = new javax.swing.JPanel();
        jPanel133 = new javax.swing.JPanel();
        jLabel791 = new javax.swing.JLabel();
        txt_agreemnetno = new javax.swing.JTextField();
        jLabel792 = new javax.swing.JLabel();
        jdc_agreementdate = new com.toedter.calendar.JDateChooser();
        jLabel793 = new javax.swing.JLabel();
        txt_workorderno = new javax.swing.JTextField();
        jdc_workorderdate = new com.toedter.calendar.JDateChooser();
        jLabel794 = new javax.swing.JLabel();
        jLabel795 = new javax.swing.JLabel();
        txt_sdrs = new javax.swing.JTextField();
        jLabel796 = new javax.swing.JLabel();
        jdc_sdrsdate = new com.toedter.calendar.JDateChooser();
        jLabel797 = new javax.swing.JLabel();
        txt_shape = new javax.swing.JTextField();
        jLabel798 = new javax.swing.JLabel();
        txt_period = new javax.swing.JTextField();
        jLabel799 = new javax.swing.JLabel();
        txt_nameofbank = new javax.swing.JTextField();
        jLabel800 = new javax.swing.JLabel();
        txt_fromoftender = new javax.swing.JTextField();
        jLabel801 = new javax.swing.JLabel();
        txt_negotiation = new javax.swing.JTextField();
        jLabel802 = new javax.swing.JLabel();
        txt_acceptance = new javax.swing.JTextField();
        jLabel803 = new javax.swing.JLabel();
        txt_letterno = new javax.swing.JTextField();
        jLabel804 = new javax.swing.JLabel();
        txt_dt = new javax.swing.JTextField();
        jLabel805 = new javax.swing.JLabel();
        jdc_workstarted = new com.toedter.calendar.JDateChooser();
        jLabel806 = new javax.swing.JLabel();
        jLabel807 = new javax.swing.JLabel();
        jLabel808 = new javax.swing.JLabel();
        txt_timeextention83 = new javax.swing.JTextField();
        jLabel809 = new javax.swing.JLabel();
        txt_timeextention84 = new javax.swing.JTextField();
        jdc_stipulateddateofcomplition = new com.toedter.calendar.JDateChooser();
        jdc_completionofwork = new com.toedter.calendar.JDateChooser();
        lbl_agreementid41 = new javax.swing.JLabel();
        btn_cancel = new javax.swing.JButton();
        btn_update = new javax.swing.JButton();
        btn_delete = new javax.swing.JButton();
        lbl_agreementid = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        txt_nitno = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jdc_nitdate = new com.toedter.calendar.JDateChooser();
        jLabel27 = new javax.swing.JLabel();
        txt_pac = new javax.swing.JTextField();
        jLabel28 = new javax.swing.JLabel();
        txt_percentage = new javax.swing.JTextField();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        txt_totalamount = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        txt_em_rs = new javax.swing.JTextField();
        jPanel6 = new javax.swing.JPanel();
        jdc_purchaseofdate = new com.toedter.calendar.JDateChooser();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jdc_submission = new com.toedter.calendar.JDateChooser();
        jLabel23 = new javax.swing.JLabel();
        jdc_openingtender = new com.toedter.calendar.JDateChooser();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jPanel130 = new javax.swing.JPanel();
        jButton3 = new javax.swing.JButton();
        btn_view = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbl_paymentsheet = new javax.swing.JTable();
        tbn_remove = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        jPanel3.setBackground(new java.awt.Color(58, 148, 175));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Payment Sheet of Agreement ");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addContainerGap())
        );

        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        jPanel131.setBackground(new java.awt.Color(255, 255, 255));

        jSplitPane42.setBackground(new java.awt.Color(255, 255, 255));
        jSplitPane42.setBorder(null);
        jSplitPane42.setDividerLocation(265);
        jSplitPane42.setDividerSize(1);

        jPanel132.setBackground(new java.awt.Color(255, 255, 255));

        jPanel133.setBackground(new java.awt.Color(255, 255, 255));
        jPanel133.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Agreement Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel791.setText("Agreement No.");

        jLabel792.setText("Date");

        jdc_agreementdate.setDateFormatString("dd-MM-yyyy");

        jLabel793.setText("Work Order No.");

        jdc_workorderdate.setDateFormatString("dd-MM-yyyy");

        jLabel794.setText("Date");

        jLabel795.setText("S.D.Deposite at the time Against");

        jLabel796.setText("Date");

        jdc_sdrsdate.setDateFormatString("dd-MM-yyyy");

        jLabel797.setText("Shape of F.D.R./ B.G. No.");

        jLabel798.setText("Period ");

        jLabel799.setText("Name of Bank ");

        txt_nameofbank.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_nameofbankActionPerformed(evt);
            }
        });
        txt_nameofbank.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_nameofbankKeyPressed(evt);
            }
        });

        jLabel800.setText("Form of Tender");

        jLabel801.setText("Negotiation ");

        jLabel802.setText("Acceptance from ENC/ CE / SE / EE /");

        jLabel803.setText("Letter No ");

        jLabel804.setText("Dt");

        jLabel805.setText("Actual Date of Work Started ");

        jdc_workstarted.setDateFormatString("dd-MM-yyyy");

        jLabel806.setText("Actual Date of Completion of work ");

        jLabel807.setText("Stipulated Date of Complition ");

        jLabel808.setText("Time Extention (1)");

        jLabel809.setText("(2)");

        jdc_stipulateddateofcomplition.setDateFormatString("dd-MM-yyyy");

        jdc_completionofwork.setDateFormatString("dd-MM-yyyy");

        lbl_agreementid41.setForeground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanel133Layout = new javax.swing.GroupLayout(jPanel133);
        jPanel133.setLayout(jPanel133Layout);
        jPanel133Layout.setHorizontalGroup(
            jPanel133Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel133Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel133Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel133Layout.createSequentialGroup()
                        .addGroup(jPanel133Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_agreemnetno)
                            .addComponent(txt_fromoftender)
                            .addComponent(txt_nameofbank)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel133Layout.createSequentialGroup()
                                .addGroup(jPanel133Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel133Layout.createSequentialGroup()
                                        .addComponent(jLabel795)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addComponent(txt_sdrs))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel133Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel797)
                                    .addComponent(txt_shape, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(txt_workorderno)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel133Layout.createSequentialGroup()
                                .addComponent(jLabel805)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel806)
                                .addGap(68, 68, 68))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel133Layout.createSequentialGroup()
                                .addGroup(jPanel133Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel802)
                                    .addComponent(txt_acceptance, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel133Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txt_letterno, javax.swing.GroupLayout.PREFERRED_SIZE, 261, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel803)))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel133Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addGroup(jPanel133Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel133Layout.createSequentialGroup()
                                        .addGroup(jPanel133Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel793)
                                            .addComponent(jLabel799)
                                            .addComponent(jLabel800)
                                            .addGroup(jPanel133Layout.createSequentialGroup()
                                                .addComponent(jLabel791)
                                                .addGap(65, 65, 65)
                                                .addComponent(lbl_agreementid41, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGap(298, 298, 298))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel133Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel809)
                                        .addComponent(jdc_completionofwork, javax.swing.GroupLayout.PREFERRED_SIZE, 261, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addGroup(jPanel133Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel133Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel133Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jdc_sdrsdate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGroup(jPanel133Layout.createSequentialGroup()
                                        .addGroup(jPanel133Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel792)
                                            .addComponent(jLabel794)
                                            .addComponent(jLabel801)
                                            .addComponent(jLabel796)
                                            .addComponent(jLabel798)
                                            .addComponent(jLabel804))
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addComponent(txt_negotiation)
                                    .addComponent(txt_period)
                                    .addComponent(jdc_workorderdate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jdc_agreementdate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(jPanel133Layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addGroup(jPanel133Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txt_dt)
                                    .addGroup(jPanel133Layout.createSequentialGroup()
                                        .addComponent(jLabel807)
                                        .addGap(0, 94, Short.MAX_VALUE))
                                    .addComponent(jdc_stipulateddateofcomplition, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                    .addGroup(jPanel133Layout.createSequentialGroup()
                        .addGroup(jPanel133Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txt_timeextention83, javax.swing.GroupLayout.DEFAULT_SIZE, 227, Short.MAX_VALUE)
                            .addComponent(jLabel808)
                            .addComponent(jdc_workstarted, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txt_timeextention84, javax.swing.GroupLayout.PREFERRED_SIZE, 508, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel133Layout.setVerticalGroup(
            jPanel133Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel133Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel133Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(jPanel133Layout.createSequentialGroup()
                        .addComponent(jLabel792)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jdc_agreementdate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel133Layout.createSequentialGroup()
                        .addComponent(lbl_agreementid41)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel791)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_agreemnetno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel133Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel133Layout.createSequentialGroup()
                        .addComponent(jLabel793)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_workorderno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel133Layout.createSequentialGroup()
                        .addComponent(jLabel794)
                        .addGap(7, 7, 7)
                        .addComponent(jdc_workorderdate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel133Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel133Layout.createSequentialGroup()
                        .addComponent(jLabel797)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_shape, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel133Layout.createSequentialGroup()
                        .addComponent(jLabel795)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_sdrs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel133Layout.createSequentialGroup()
                        .addComponent(jLabel796)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jdc_sdrsdate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(jPanel133Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel133Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel133Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel799)
                            .addComponent(jLabel798))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel133Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_nameofbank, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_period, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel133Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel800)
                            .addComponent(jLabel801))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel133Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_fromoftender, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_negotiation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel133Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel802)
                            .addComponent(jLabel803))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel133Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_letterno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_dt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_acceptance, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel806)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel133Layout.createSequentialGroup()
                        .addGap(97, 97, 97)
                        .addComponent(jLabel804)
                        .addGap(40, 40, 40)
                        .addGroup(jPanel133Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel805)
                            .addComponent(jLabel807))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                .addGroup(jPanel133Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jdc_completionofwork, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jdc_workstarted, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jdc_stipulateddateofcomplition, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 11, Short.MAX_VALUE)
                .addGroup(jPanel133Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel809)
                    .addComponent(jLabel808))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel133Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_timeextention83, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_timeextention84, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        btn_cancel.setText("Cancel");
        btn_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelActionPerformed(evt);
            }
        });

        btn_update.setText("Update");
        btn_update.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_updateActionPerformed(evt);
            }
        });

        btn_delete.setText("Delete");
        btn_delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_deleteActionPerformed(evt);
            }
        });

        lbl_agreementid.setBackground(new java.awt.Color(255, 255, 255));
        lbl_agreementid.setForeground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanel132Layout = new javax.swing.GroupLayout(jPanel132);
        jPanel132.setLayout(jPanel132Layout);
        jPanel132Layout.setHorizontalGroup(
            jPanel132Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel132Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel132Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel133, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel132Layout.createSequentialGroup()
                        .addComponent(btn_delete)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lbl_agreementid)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_update)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_cancel)))
                .addContainerGap())
        );

        jPanel132Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_cancel, btn_delete, lbl_agreementid});

        jPanel132Layout.setVerticalGroup(
            jPanel132Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel132Layout.createSequentialGroup()
                .addComponent(jPanel133, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 78, Short.MAX_VALUE)
                .addGroup(jPanel132Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_cancel)
                    .addComponent(btn_update)
                    .addComponent(btn_delete)
                    .addComponent(lbl_agreementid))
                .addContainerGap())
        );

        jPanel132Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_delete, lbl_agreementid});

        jSplitPane42.setRightComponent(jPanel132);

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Tender Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel6.setText("NIT No.");

        txt_nitno.setEditable(false);
        txt_nitno.setBackground(new java.awt.Color(255, 255, 255));

        jLabel7.setText("Date");

        jdc_nitdate.setDateFormatString("dd-MM-yyyy");

        jLabel27.setText("PAC");

        txt_pac.setEditable(false);
        txt_pac.setBackground(new java.awt.Color(255, 255, 255));

        jLabel28.setText(" X");

        txt_percentage.setEditable(false);
        txt_percentage.setBackground(new java.awt.Color(255, 255, 255));

        jLabel29.setText("Percentage");

        jLabel30.setText("Total Amount ");

        txt_totalamount.setEditable(false);
        txt_totalamount.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setText("Em Rs");

        txt_em_rs.setEditable(false);
        txt_em_rs.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_totalamount)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel27)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(txt_pac, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, 11, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel29)
                            .addComponent(txt_percentage, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txt_nitno, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel6))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel7)
                                    .addComponent(jdc_nitdate, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jLabel30)
                            .addComponent(jLabel1))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(txt_em_rs))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE, false)
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_nitno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jdc_nitdate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel27)
                    .addComponent(jLabel29))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_pac, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel28)
                    .addComponent(txt_percentage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel30)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_totalamount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_em_rs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Due Date", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jdc_purchaseofdate.setDateFormatString("dd-MM-yyyy");

        jLabel21.setText("Purchase of Tender");

        jLabel22.setText("Submission of Tender");

        jdc_submission.setDateFormatString("dd-MM-yyyy");

        jLabel23.setText("Opening of Tender");

        jdc_openingtender.setDateFormatString("dd-MM-yyyy");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jdc_purchaseofdate, javax.swing.GroupLayout.DEFAULT_SIZE, 212, Short.MAX_VALUE)
                    .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jdc_submission, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jdc_openingtender, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel21)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jdc_purchaseofdate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel22)
                .addGap(8, 8, 8)
                .addComponent(jdc_submission, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel23)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jdc_openingtender, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(15, Short.MAX_VALUE))
        );

        jButton1.setText("View");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Cancel");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jButton1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButton2)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jPanel4Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jButton1, jButton2});

        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 76, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2))
                .addContainerGap())
        );

        jSplitPane42.setLeftComponent(jPanel4);

        javax.swing.GroupLayout jPanel131Layout = new javax.swing.GroupLayout(jPanel131);
        jPanel131.setLayout(jPanel131Layout);
        jPanel131Layout.setHorizontalGroup(
            jPanel131Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane42, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        jPanel131Layout.setVerticalGroup(
            jPanel131Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane42, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel131, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel131, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Agreement Sheet", jPanel8);

        jPanel130.setBackground(new java.awt.Color(255, 255, 255));

        jButton3.setText("Cancel");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        btn_view.setText("View");
        btn_view.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_viewActionPerformed(evt);
            }
        });

        jButton5.setText("Add Payment");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        tbl_paymentsheet.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tbl_paymentsheet.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_paymentsheetMouseClicked(evt);
            }
        });
        tbl_paymentsheet.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbl_paymentsheetKeyPressed(evt);
            }
        });
        jScrollPane2.setViewportView(tbl_paymentsheet);
        popupMenu = new JPopupMenu();

        JMenuItem viewpayment = new JMenuItem("View Payment");
        JMenuItem delete_paument = new JMenuItem("Delete");

        popupMenu.add(viewpayment);
        popupMenu.add(delete_paument);
        tbl_paymentsheet.setComponentPopupMenu(popupMenu);

        viewpayment.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_viewActionPerformed(null);
            }
        });
        delete_paument.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tbn_removeActionPerformed(null);
            }
        });

        tbn_remove.setText("Remove");
        tbn_remove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tbn_removeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel130Layout = new javax.swing.GroupLayout(jPanel130);
        jPanel130.setLayout(jPanel130Layout);
        jPanel130Layout.setHorizontalGroup(
            jPanel130Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel130Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel130Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel130Layout.createSequentialGroup()
                        .addComponent(jButton5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(tbn_remove)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_view)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton3))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 1044, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel130Layout.setVerticalGroup(
            jPanel130Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel130Layout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 495, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel130Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton3)
                    .addComponent(btn_view)
                    .addComponent(jButton5)
                    .addComponent(tbn_remove))
                .addGap(0, 11, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Payment Sheet", jPanel130);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jTabbedPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane1))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_deleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_deleteActionPerformed
        try{
            if (Config.agreementmgr.delAgreement(lbl_agreementid.getText())) {
                JOptionPane.showMessageDialog(this, "Agreement file deleted successfully.", "Creation successful", JOptionPane.NO_OPTION);
                dispose();
            } else {
                JOptionPane.showMessageDialog(this, "Error in Agreement file deletion.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }catch(Exception e ){
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, "Error in Agreement file deletion.", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_deleteActionPerformed

    private void btn_updateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_updateActionPerformed
        try{
            Agreement a =new  Agreement();
            a.setAgreementid(lbl_agreementid.getText());
            a.setAgreementno(txt_agreemnetno.getText());
            try{
                a.setAgreementdate(sdf.format(jdc_agreementdate.getDate()));
            }catch(Exception e){
                a.setAgreementdate("");
            }
            a.setWorkorderno(txt_workorderno.getText());

            try{
                a.setWorkordernodate(sdf.format(jdc_workorderdate.getDate()));
            }catch(Exception e){
                a.setWorkordernodate("");
            }

            a.setSdrs(txt_sdrs.getText());
            a.setShapeof(txt_shape.getText());

            try{
                a.setShapeofdate(sdf.format(jdc_sdrsdate.getDate()));
            }catch(Exception e){
                a.setShapeofdate("");
            }

            a.setNameofbank(txt_nameofbank.getText());
            a.setPeriod(txt_period.getText());
            a.setFormoftender(txt_fromoftender.getText());
            a.setNegotiation(txt_negotiation.getText());
            a.setAcceptance(txt_acceptance.getText());
            a.setLetterno(txt_letterno.getText());
            a.setDt(txt_dt.getText());

            try{
                a.setActualdateofworkstarted(sdf.format(jdc_workstarted.getDate()));
            }catch(Exception e){
                a.setActualdateofworkstarted("");
            }

            try{
                a.setCompletionofworkdate(sdf.format(jdc_completionofwork.getDate()));
            }catch(Exception e){
                a.setCompletionofworkdate("");
            }

            try{
                a.setStipulateddate(sdf.format(jdc_stipulateddateofcomplition.getDate()));
            }catch(Exception e){
                a.setStipulateddate("");
            }

            a.setTimeextention1(txt_timeextention83.getText());
            a.setTimeextention2(txt_timeextention84.getText());
            if (Config.agreementmgr.updAgreement(a)) {
                JOptionPane.showMessageDialog(this, "Agreement file updated successfully.", "Creation successful", JOptionPane.NO_OPTION);
                dispose();
            } else {
                JOptionPane.showMessageDialog(this, "Error in Agreement file updation.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }catch(Exception e ){
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, "Error in Agreement file updatation.", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_updateActionPerformed

    private void btn_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelActionPerformed
        dispose();
    }//GEN-LAST:event_btn_cancelActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
          Config.newviewtenderentry.onloadReset(lbl_agreementid.getText());
          Config.newviewtenderentry.setVisible(true);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        Config.new_payment.onloadReset(lbl_agreementid.getText());
        Config.new_payment.setVisible(true);
    }//GEN-LAST:event_jButton5ActionPerformed

    private void btn_viewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_viewActionPerformed
        try {
            Config.view_payment.onloadReset(tbl_paymentsheet.getValueAt(tbl_paymentsheet.getSelectedRow(), 23).toString());
            Config.view_payment.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, "Select Any Rows", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_viewActionPerformed

    private void tbl_paymentsheetKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbl_paymentsheetKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tbl_paymentsheetKeyPressed

    private void tbl_paymentsheetMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_paymentsheetMouseClicked
       if(evt.getClickCount()==2){
           btn_viewActionPerformed(null);
       }
    }//GEN-LAST:event_tbl_paymentsheetMouseClicked

    private void tbn_removeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tbn_removeActionPerformed
        try {
            String id =tbl_paymentsheet.getValueAt(tbl_paymentsheet.getSelectedRow(), 23).toString();
            System.out.println(id);
        if(Config.paymentsheetmgr.delPaymentSheet(id)){
            Config.viewagreement.onloadPayment();
            JOptionPane.showMessageDialog(this, "Payment Detail Deleted successfully", "Success", JOptionPane.NO_OPTION);
        }else{
            JOptionPane.showMessageDialog(this, "Error in Deletion", "Error", JOptionPane.ERROR_MESSAGE);
        }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select Any Rows", "Error", JOptionPane.ERROR_MESSAGE);
        }
        
    }//GEN-LAST:event_tbn_removeActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        dispose();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void txt_nameofbankKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_nameofbankKeyPressed
        if(txt_nameofbank.getText().equals("")){
 	if(Character.isLetter(evt.getKeyChar())){
           Config.bank_search.onloadReset(6,String.valueOf(evt.getKeyChar()));
           Config.bank_search.setVisible(true);
	 }
      }else{
	if(Character.isLetter(evt.getKeyChar())||evt.getKeyCode()==KeyEvent.VK_BACK_SPACE &&evt.getKeyCode()!=KeyEvent.VK_BACK_SPACE ){
          Config.bank_search.onloadReset(6,txt_nameofbank.getText());
          Config.bank_search.setVisible(true);
	}
      }
    }//GEN-LAST:event_txt_nameofbankKeyPressed

    private void txt_nameofbankActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_nameofbankActionPerformed
        Config.bank_search.onloadReset(6,txt_nameofbank.getText());
	Config.bank_search.setVisible(true);
    }//GEN-LAST:event_txt_nameofbankActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_cancel;
    private javax.swing.JButton btn_delete;
    private javax.swing.JButton btn_update;
    private javax.swing.JButton btn_view;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton5;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel791;
    private javax.swing.JLabel jLabel792;
    private javax.swing.JLabel jLabel793;
    private javax.swing.JLabel jLabel794;
    private javax.swing.JLabel jLabel795;
    private javax.swing.JLabel jLabel796;
    private javax.swing.JLabel jLabel797;
    private javax.swing.JLabel jLabel798;
    private javax.swing.JLabel jLabel799;
    private javax.swing.JLabel jLabel800;
    private javax.swing.JLabel jLabel801;
    private javax.swing.JLabel jLabel802;
    private javax.swing.JLabel jLabel803;
    private javax.swing.JLabel jLabel804;
    private javax.swing.JLabel jLabel805;
    private javax.swing.JLabel jLabel806;
    private javax.swing.JLabel jLabel807;
    private javax.swing.JLabel jLabel808;
    private javax.swing.JLabel jLabel809;
    private javax.swing.JPanel jPanel130;
    private javax.swing.JPanel jPanel131;
    private javax.swing.JPanel jPanel132;
    private javax.swing.JPanel jPanel133;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSplitPane jSplitPane42;
    private javax.swing.JTabbedPane jTabbedPane1;
    private com.toedter.calendar.JDateChooser jdc_agreementdate;
    private com.toedter.calendar.JDateChooser jdc_completionofwork;
    private com.toedter.calendar.JDateChooser jdc_nitdate;
    private com.toedter.calendar.JDateChooser jdc_openingtender;
    private com.toedter.calendar.JDateChooser jdc_purchaseofdate;
    private com.toedter.calendar.JDateChooser jdc_sdrsdate;
    private com.toedter.calendar.JDateChooser jdc_stipulateddateofcomplition;
    private com.toedter.calendar.JDateChooser jdc_submission;
    private com.toedter.calendar.JDateChooser jdc_workorderdate;
    private com.toedter.calendar.JDateChooser jdc_workstarted;
    private javax.swing.JLabel lbl_agreementid;
    private javax.swing.JLabel lbl_agreementid41;
    private javax.swing.JTable tbl_paymentsheet;
    private javax.swing.JPopupMenu popupMenu;
    private javax.swing.JButton tbn_remove;
    private javax.swing.JTextField txt_acceptance;
    private javax.swing.JTextField txt_agreemnetno;
    private javax.swing.JTextField txt_dt;
    private javax.swing.JTextField txt_em_rs;
    private javax.swing.JTextField txt_fromoftender;
    private javax.swing.JTextField txt_letterno;
    private javax.swing.JTextField txt_nameofbank;
    private javax.swing.JTextField txt_negotiation;
    private javax.swing.JTextField txt_nitno;
    private javax.swing.JTextField txt_pac;
    private javax.swing.JTextField txt_percentage;
    private javax.swing.JTextField txt_period;
    private javax.swing.JTextField txt_sdrs;
    private javax.swing.JTextField txt_shape;
    private javax.swing.JTextField txt_timeextention83;
    private javax.swing.JTextField txt_timeextention84;
    private javax.swing.JTextField txt_totalamount;
    private javax.swing.JTextField txt_workorderno;
    // End of variables declaration//GEN-END:variables


      public void onloadReset(String agrimentid)
    {
        
      txt_agreemnetno.setText("");
      jdc_agreementdate.setDate(null);
      txt_workorderno.setText("");
      jdc_workorderdate.setDate(null);
      txt_sdrs.setText("");
      txt_shape.setText("");
      jdc_sdrsdate.setDate(null);
      txt_nameofbank.setText("");
      txt_period.setText("");
      txt_fromoftender.setText("");
      txt_negotiation.setText("");
      txt_acceptance.setText("");
      txt_letterno.setText("");
      txt_dt.setText("");
      jdc_workstarted.setDate(null);
      jdc_completionofwork.setDate(null);
      jdc_stipulateddateofcomplition.setDate(null);
      txt_timeextention83.setText("");
      txt_timeextention84.setText("");

        
      this.no=no;  
      lbl_agreementid.setText(agrimentid);
       try {
            String sql="select * from tenderentry where tenderentryid='"+lbl_agreementid.getText()+"'";
            Config.rs = Config.stmt.executeQuery(sql);
            while (Config.rs.next()) {
                txt_nitno.setText(Config.rs.getString("nitno"));
                try {
                jdc_nitdate.setDate(sdf.parse(Config.rs.getString("nitdate")));
                } catch (Exception e) {
                jdc_nitdate.setDate(null);
                }
                txt_pac.setText(Config.rs.getString("pac"));
                txt_percentage.setText(Config.rs.getString("percentage"));
                txt_totalamount.setText(Config.rs.getString("totalamount"));
                try {
                jdc_purchaseofdate.setDate(sdf.parse(Config.rs.getString("purchaseoftender")));
                } catch (Exception e) {
                jdc_purchaseofdate.setDate(null);
                }
                try {
                jdc_submission.setDate(sdf.parse(Config.rs.getString("submissionoftender")));
                } catch (Exception e) {
                jdc_submission.setDate(null);
                }
                try {
                jdc_openingtender.setDate(sdf.parse(Config.rs.getString("openingoftender")));
                } catch (Exception e) {
                jdc_openingtender.setDate(null);
                }
                txt_em_rs.setText(Config.rs.getString("emrs"));
            }
            }catch(Exception e){
                e.printStackTrace();
            }
        try {
            Config.sql = "select * from agreement where agreementid='"+lbl_agreementid.getText()+"' ";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
                lbl_agreementid.setText(Config.rs.getString("agreementid"));     
                
                txt_agreemnetno.setText(Config.rs.getString("agreementno"));     
                try {
                jdc_agreementdate.setDate(sdf.parse(Config.rs.getString("agreementdate")));
                } catch (Exception e) {
                jdc_purchaseofdate.setDate(null);
                }
                txt_workorderno.setText(Config.rs.getString("workorderno"));     
                try {
                jdc_workorderdate.setDate(sdf.parse(Config.rs.getString("workordernodate")));
                } catch (Exception e) {
                jdc_workorderdate.setDate(null);
                }
                txt_sdrs.setText(Config.rs.getString("sdrs"));     
                txt_shape.setText(Config.rs.getString("shapeof"));     
                try {
                jdc_sdrsdate.setDate(sdf.parse(Config.rs.getString("shapeofdate")));
                } catch (Exception e) {
                jdc_sdrsdate.setDate(null);
                }
                txt_nameofbank.setText(Config.rs.getString("nameofbank"));     
                txt_period.setText(Config.rs.getString("period"));     
                txt_fromoftender.setText(Config.rs.getString("formoftender"));     
                txt_negotiation.setText(Config.rs.getString("negotiation"));     
                txt_acceptance.setText(Config.rs.getString("acceptance"));     
                txt_letterno.setText(Config.rs.getString("letterno"));     
                txt_dt.setText(Config.rs.getString("dt"));     
                try {
                jdc_workstarted.setDate(sdf.parse(Config.rs.getString("actualdateofworkstarted")));
                } catch (Exception e) {
                jdc_workstarted.setDate(null);
                }
                try {
                jdc_completionofwork.setDate(sdf.parse(Config.rs.getString("completionofworkdate")));
                } catch (Exception e) {
                jdc_completionofwork.setDate(null);
                }
                try {
                jdc_stipulateddateofcomplition.setDate(sdf.parse(Config.rs.getString("stipulateddate")));
                } catch (Exception e) {
                jdc_stipulateddateofcomplition.setDate(null);
                }
                txt_timeextention83.setText(Config.rs.getString("timeextention1"));           
                txt_timeextention84.setText(Config.rs.getString("timeextention2")); 
                
            }                
        } catch (SQLException ex) {
            ex.printStackTrace();
        }    
       onloadPayment();
    }

    public void onloadPayment() {
        //................................................................SET VARIABLE..........................................................
        final boolean[]can = {false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,false};
        String []col = {"RUNNING BILLS NO.", "RUNNIG BILLS DATE", "GROSS VALUE OF WORKDONE", "S.D. CASH", "S.D. BG", "P.G. CASH", "P.G. BG", "INCOME TAX", "COST OF BILL", "COMMERCIAL TAX", "LABOUR WELFARE", "ROYALTY DEDUCTION", "ADVANCE DEDUCTION", "TIME DEDUCTION", "TIME EXTENSION", "WITHHELD DEDUCTION", "NET CHEQUE", " CHEQUE NO. ", " CHEQUE DATE ", "NAME OF BANK", "REMARKS", "STATUS", "","" };
        //...................................................................SET DATE ON CELL.....................................................
        System.out.println(col.length);
            try {
                Config.sql = "select * from payment_sheet where agreementid='"+lbl_agreementid.getText()+"' ";
                Config.rs = Config.stmt.executeQuery(Config.sql);
                Config.rs.beforeFirst();
                Config.rs.last();
                Object [][]obj = new Object[Config.rs.getRow()][can.length];
                ResultSetMetaData rd =Config.rs.getMetaData();
                int count=0;
                Config.rs.beforeFirst();
                
                while(Config.rs.next()){
                   obj[count][col.length-1]=Config.rs.getString(rd.getColumnLabel(1)); 
                    for (int i = 2; i <= col.length; i++)
                    {
                       obj[count][i-2]=Config.rs.getString(rd.getColumnLabel(i)); 
                    }
                    count++;
                }
                
                tbl_paymentsheet.setModel(new javax.swing.table.DefaultTableModel(obj,col) {
                    public boolean isCellEditable(int rowIndex, int columnIndex) {
                        return can [columnIndex];
                    }
                });
                
                int j=0;
                for ( j = 0; j <col.length-2; j++) {
                    tbl_paymentsheet.getTableHeader().getColumnModel().getColumn(j).setResizable(false);
                    tbl_paymentsheet.getTableHeader().getColumnModel().getColumn(j).setPreferredWidth(col[j].length()+100);
                }    
                tbl_paymentsheet.getTableHeader().getColumnModel().getColumn(j).setResizable(true);
                tbl_paymentsheet.getTableHeader().getColumnModel().getColumn(j).setPreferredWidth(0);
                tbl_paymentsheet.getTableHeader().getColumnModel().getColumn(j).setMinWidth(0);
                tbl_paymentsheet.getTableHeader().getColumnModel().getColumn(j).setMaxWidth(0);
                tbl_paymentsheet.getTableHeader().getColumnModel().getColumn(j+1).setResizable(true);
                tbl_paymentsheet.getTableHeader().getColumnModel().getColumn(j+1).setPreferredWidth(0);
                tbl_paymentsheet.getTableHeader().getColumnModel().getColumn(j+1).setMinWidth(0);
                tbl_paymentsheet.getTableHeader().getColumnModel().getColumn(j+1).setMaxWidth(0);
                
            }catch(Exception  e){
               e.printStackTrace();
            }
    }

    public void setBankDetails(String n) {
        txt_nameofbank.setText(n);
    }        
}

