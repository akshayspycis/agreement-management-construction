/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modules.agreement.payment;

import datamanager.BGDetails;
import datamanager.Config;
import datamanager.PaymentSheetFormula;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

/**
 *
 * @author akshay
 */
public class ManualPayment extends javax.swing.JDialog {
    
    ArrayList<BGDetails> bg = null;
    boolean  b=true;
    public ManualPayment(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        okButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txt_sd_cash = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txt_sd_bg = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txt_pg_cash = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txt_pg_bg = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txt_income_tax = new javax.swing.JTextField();
        txt_cost_of_bill = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txt_commercial_tax = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txt_labour_welfare = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        lbl_sd_bg_no = new javax.swing.JLabel();
        lbl_pg_bg_no = new javax.swing.JLabel();

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        okButton.setText("Save");
        okButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okButtonActionPerformed(evt);
            }
        });

        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Payment"));

        jLabel1.setText("SD Cash");

        txt_sd_cash.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_sd_cashFocusLost(evt);
            }
        });

        jLabel2.setText("SD BG");

        txt_sd_bg.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_sd_bgFocusLost(evt);
            }
        });

        jLabel3.setText("PG Cash");

        txt_pg_cash.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_pg_cashFocusLost(evt);
            }
        });

        jLabel4.setText("PG BG");

        txt_pg_bg.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_pg_bgFocusLost(evt);
            }
        });

        jLabel5.setText("Income Tax");

        txt_income_tax.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_income_taxFocusLost(evt);
            }
        });

        txt_cost_of_bill.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_cost_of_billFocusLost(evt);
            }
        });

        jLabel6.setText("Cost of Bill");

        jLabel7.setText("Commercial Tax");

        txt_commercial_tax.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_commercial_taxFocusLost(evt);
            }
        });

        jLabel8.setText("Labour Welfare");

        txt_labour_welfare.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_labour_welfareFocusLost(evt);
            }
        });

        jButton1.setText("BG");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("BG");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(txt_pg_cash, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
                                .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(txt_sd_cash))
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(txt_pg_bg)
                                    .addComponent(txt_sd_bg))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jButton1)
                                    .addComponent(jButton2, javax.swing.GroupLayout.Alignment.TRAILING)))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(jLabel2)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(lbl_sd_bg_no, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(jLabel4)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(lbl_pg_bg_no, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_income_tax, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel6)
                            .addComponent(txt_cost_of_bill, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_commercial_tax, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_labour_welfare, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8))))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(jLabel2))
                    .addComponent(lbl_sd_bg_no, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_sd_cash, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_sd_bg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(lbl_pg_bg_no, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_pg_cash, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_pg_bg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_income_tax, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_cost_of_bill, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(9, 9, 9)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_commercial_tax, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_labour_welfare, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(okButton, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cancelButton)
                .addContainerGap())
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {cancelButton, okButton});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cancelButton)
                    .addComponent(okButton))
                .addContainerGap())
        );

        getRootPane().setDefaultButton(okButton);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okButtonActionPerformed
        PaymentSheetFormula psf = new PaymentSheetFormula();
            psf.setSd_cash(txt_sd_cash.getText());
            psf.setSd_bg(txt_sd_bg.getText());
            psf.setPg_bg(txt_pg_bg.getText());
            psf.setPg_cash(txt_pg_cash.getText());
            psf.setIncome_tax(txt_income_tax.getText());
            psf.setCommercial_tax(txt_commercial_tax.getText());
            psf.setCost_of_bill(txt_cost_of_bill.getText());
            psf.setLabour_welfare(txt_labour_welfare.getText());
            psf.setBg_details(this.bg);
            if(b){
                Config.new_payment.setData(psf);
            }else{
                Config.view_payment.setData(psf);
            }
            dispose();
                
    }//GEN-LAST:event_okButtonActionPerformed

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed

    /**
     * Closes the dialog
     */
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        dispose();
    }//GEN-LAST:event_closeDialog

    private void txt_sd_cashFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_sd_cashFocusLost
        if(checkNumric(txt_sd_cash.getText())){
        }else{
            JOptionPane.showMessageDialog(this, "Fill inside the Numeric Value in SD Cash Amt", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_txt_sd_cashFocusLost

    private void txt_sd_bgFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_sd_bgFocusLost
        if(checkNumric(txt_sd_bg.getText())){
        }else{
            JOptionPane.showMessageDialog(this, "Fill inside the Numeric Value in Sd Bg Amt", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_txt_sd_bgFocusLost

    private void txt_pg_cashFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_pg_cashFocusLost
        if(checkNumric(txt_pg_cash.getText())){
        }else{
            JOptionPane.showMessageDialog(this, "Fill inside the Numeric Value in Pg Cash Amt", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_txt_pg_cashFocusLost

    private void txt_pg_bgFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_pg_bgFocusLost
        if(checkNumric(txt_pg_bg.getText())){
        }else{
            JOptionPane.showMessageDialog(this, "Fill inside the Numeric Value in Pg Bg Amt", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_txt_pg_bgFocusLost

    private void txt_income_taxFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_income_taxFocusLost
        if(checkNumric(txt_income_tax.getText())){
        }else{
            JOptionPane.showMessageDialog(this, "Fill inside the Numeric Value in Income Tax Amt", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_txt_income_taxFocusLost

    private void txt_cost_of_billFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_cost_of_billFocusLost
        if(checkNumric(txt_cost_of_bill.getText())){
        }else{
            JOptionPane.showMessageDialog(this, "Fill inside the Numeric Value in Cost of bill Amt", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_txt_cost_of_billFocusLost

    private void txt_commercial_taxFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_commercial_taxFocusLost
        if(checkNumric(txt_commercial_tax.getText())){
        }else{
            JOptionPane.showMessageDialog(this, "Fill inside the Numeric Value in Comme Amt", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_txt_commercial_taxFocusLost

    private void txt_labour_welfareFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_labour_welfareFocusLost
        if(checkNumric(txt_labour_welfare.getText())){
        }else{
            JOptionPane.showMessageDialog(this, "Fill inside the Numeric Value in Labour Welfare Amt", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_txt_labour_welfareFocusLost

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
         Config.bg_detials_mgmt.onloadReset(bg,1);
         Config.bg_detials_mgmt.setVisible(true);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
         Config.bg_detials_mgmt.onloadReset(bg,2);
         Config.bg_detials_mgmt.setVisible(true);
    }//GEN-LAST:event_jButton2ActionPerformed
    
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cancelButton;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lbl_pg_bg_no;
    private javax.swing.JLabel lbl_sd_bg_no;
    private javax.swing.JButton okButton;
    private javax.swing.JTextField txt_commercial_tax;
    private javax.swing.JTextField txt_cost_of_bill;
    private javax.swing.JTextField txt_income_tax;
    private javax.swing.JTextField txt_labour_welfare;
    private javax.swing.JTextField txt_pg_bg;
    private javax.swing.JTextField txt_pg_cash;
    private javax.swing.JTextField txt_sd_bg;
    private javax.swing.JTextField txt_sd_cash;
    // End of variables declaration//GEN-END:variables

    public void onloadReset(PaymentSheetFormula psf,boolean b) {
        bg=psf.getBg_details();
        this.b=b;
        txt_sd_cash.setText(psf.getSd_cash());
        txt_sd_bg.setText(psf.getSd_bg());
        txt_pg_cash.setText(psf.getPg_cash());
        txt_pg_bg.setText(psf.getPg_bg());
        txt_income_tax.setText(psf.getIncome_tax());
        txt_commercial_tax.setText(psf.getCommercial_tax());
        txt_labour_welfare.setText(psf.getLabour_welfare());
        txt_cost_of_bill.setText(psf.getCost_of_bill());
        
    }
    
        public boolean checkNumric(String str) {
        try {
            Double.parseDouble(str);
        } catch (Exception e) {
            return false;
        }
            return true;
        }

    public boolean addBgDetails(ArrayList<BGDetails> bg) {
        this.bg=bg;
        if(bg.get(0).getAmount_type().equals("SD.BG.")){
            Double d=0.0;
            for (int i = 0; i < bg.size(); i++) {
                d=d+Double.parseDouble(bg.get(i).getAmount());
            }
            txt_sd_bg.setText(String.valueOf(d));
            lbl_sd_bg_no.setText(String.valueOf(bg.size()));
            
        }else{
            Double d=0.0;
            for (int i = 0; i < bg.size(); i++) {
                d=d+Double.parseDouble(bg.get(i).getAmount());
            }
            txt_pg_bg.setText(String.valueOf(d));
            lbl_pg_bg_no.setText(String.valueOf(bg.size()));
        }
        return true;
    }
}
