/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modules.agreement.payment.bg;

import datamanager.BGDetails;
import datamanager.Config;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author akshay
 */
public class BGDetailsMgmt extends javax.swing.JDialog {
    boolean b = true;
    int i=0;
    DefaultTableModel tbl_bg_model = null;
    public BGDetailsMgmt(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        tbl_bg_model = (DefaultTableModel) tbl_bg.getModel();
        this.setLocationRelativeTo(null);
        this.setIconImage(Config.configmgr.getLogo());

        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        okButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_bg = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        lbl_amount_type = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        okButton.setText("ADD BG");
        okButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okButtonActionPerformed(evt);
            }
        });

        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        tbl_bg.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "BG NO.", "BG DATE", "NAME OF BANK", "AMOUNT", "VALID DATE"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tbl_bg);
        if (tbl_bg.getColumnModel().getColumnCount() > 0) {
            tbl_bg.getColumnModel().getColumn(0).setResizable(false);
            tbl_bg.getColumnModel().getColumn(0).setPreferredWidth(200);
            tbl_bg.getColumnModel().getColumn(1).setResizable(false);
            tbl_bg.getColumnModel().getColumn(1).setPreferredWidth(100);
            tbl_bg.getColumnModel().getColumn(2).setResizable(false);
            tbl_bg.getColumnModel().getColumn(2).setPreferredWidth(400);
            tbl_bg.getColumnModel().getColumn(3).setResizable(false);
            tbl_bg.getColumnModel().getColumn(3).setPreferredWidth(200);
            tbl_bg.getColumnModel().getColumn(4).setResizable(false);
            tbl_bg.getColumnModel().getColumn(4).setPreferredWidth(100);
        }

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 767, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 91, Short.MAX_VALUE)
        );

        jLabel1.setText("Amount Type :");

        jButton1.setText("Remove");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Save");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbl_amount_type, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(okButton, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cancelButton)
                .addContainerGap())
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbl_amount_type, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cancelButton)
                        .addComponent(okButton)
                        .addComponent(jButton1)
                        .addComponent(jButton2)))
                .addContainerGap())
        );

        getRootPane().setDefaultButton(okButton);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okButtonActionPerformed
        Config.bg.onloadReset();
        Config.bg.setVisible(true);
    }//GEN-LAST:event_okButtonActionPerformed

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed

    /**
     * Closes the dialog
     */
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        dispose();
    }//GEN-LAST:event_closeDialog

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        try {
            tbl_bg_model.removeRow(tbl_bg.getSelectedRow());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this,"Select Any Row", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        try {
            if(tbl_bg.getRowCount()>0){
                ArrayList<BGDetails> bg = new ArrayList<BGDetails>();
                for (int j = 0; j < tbl_bg.getRowCount(); j++) {
                   BGDetails b =new BGDetails();
                   b.setBg_no(tbl_bg.getValueAt(j, 0).toString());
                   b.setBg_date(tbl_bg.getValueAt(j, 1).toString());
                   b.setName_of_bank(tbl_bg.getValueAt(j, 2).toString());
                   b.setAmount(tbl_bg.getValueAt(j, 3).toString());
                   b.setValid_date(tbl_bg.getValueAt(j, 4).toString());
                   b.setAmount_type(lbl_amount_type.getText());
                   bg.add(b);
                }
                if (Config.manual_payment.addBgDetails(bg)) {
                    JOptionPane.showMessageDialog(this, "BG Details insert successfully.", "Creation successful", JOptionPane.NO_OPTION);
                    dispose();
                } else {
                    JOptionPane.showMessageDialog(this,"Error in sava", "Error", JOptionPane.ERROR_MESSAGE);
                }
                   
            }else{
                JOptionPane.showMessageDialog(this,"Insert the data before save", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this,"Error in Save details", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jButton2ActionPerformed
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cancelButton;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lbl_amount_type;
    private javax.swing.JButton okButton;
    private javax.swing.JTable tbl_bg;
    // End of variables declaration//GEN-END:variables

    public void onloadReset(ArrayList<BGDetails> bg, int i) {
        this.b=b;
        this.i=i;
        if(i==1){
                lbl_amount_type.setText("SD.BG.");
            }else{
                lbl_amount_type.setText("PG.BG.");
        }
        if(bg==null){
            tbl_bg_model.setRowCount(0);
        }else{
            System.out.println(bg.size());
            tbl_bg_model.setRowCount(0);
            if(i==1){
                for (int j = 0; j < bg.size(); j++) {
                if(bg.get(j).getAmount_type().equals("SD.BG."))
                tbl_bg_model.addRow(new Object[]{
                    bg.get(j).getBg_no(),
                    bg.get(j).getBg_date(),
                    bg.get(j).getName_of_bank(),
                    bg.get(j).getAmount(),
                    bg.get(j).getValid_date()
                });
            }
            }else{
                lbl_amount_type.setText("");
                for (int j = 0; j < bg.size(); j++) {
                if(bg.get(j).getAmount_type().equals("PG.BG."))
                tbl_bg_model.addRow(new Object[]{
                    bg.get(j).getBg_no(),
                    bg.get(j).getBg_date(),
                    bg.get(j).getName_of_bank(),
                    bg.get(j).getAmount(),
                    bg.get(j).getValid_date()
                });
            }
            }
        }
    }

    boolean insBg(BGDetails bg) {
        try {
            tbl_bg_model.addRow(new Object[]{
                bg.getBg_no(),
                bg.getBg_date(),
                bg.getName_of_bank(),
                bg.getAmount(),
                bg.getValid_date()
            });
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
