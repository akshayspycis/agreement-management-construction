/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modules.agreement.payment;

import datamanager.Agreement;
import datamanager.BGDetails;
import datamanager.Config;
import datamanager.PaymentSheet;
import datamanager.PaymentSheetFormula;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

/**
 *
 * @author akshay
 */
public class ViewPayment extends javax.swing.JDialog {

    String payment_sheet_id="";
    String agreementid="";
    PaymentSheetFormula psf=null;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    public ViewPayment(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setIconImage(Config.configmgr.getLogo());
        this.setLocationRelativeTo(null);
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cancelButton = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        okButton = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txt_running_bills = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jdc_payment_date = new com.toedter.calendar.JDateChooser();
        jLabel3 = new javax.swing.JLabel();
        txt_gross_value_of_work_done = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txt_royalty_deduction = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txt_time_deduction = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txt_advance_deduction = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        txt_net_cheque = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txt_cheque_no = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txt_name_of_bank = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jdc_cheque_date = new com.toedter.calendar.JDateChooser();
        jLabel14 = new javax.swing.JLabel();
        txt_remarks = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        txt_status = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        txt_withheld_deduction = new javax.swing.JTextField();
        jdc_time_extension = new com.toedter.calendar.JDateChooser();
        jButton3 = new javax.swing.JButton();

        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        jButton1.setText("Delete");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        okButton.setText("Update");
        okButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okButtonActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Payment Details"));

        jLabel1.setText("Running Bills ");

        jLabel2.setText("Date:");

        jdc_payment_date.setDateFormatString("dd-MM-yyyy");

        jLabel3.setText("Gross Value of Work Done");

        txt_gross_value_of_work_done.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_gross_value_of_work_doneFocusLost(evt);
            }
        });

        jLabel5.setText("Royalty Deduction");

        txt_royalty_deduction.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_royalty_deductionFocusLost(evt);
            }
        });

        jLabel6.setText("Time Deduction");

        txt_time_deduction.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_time_deductionFocusLost(evt);
            }
        });

        jLabel7.setText("Time Extension");

        jLabel8.setText("Advance Deduction");

        txt_advance_deduction.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_advance_deductionFocusLost(evt);
            }
        });

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Cheque Details"));

        jLabel9.setText("Net Cheque");

        txt_net_cheque.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_net_chequeFocusLost(evt);
            }
        });

        jLabel10.setText("Cheque No.");

        jLabel11.setText("Name of Bank");

        txt_name_of_bank.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_name_of_bankActionPerformed(evt);
            }
        });
        txt_name_of_bank.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_name_of_bankKeyPressed(evt);
            }
        });

        jLabel12.setText("Date");

        jdc_cheque_date.setDateFormatString("dd-MM-yyyy");

        jLabel14.setText("Remarks");

        jLabel15.setText("Status");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9)
                            .addComponent(txt_net_cheque, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel10)
                                .addGap(0, 68, Short.MAX_VALUE))
                            .addComponent(txt_cheque_no)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_name_of_bank, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE)
                            .addComponent(jLabel14, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_remarks, javax.swing.GroupLayout.Alignment.LEADING))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jdc_cheque_date, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel12)
                                    .addComponent(jLabel15))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(txt_status, javax.swing.GroupLayout.Alignment.TRAILING))))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_net_cheque, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_cheque_no, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(jLabel12))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_name_of_bank, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jdc_cheque_date, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(jLabel15))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_remarks, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_status, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel13.setText("Withheld Deduction");

        txt_withheld_deduction.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_withheld_deductionFocusLost(evt);
            }
        });

        jdc_time_extension.setDateFormatString("dd-MM-yyyy");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_withheld_deduction, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_advance_deduction)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel1)
                            .addComponent(jLabel3)
                            .addComponent(txt_running_bills, javax.swing.GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE)
                            .addComponent(txt_gross_value_of_work_done)
                            .addComponent(jLabel6)
                            .addComponent(txt_time_deduction))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_royalty_deduction)
                            .addComponent(jdc_payment_date, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel7)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel2))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(jdc_time_extension, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_running_bills, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jdc_payment_date, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel5))
                .addGap(4, 4, 4)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_gross_value_of_work_done, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_royalty_deduction, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_time_deduction, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jdc_time_extension, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_withheld_deduction, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_advance_deduction, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jButton3.setText("Manual Payment");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(okButton, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cancelButton)
                .addContainerGap())
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(11, 11, 11)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cancelButton)
                    .addComponent(okButton)
                    .addComponent(jButton1)
                    .addComponent(jButton3))
                .addContainerGap())
        );

        getRootPane().setDefaultButton(okButton);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Closes the dialog
     */
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        dispose();
    }//GEN-LAST:event_closeDialog

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okButtonActionPerformed
    String str =checkValidation();
        if (str.equals("ok")) {
            PaymentSheet p = new PaymentSheet();
            p.setPayment_sheet_id(payment_sheet_id);
            p.setRunning_bills(txt_running_bills.getText());
            
            try{
                p.setDate(sdf.format(jdc_payment_date.getDate()));
            }catch(Exception e){
                p.setDate("");
            }
            
            p.setWork_done(txt_gross_value_of_work_done.getText());
            
            if (psf==null) {
                p.setSd_cash(String.valueOf(Double.parseDouble(txt_gross_value_of_work_done.getText())*Double.parseDouble(Config.payment_sheet_formula.get(0).getSd_cash())/100));
                p.setSd_bg(String.valueOf(Double.parseDouble(txt_gross_value_of_work_done.getText())*Double.parseDouble(Config.payment_sheet_formula.get(0).getSd_bg())/100));
                p.setPg_cash(String.valueOf(Double.parseDouble(txt_gross_value_of_work_done.getText())*Double.parseDouble(Config.payment_sheet_formula.get(0).getPg_cash())/100));
                p.setPg_bg(String.valueOf(Double.parseDouble(txt_gross_value_of_work_done.getText())*Double.parseDouble(Config.payment_sheet_formula.get(0).getPg_cash())/100));
                p.setIncome_tax(String.valueOf(Double.parseDouble(txt_gross_value_of_work_done.getText())*Double.parseDouble(Config.payment_sheet_formula.get(0).getIncome_tax())/100));
                p.setCost_of_bill(String.valueOf(Double.parseDouble(txt_gross_value_of_work_done.getText())*Double.parseDouble(Config.payment_sheet_formula.get(0).getCost_of_bill())/100));
                p.setCommercial_tax(String.valueOf(Double.parseDouble(txt_gross_value_of_work_done.getText())*Double.parseDouble(Config.payment_sheet_formula.get(0).getCommercial_tax())/100));
                p.setLabour_welfare(String.valueOf(Double.parseDouble(txt_gross_value_of_work_done.getText())*Double.parseDouble(Config.payment_sheet_formula.get(0).getLabour_welfare())/100));
            } else {
                p.setSd_cash(psf.getSd_cash());
                p.setSd_bg(psf.getSd_bg());
                p.setPg_cash(psf.getPg_cash());
                p.setPg_bg(psf.getPg_bg());
                p.setIncome_tax(psf.getIncome_tax());
                p.setCost_of_bill(psf.getCost_of_bill());
                p.setCommercial_tax(psf.getCommercial_tax());
                p.setLabour_welfare(psf.getLabour_welfare());
            }
                p.setRoyalty_deduction(txt_royalty_deduction.getText());
                p.setAdvance_deduction(txt_advance_deduction.getText());
                p.setTime_deduction(txt_advance_deduction.getText());
            try{
                p.setTime_extention(sdf.format(jdc_time_extension.getDate()));
            }catch(Exception e){
                p.setTime_extention("");
            }

                p.setWithheld_dedution(txt_withheld_deduction.getText());
                p.setNet_cheque(txt_net_cheque.getText());
                p.setCheque_no(txt_cheque_no.getText());
            
            try{
                p.setCheque_date(sdf.format(jdc_cheque_date.getDate()));
            }catch(Exception e){
                p.setCheque_date("");
            }

                p.setName_of_bank(txt_name_of_bank.getText());
                p.setRemarks(txt_remarks.getText());
                p.setStatus(txt_status.getText());
                p.setAgreementid(agreementid);
                p.setPayment_sheet_id(payment_sheet_id);
                for (int i = 0; i < psf.getBg_details().size(); i++) {
                        psf.getBg_details().get(i).setAgreement_id(agreementid);
                        psf.getBg_details().get(i).setPayment_sheet_id(payment_sheet_id);
                }
                p.setBg(psf.getBg_details());
            if(Config.paymentsheetmgr.updPaymentSheet(p)) {
                Config.viewagreement.onloadPayment();
                JOptionPane.showMessageDialog(this, "Department Add successfully.", "Creation successful", JOptionPane.NO_OPTION);
                dispose();
            } else {
                JOptionPane.showMessageDialog(this, "Error in Department Add.", "Error", JOptionPane.ERROR_MESSAGE);
            }   
            } else {
                JOptionPane.showMessageDialog(this, "Department Name should not be blank.", "Error", JOptionPane.ERROR_MESSAGE);
            }

    }//GEN-LAST:event_okButtonActionPerformed

    private void txt_gross_value_of_work_doneFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_gross_value_of_work_doneFocusLost
        if(checkNumric(txt_gross_value_of_work_done.getText())){
        }else{
            JOptionPane.showMessageDialog(this, "Fill inside the Numeric Value in Work done Amt", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_txt_gross_value_of_work_doneFocusLost

    private void txt_royalty_deductionFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_royalty_deductionFocusLost
        if(checkNumric(txt_royalty_deduction.getText())){
        }else{
            JOptionPane.showMessageDialog(this, "Fill inside the Numeric Value in Royalty Amt", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_txt_royalty_deductionFocusLost

    private void txt_time_deductionFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_time_deductionFocusLost
        if(checkNumric(txt_time_deduction.getText())){
        }else{
            JOptionPane.showMessageDialog(this, "Fill inside the Numeric Value in time deduction Amt", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_txt_time_deductionFocusLost

    private void txt_advance_deductionFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_advance_deductionFocusLost
        if(checkNumric(txt_advance_deduction.getText())){
        }else{
            JOptionPane.showMessageDialog(this, "Fill inside the Numeric Value in Advance Amt", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_txt_advance_deductionFocusLost

    private void txt_net_chequeFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_net_chequeFocusLost
        if(checkNumric(txt_net_cheque.getText())){
        }else{
            JOptionPane.showMessageDialog(this, "Fill inside the Numeric Value in Net Cheque Amt", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_txt_net_chequeFocusLost

    private void txt_withheld_deductionFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_withheld_deductionFocusLost
        if(checkNumric(txt_withheld_deduction.getText())){
        }else{
            JOptionPane.showMessageDialog(this, "Fill inside the Numeric Value in with held Amt", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_txt_withheld_deductionFocusLost

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
            if(Config.paymentsheetmgr.delPaymentSheet(payment_sheet_id)){
                Config.viewagreement.onloadPayment();
                JOptionPane.showMessageDialog(this, "Payment Detail Deleted successfully", "Success", JOptionPane.NO_OPTION);
                dispose();
            }else{
                JOptionPane.showMessageDialog(this, "Error in Deletion", "Error", JOptionPane.ERROR_MESSAGE);
            }

    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
            Config.manual_payment.onloadReset(psf,false);
            Config.manual_payment.setVisible(true);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void txt_name_of_bankActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_name_of_bankActionPerformed
        Config.bank_search.onloadReset(4,txt_name_of_bank.getText());
        Config.bank_search.setVisible(true);
    }//GEN-LAST:event_txt_name_of_bankActionPerformed

    private void txt_name_of_bankKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_name_of_bankKeyPressed
        
      if(txt_name_of_bank.getText().equals("")){
        if(Character.isLetter(evt.getKeyChar())){
        Config.bank_search.onloadReset(4,String.valueOf(evt.getKeyChar()));
        Config.bank_search.setVisible(true);
        }
        }else{
        if(Character.isLetter(evt.getKeyChar())||evt.getKeyCode()==KeyEvent.VK_BACK_SPACE ){
        Config.bank_search.onloadReset(4,txt_name_of_bank.getText());
        Config.bank_search.setVisible(true);
        }
        }
    }//GEN-LAST:event_txt_name_of_bankKeyPressed
    
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cancelButton;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private com.toedter.calendar.JDateChooser jdc_cheque_date;
    private com.toedter.calendar.JDateChooser jdc_payment_date;
    private com.toedter.calendar.JDateChooser jdc_time_extension;
    private javax.swing.JButton okButton;
    private javax.swing.JTextField txt_advance_deduction;
    private javax.swing.JTextField txt_cheque_no;
    private javax.swing.JTextField txt_gross_value_of_work_done;
    private javax.swing.JTextField txt_name_of_bank;
    private javax.swing.JTextField txt_net_cheque;
    private javax.swing.JTextField txt_remarks;
    private javax.swing.JTextField txt_royalty_deduction;
    private javax.swing.JTextField txt_running_bills;
    private javax.swing.JTextField txt_status;
    private javax.swing.JTextField txt_time_deduction;
    private javax.swing.JTextField txt_withheld_deduction;
    // End of variables declaration//GEN-END:variables

private String checkValidation() {
        if(txt_running_bills.getText().equals("")){
        return "department";    
        }else if(jdc_payment_date.getDate()==null){
        return "department";            
        }else{
         return "ok";   
        }
    }    
    public boolean checkNumric(String str) {
        try {
            Double.parseDouble(str);
        } catch (Exception e) {
            return false;
        }
            return true;
    }

    public void onloadReset(String payment_sheet_id) {
        this.payment_sheet_id=payment_sheet_id;
        try {
            Config.sql = "select * from payment_sheet where payment_sheet_id='"+payment_sheet_id+"' ";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            if (Config.rs.next()) {
                agreementid=Config.rs.getString("agreementid");
                txt_running_bills.setText(Config.rs.getString("running_bills"));
                try {
                    jdc_payment_date.setDate(sdf.parse(Config.rs.getString("_date")));
                } catch (ParseException ex) {
                }
                txt_gross_value_of_work_done.setText(Config.rs.getString("work_done"));
                txt_royalty_deduction.setText(Config.rs.getString("royalty_deduction"));
                txt_time_deduction.setText(Config.rs.getString("time_dedution"));
                
                try {
                    jdc_time_extension.setDate(sdf.parse(Config.rs.getString("time_extention")));
                } catch (ParseException ex) {
                }
                txt_withheld_deduction.setText(Config.rs.getString("withheld_dedution"));
                txt_advance_deduction.setText(Config.rs.getString("advance_deduction"));
                txt_net_cheque.setText(Config.rs.getString("net_cheque"));
                txt_cheque_no.setText(Config.rs.getString("cheque_no"));
                txt_name_of_bank.setText(Config.rs.getString("name_of_bank"));
                
                try {
                    jdc_cheque_date.setDate(sdf.parse(Config.rs.getString("cheque_date")));
                } catch (ParseException ex) {
                }
                
                txt_remarks.setText(Config.rs.getString("remarks"));
                txt_status.setText(Config.rs.getString("status"));
                
                psf  = new PaymentSheetFormula();
                psf.setSd_cash(Config.rs.getString("sd_cash"));
                psf.setSd_bg(Config.rs.getString("sd_bg"));
                psf.setPg_cash(Config.rs.getString("pg_cash"));
                psf.setPg_bg(Config.rs.getString("pg_bg"));
                psf.setCommercial_tax(Config.rs.getString("commercial_tax"));
                psf.setCost_of_bill(Config.rs.getString("cost_of_bill"));
                psf.setIncome_tax(Config.rs.getString("income_tax"));
                psf.setLabour_welfare(Config.rs.getString("labour_welfare"));
                
            Config.sql = "select * from bg_details where payment_sheet_id='"+payment_sheet_id+"' ";
            Config.rs = Config.stmt.executeQuery(Config.sql);
                ArrayList<BGDetails> bg = new ArrayList<BGDetails>();
            while (Config.rs.next()) {
                BGDetails b =new BGDetails();
                b.setPayment_sheet_id(payment_sheet_id);
                b.setBg_no(Config.rs.getString("bg_no"));
                b.setBg_date(Config.rs.getString("bg_date"));
                b.setName_of_bank(Config.rs.getString("name_of_bank"));
                b.setAmount(Config.rs.getString("amount"));
                b.setValid_date(Config.rs.getString("valid_date"));
                b.setAmount_type(Config.rs.getString("amount_type"));
                b.setAgreement_id(Config.rs.getString("agreement_id"));
                bg.add(b);
              }
            psf.setBg_details(bg);
            }                
        } catch (SQLException ex) {
            ex.printStackTrace();
        }    
        
        
    }

    public void setData(PaymentSheetFormula psf) {
        this.psf = psf;
    }

    public void setBankDetails(String n) {
        txt_name_of_bank.setText(n);
    }
}
