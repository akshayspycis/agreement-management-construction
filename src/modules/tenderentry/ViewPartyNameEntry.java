/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modules.tenderentry;

import datamanager.Config;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.text.DecimalFormat;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

/**
 *
 * @author akshay
 */
public class ViewPartyNameEntry extends javax.swing.JDialog {
    int module=0;
    boolean check=true;
    String customerid="";
    public ViewPartyNameEntry(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        setIconImage(Config.configmgr.getLogo());
         String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
              dispose();
            }
        });
        
    }

   
    @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    btn_add = new javax.swing.JButton();
    btn_cencel = new javax.swing.JButton();
    txt_reset = new javax.swing.JButton();
    jPanel2 = new javax.swing.JPanel();
    jPanel5 = new javax.swing.JPanel();
    jLabel23 = new javax.swing.JLabel();
    txt_party = new javax.swing.JTextField();
    jLabel24 = new javax.swing.JLabel();
    txt_rate = new javax.swing.JTextField();
    jLabel25 = new javax.swing.JLabel();
    txt_per = new javax.swing.JTextField();
    jLabel26 = new javax.swing.JLabel();
    jLabel27 = new javax.swing.JLabel();
    txt_total = new javax.swing.JTextField();
    cb_type = new javax.swing.JComboBox();

    setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
    setTitle("View Party");

    btn_add.setText("Update");
    btn_add.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btn_addActionPerformed(evt);
      }
    });

    btn_cencel.setText("Cancel");
    btn_cencel.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btn_cencelActionPerformed(evt);
      }
    });

    txt_reset.setText("Reset");
    txt_reset.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        txt_resetActionPerformed(evt);
      }
    });

    jPanel2.setBackground(new java.awt.Color(255, 255, 255));

    jPanel5.setBackground(new java.awt.Color(255, 255, 255));
    jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Party Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

    jLabel23.setText("Party Name");

    txt_party.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        txt_partyActionPerformed(evt);
      }
    });
    txt_party.addKeyListener(new java.awt.event.KeyAdapter() {
      public void keyPressed(java.awt.event.KeyEvent evt) {
        txt_partyKeyPressed(evt);
      }
    });

    jLabel24.setText("Amount (PAC)");

    txt_rate.addCaretListener(new javax.swing.event.CaretListener() {
      public void caretUpdate(javax.swing.event.CaretEvent evt) {
        txt_rateCaretUpdate(evt);
      }
    });
    txt_rate.addFocusListener(new java.awt.event.FocusAdapter() {
      public void focusLost(java.awt.event.FocusEvent evt) {
        txt_rateFocusLost(evt);
      }
    });

    jLabel25.setText("Per(%)");

    txt_per.addFocusListener(new java.awt.event.FocusAdapter() {
      public void focusLost(java.awt.event.FocusEvent evt) {
        txt_perFocusLost(evt);
      }
    });

    jLabel26.setText("Type");

    jLabel27.setText("Total Amount");

    txt_total.addCaretListener(new javax.swing.event.CaretListener() {
      public void caretUpdate(javax.swing.event.CaretEvent evt) {
        txt_totalCaretUpdate(evt);
      }
    });

    cb_type.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Above", "Below" }));
    cb_type.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        cb_typeActionPerformed(evt);
      }
    });

    javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
    jPanel5.setLayout(jPanel5Layout);
    jPanel5Layout.setHorizontalGroup(
      jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel5Layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addGroup(jPanel5Layout.createSequentialGroup()
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
              .addComponent(txt_rate)
              .addComponent(jLabel24)
              .addComponent(jLabel26)
              .addComponent(cb_type, 0, 170, Short.MAX_VALUE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
              .addComponent(jLabel27)
              .addComponent(jLabel25)
              .addComponent(txt_total, javax.swing.GroupLayout.DEFAULT_SIZE, 169, Short.MAX_VALUE)
              .addComponent(txt_per)))
          .addComponent(jLabel23)
          .addComponent(txt_party, javax.swing.GroupLayout.PREFERRED_SIZE, 350, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );
    jPanel5Layout.setVerticalGroup(
      jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel5Layout.createSequentialGroup()
        .addContainerGap()
        .addComponent(jLabel23)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(txt_party, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jLabel24)
          .addComponent(jLabel25))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(txt_rate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(txt_per, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jLabel26)
          .addComponent(jLabel27))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(txt_total, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(cb_type, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );

    javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
    jPanel2.setLayout(jPanel2Layout);
    jPanel2Layout.setHorizontalGroup(
      jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel2Layout.createSequentialGroup()
        .addContainerGap()
        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );
    jPanel2Layout.setVerticalGroup(
      jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel2Layout.createSequentialGroup()
        .addContainerGap()
        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(layout.createSequentialGroup()
        .addContainerGap()
        .addComponent(txt_reset)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        .addComponent(btn_add)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(btn_cencel)
        .addContainerGap())
      .addGroup(layout.createSequentialGroup()
        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addGap(0, 0, Short.MAX_VALUE))
    );

    layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_cencel, txt_reset});

    layout.setVerticalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(layout.createSequentialGroup()
        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addGap(11, 11, 11)
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(txt_reset)
          .addComponent(btn_cencel)
          .addComponent(btn_add))
        .addContainerGap())
    );

    pack();
  }// </editor-fold>//GEN-END:initComponents

    private void btn_addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_addActionPerformed
    
    String[] str=new String[6];
    str[0]=txt_party.getText();
    str[1]=txt_rate.getText();
    str[2]=txt_per.getText();
    str[3]=cb_type.getSelectedItem().toString();
    str[4]=txt_total.getText();
    str[5]=customerid;
    switch(module){
          case 1:
              Config.newtenderentry.updateRowParty(str);
          break;
          case 2:
              Config.newviewtenderentry.updateRowParty(str);
          break;
          case 3:
          break;
	}
        dispose();
    }//GEN-LAST:event_btn_addActionPerformed

    private void btn_cencelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cencelActionPerformed
        dispose();
    }//GEN-LAST:event_btn_cencelActionPerformed

    private void txt_resetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_resetActionPerformed
       
    }//GEN-LAST:event_txt_resetActionPerformed

    private void txt_rateCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_rateCaretUpdate

    }//GEN-LAST:event_txt_rateCaretUpdate

    private void txt_totalCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_totalCaretUpdate
        
    }//GEN-LAST:event_txt_totalCaretUpdate

    private void txt_rateFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_rateFocusLost
        cal_pac();
    }//GEN-LAST:event_txt_rateFocusLost

    private void txt_perFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_perFocusLost
        cal_pac();
    }//GEN-LAST:event_txt_perFocusLost

    private void cb_typeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_typeActionPerformed
        cal_pac();
    }//GEN-LAST:event_cb_typeActionPerformed

    private void txt_partyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_partyActionPerformed
        Config.search.onloadReset(2,txt_party.getText());
        Config.search.setVisible(true);
    }//GEN-LAST:event_txt_partyActionPerformed

  private void txt_partyKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_partyKeyPressed
      customerid="";
      if(txt_party.getText().equals("")){
        if(Character.isLetter(evt.getKeyChar())){
          Config.search.onloadReset(2,String.valueOf(evt.getKeyChar()));
          Config.search.setVisible(true);
        }
      }else{
        if(Character.isLetter(evt.getKeyChar())||evt.getKeyCode()==KeyEvent.VK_BACK_SPACE &&evt.getKeyCode()!=KeyEvent.VK_BACK_SPACE ){
          Config.search.onloadReset( 2,txt_party.getText());
          Config.search.setVisible(true);
        }
      }
  }//GEN-LAST:event_txt_partyKeyPressed

    
   
  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JButton btn_add;
  private javax.swing.JButton btn_cencel;
  private javax.swing.JComboBox cb_type;
  private javax.swing.JLabel jLabel23;
  private javax.swing.JLabel jLabel24;
  private javax.swing.JLabel jLabel25;
  private javax.swing.JLabel jLabel26;
  private javax.swing.JLabel jLabel27;
  private javax.swing.JPanel jPanel2;
  private javax.swing.JPanel jPanel5;
  private javax.swing.JTextField txt_party;
  private javax.swing.JTextField txt_per;
  private javax.swing.JTextField txt_rate;
  private javax.swing.JButton txt_reset;
  private javax.swing.JTextField txt_total;
  // End of variables declaration//GEN-END:variables
public void onloadReset(String [] view,int module){
    this.module=module;
    txt_party.setText(view[0]);
    txt_rate.setText(view[1]);
    txt_per.setText(view[2]);
    cb_type.setSelectedItem(view[3]);
    txt_total.setText(view[4]);
}

private void cal_pac() {
        Double poc=0.0;
        Double per=0.0;
        try {
            poc =Double.parseDouble(txt_rate.getText());
        } catch (Exception e) {
            poc=0.0;
        }
        try {
            per =Double.parseDouble(txt_per.getText());
        } catch (Exception e) {
            per=0.0;
        }
        
        if(cb_type.getSelectedIndex()==0){
            txt_total.setText(new DecimalFormat("#.##").format((poc+(poc*per)/100)));
        }else{
            txt_total.setText(new DecimalFormat("#.##").format((poc-(poc*per)/100)));
        }
        txt_rate.setText(new DecimalFormat("#.##").format(poc));
        txt_per.setText(new DecimalFormat("#.##").format(per));
    }

   public void setData(String n) {
        txt_party.setText(n);
    }
}
