package modules.tenderentry;

import datamanager.Config;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.text.DecimalFormat;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;


public class NewPartyNameEntry extends javax.swing.JDialog {
    int module=0;
    String customerid="";
 
    public NewPartyNameEntry(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
           this.setLocationRelativeTo(null);
           setIconImage(Config.configmgr.getLogo());
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
              dispose();
            }
        });
    }

   
   
    @SuppressWarnings("unchecked")
 // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
 private void initComponents() {

  jPanel1 = new javax.swing.JPanel();
  jPanel4 = new javax.swing.JPanel();
  jLabel18 = new javax.swing.JLabel();
  txt_party = new javax.swing.JTextField();
  jLabel19 = new javax.swing.JLabel();
  txt_rate = new javax.swing.JTextField();
  jLabel20 = new javax.swing.JLabel();
  txt_per = new javax.swing.JTextField();
  jLabel21 = new javax.swing.JLabel();
  jLabel22 = new javax.swing.JLabel();
  txt_total = new javax.swing.JTextField();
  cb_type = new javax.swing.JComboBox();
  btn_cencel = new javax.swing.JButton();
  btn_add = new javax.swing.JButton();
  txt_reset = new javax.swing.JButton();

  setTitle("New Party");
  addWindowListener(new java.awt.event.WindowAdapter() {
   public void windowClosing(java.awt.event.WindowEvent evt) {
    closeDialog(evt);
   }
  });

  jPanel1.setBackground(new java.awt.Color(255, 255, 255));

  jPanel4.setBackground(new java.awt.Color(255, 255, 255));
  jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Party Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

  jLabel18.setText("Party Name");

  txt_party.addActionListener(new java.awt.event.ActionListener() {
   public void actionPerformed(java.awt.event.ActionEvent evt) {
    txt_partyActionPerformed(evt);
   }
  });
  txt_party.addKeyListener(new java.awt.event.KeyAdapter() {
   public void keyPressed(java.awt.event.KeyEvent evt) {
    txt_partyKeyPressed(evt);
   }
  });

  jLabel19.setText("Amount (PAC)");

  txt_rate.addCaretListener(new javax.swing.event.CaretListener() {
   public void caretUpdate(javax.swing.event.CaretEvent evt) {
    txt_rateCaretUpdate(evt);
   }
  });
  txt_rate.addFocusListener(new java.awt.event.FocusAdapter() {
   public void focusLost(java.awt.event.FocusEvent evt) {
    txt_rateFocusLost(evt);
   }
  });

  jLabel20.setText("Per(%)");

  txt_per.addFocusListener(new java.awt.event.FocusAdapter() {
   public void focusLost(java.awt.event.FocusEvent evt) {
    txt_perFocusLost(evt);
   }
  });

  jLabel21.setText("Type");

  jLabel22.setText("Total Amount");

  txt_total.addCaretListener(new javax.swing.event.CaretListener() {
   public void caretUpdate(javax.swing.event.CaretEvent evt) {
    txt_totalCaretUpdate(evt);
   }
  });

  cb_type.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Above", "Below" }));
  cb_type.addActionListener(new java.awt.event.ActionListener() {
   public void actionPerformed(java.awt.event.ActionEvent evt) {
    cb_typeActionPerformed(evt);
   }
  });

  javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
  jPanel4.setLayout(jPanel4Layout);
  jPanel4Layout.setHorizontalGroup(
   jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addGroup(jPanel4Layout.createSequentialGroup()
    .addContainerGap()
    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
     .addGroup(jPanel4Layout.createSequentialGroup()
      .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
       .addComponent(txt_rate, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
       .addComponent(jLabel19)
       .addComponent(jLabel21)
       .addComponent(cb_type, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
      .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
      .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
       .addComponent(txt_per)
       .addGroup(jPanel4Layout.createSequentialGroup()
        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addComponent(jLabel22)
         .addComponent(jLabel20))
        .addGap(0, 0, Short.MAX_VALUE))
       .addComponent(txt_total)))
     .addComponent(txt_party, javax.swing.GroupLayout.PREFERRED_SIZE, 350, javax.swing.GroupLayout.PREFERRED_SIZE)
     .addComponent(jLabel18))
    .addContainerGap())
  );
  jPanel4Layout.setVerticalGroup(
   jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addGroup(jPanel4Layout.createSequentialGroup()
    .addContainerGap()
    .addComponent(jLabel18)
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
    .addComponent(txt_party, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
     .addComponent(jLabel19)
     .addComponent(jLabel20))
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
     .addComponent(txt_rate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
     .addComponent(txt_per, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
     .addComponent(jLabel21)
     .addComponent(jLabel22))
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
     .addComponent(txt_total, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
     .addComponent(cb_type, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
  );

  javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
  jPanel1.setLayout(jPanel1Layout);
  jPanel1Layout.setHorizontalGroup(
   jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addGroup(jPanel1Layout.createSequentialGroup()
    .addContainerGap()
    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
    .addContainerGap())
  );
  jPanel1Layout.setVerticalGroup(
   jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addGroup(jPanel1Layout.createSequentialGroup()
    .addContainerGap()
    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
  );

  btn_cencel.setText("Cancel");
  btn_cencel.addActionListener(new java.awt.event.ActionListener() {
   public void actionPerformed(java.awt.event.ActionEvent evt) {
    btn_cencelActionPerformed(evt);
   }
  });

  btn_add.setText("Add");
  btn_add.addActionListener(new java.awt.event.ActionListener() {
   public void actionPerformed(java.awt.event.ActionEvent evt) {
    btn_addActionPerformed(evt);
   }
  });

  txt_reset.setText("Reset");
  txt_reset.addActionListener(new java.awt.event.ActionListener() {
   public void actionPerformed(java.awt.event.ActionEvent evt) {
    txt_resetActionPerformed(evt);
   }
  });

  javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
  getContentPane().setLayout(layout);
  layout.setHorizontalGroup(
   layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
   .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
    .addContainerGap()
    .addComponent(txt_reset)
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
    .addComponent(btn_add)
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
    .addComponent(btn_cencel)
    .addContainerGap())
  );

  layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_add, btn_cencel, txt_reset});

  layout.setVerticalGroup(
   layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
   .addGroup(layout.createSequentialGroup()
    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
     .addComponent(btn_cencel)
     .addComponent(btn_add)
     .addComponent(txt_reset))
    .addGap(6, 6, 6))
  );

  pack();
 }// </editor-fold>//GEN-END:initComponents
    
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
      dispose();
    }//GEN-LAST:event_closeDialog

    private void txt_resetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_resetActionPerformed
     onloadReset(module);
    }//GEN-LAST:event_txt_resetActionPerformed

    private void btn_cencelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cencelActionPerformed
    dispose();
    }//GEN-LAST:event_btn_cencelActionPerformed

    private void btn_addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_addActionPerformed
    String s =checkValidity();
    if(s.equals("ok")){
    String[] str=new String[6];
    str[0]=txt_party.getText();
    str[1]=txt_rate.getText();
    str[2]=txt_per.getText();
    str[3]=cb_type.getSelectedItem().toString();
    str[4]=txt_total.getText();
    str[5]=customerid;
	switch(module){
   	    case 1:
		Config.newtenderentry.insertRowParty(str);
	    break;
	    case 2:
		Config.newviewtenderentry.insertRowParty(str);
	    break;
	    case 3:
	    break;
	}
    onloadReset(module);  
    } else{
    JOptionPane.showMessageDialog(this, s, "Error", JOptionPane.ERROR_MESSAGE);
    }       
    }//GEN-LAST:event_btn_addActionPerformed

    private void txt_rateCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_rateCaretUpdate
    
    }//GEN-LAST:event_txt_rateCaretUpdate

    private void txt_totalCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_totalCaretUpdate
    
    }//GEN-LAST:event_txt_totalCaretUpdate

    private void txt_rateFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_rateFocusLost
        cal_pac() ;
    }//GEN-LAST:event_txt_rateFocusLost

    private void txt_perFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_perFocusLost
        cal_pac() ;
    }//GEN-LAST:event_txt_perFocusLost

    private void cb_typeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_typeActionPerformed
        cal_pac();
    }//GEN-LAST:event_cb_typeActionPerformed

    private void txt_partyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_partyActionPerformed
        Config.search.onloadReset(1,txt_party.getText());
        Config.search.setVisible(true);
    }//GEN-LAST:event_txt_partyActionPerformed

 private void txt_partyKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_partyKeyPressed
      customerid="";
      if(txt_party.getText().equals("")){
        if(Character.isLetter(evt.getKeyChar())){
          Config.search.onloadReset(1,String.valueOf(evt.getKeyChar()));
          Config.search.setVisible(true);
        }
      }else{
        if(Character.isLetter(evt.getKeyChar())||evt.getKeyCode()==KeyEvent.VK_BACK_SPACE &&evt.getKeyCode()!=KeyEvent.VK_BACK_SPACE ){
          Config.search.onloadReset( 1,txt_party.getText());
          Config.search.setVisible(true);
        }
      }
 }//GEN-LAST:event_txt_partyKeyPressed
  
 // Variables declaration - do not modify//GEN-BEGIN:variables
 private javax.swing.JButton btn_add;
 private javax.swing.JButton btn_cencel;
 private javax.swing.JComboBox cb_type;
 private javax.swing.JLabel jLabel18;
 private javax.swing.JLabel jLabel19;
 private javax.swing.JLabel jLabel20;
 private javax.swing.JLabel jLabel21;
 private javax.swing.JLabel jLabel22;
 private javax.swing.JPanel jPanel1;
 private javax.swing.JPanel jPanel4;
 private javax.swing.JTextField txt_party;
 private javax.swing.JTextField txt_per;
 private javax.swing.JTextField txt_rate;
 private javax.swing.JButton txt_reset;
 private javax.swing.JTextField txt_total;
 // End of variables declaration//GEN-END:variables
  
    
    public void onloadReset(int module){
    this.module =module; 
    customerid="";
    txt_party.setText("");
    txt_rate.setText("");
    txt_per.setText("");
    txt_total.setText("");
    }

    private String checkValidity() {
         if (customerid.equals("")) {            
            return "Field 'Party' can not be blank.";
        }
        else {
            return "ok";
        }
    }
    private void cal_pac() {
        Double poc=0.0;
        Double per=0.0;
        try {
            poc =Double.parseDouble(txt_rate.getText());
        } catch (Exception e) {
            poc=0.0;
        }
        try {
            per =Double.parseDouble(txt_per.getText());
        } catch (Exception e) {
            per=0.0;
        }
        
        if(cb_type.getSelectedIndex()==0){
            txt_total.setText(new DecimalFormat("#.##").format((poc+(poc*per)/100)));
        }else{
            txt_total.setText(new DecimalFormat("#.##").format((poc-(poc*per)/100)));
        }
        txt_rate.setText(new DecimalFormat("#.##").format(poc));
        txt_per.setText(new DecimalFormat("#.##").format(per));
    }

    public void setData(String n) {
        txt_party.setText(n);
	for (int i = 0; i < Config.customerprofile.size(); i++) {
	    if(Config.customerprofile.get(i).getCustomername().equals(n)){
	    customerid = Config.customerprofile.get(i).getCustomerid();
	    break;
	    }
	}
    }
    
}
