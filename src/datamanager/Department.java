/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package datamanager;

/**
 *
 * @author akshay
 */
public class Department {
    String departmentid = null;
    String departmentname = null; 
    String abbreviation = null; 

    public String getDepartmentid() {
        return departmentid;
    }

    public void setDepartmentid(String departmentid) {
        this.departmentid = departmentid;
    }

    public String getDepartmentname() {
        return departmentname;
    }

    public void setDepartmentname(String departmentname) {
        this.departmentname = departmentname;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

 
    
    
}
