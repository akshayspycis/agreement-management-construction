/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package datamanager;

import java.util.ArrayList;

/**
 *
 * @author akki
 */
public class PaymentSheetFormula {
String sd_cash ="";	
String sd_bg ="";	
String pg_cash ="";	
String pg_bg ="";	
String income_tax ="";	
String cost_of_bill ="";	
String commercial_tax ="";	
String labour_welfare ="";	
String type ="";	
String value ="";	
    ArrayList<BGDetails> bg_details = null;

    public String getSd_cash() {
        return sd_cash;
    }

    public void setSd_cash(String sd_cash) {
        this.sd_cash = sd_cash;
    }

    public String getSd_bg() {
        return sd_bg;
    }

    public void setSd_bg(String sd_bg) {
        this.sd_bg = sd_bg;
    }

    public String getPg_cash() {
        return pg_cash;
    }

    public void setPg_cash(String pg_cash) {
        this.pg_cash = pg_cash;
    }

    public String getPg_bg() {
        return pg_bg;
    }

    public void setPg_bg(String pg_bg) {
        this.pg_bg = pg_bg;
    }

    public String getIncome_tax() {
        return income_tax;
    }

    public void setIncome_tax(String income_tax) {
        this.income_tax = income_tax;
    }

    public String getCost_of_bill() {
        return cost_of_bill;
    }

    public void setCost_of_bill(String cost_of_bill) {
        this.cost_of_bill = cost_of_bill;
    }

    public String getCommercial_tax() {
        return commercial_tax;
    }

    public void setCommercial_tax(String commercial_tax) {
        this.commercial_tax = commercial_tax;
    }

    public String getLabour_welfare() {
        return labour_welfare;
    }

    public void setLabour_welfare(String labour_welfare) {
        this.labour_welfare = labour_welfare;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public ArrayList<BGDetails> getBg_details() {
        return bg_details;
    }

    public void setBg_details(ArrayList<BGDetails> bg_details) {
        this.bg_details = bg_details;
    }
		
}

