/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package datamanager;

/**
 *
 * @author akshay
 */
public class Division {
    String divisionid =null;
    String departmentid =null;
    String divisionname =null;

    public String getDivisionid() {
        return divisionid;
    }

    public void setDivisionid(String divisionid) {
        this.divisionid = divisionid;
    }

    public String getDepartmentid() {
        return departmentid;
    }

    public void setDepartmentid(String departmentid) {
        this.departmentid = departmentid;
    }

    public String getDivisionname() {
        return divisionname;
    }

    public void setDivisionname(String divisionname) {
        this.divisionname = divisionname;
    }

    
}
