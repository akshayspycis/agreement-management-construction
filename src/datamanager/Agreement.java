
package datamanager;


public class Agreement {

        String agreementid ="";
        String agreementno ="";
        String agreementdate ="";
        String workorderno ="";
        String workordernodate ="";
        String sdrs ="";
        String shapeof ="";
        String shapeofdate ="";
        String nameofbank ="";
        String period ="";
        String formoftender ="";
        String negotiation ="";
        String acceptance ="";    
        String letterno ="";
        String dt ="";
        String actualdateofworkstarted ="";
        String completionofworkdate ="";
        String stipulateddate ="";
        String timeextention1 ="";
        String timeextention2 ="";
        String payement_status ="false";

    public String getAgreementid() {
        return agreementid;
    }

    public void setAgreementid(String agreementid) {
        this.agreementid = agreementid;
    }

    public String getAgreementno() {
        return agreementno;
    }

    public void setAgreementno(String agreementno) {
        this.agreementno = agreementno;
    }

    public String getAgreementdate() {
        return agreementdate;
    }

    public void setAgreementdate(String agreementdate) {
        this.agreementdate = agreementdate;
    }

    public String getWorkorderno() {
        return workorderno;
    }

    public void setWorkorderno(String workorderno) {
        this.workorderno = workorderno;
    }

    public String getWorkordernodate() {
        return workordernodate;
    }

    public void setWorkordernodate(String workordernodate) {
        this.workordernodate = workordernodate;
    }

    public String getSdrs() {
        return sdrs;
    }

    public void setSdrs(String sdrs) {
        this.sdrs = sdrs;
    }

    public String getShapeof() {
        return shapeof;
    }

    public void setShapeof(String shapeof) {
        this.shapeof = shapeof;
    }

    public String getShapeofdate() {
        return shapeofdate;
    }

    public void setShapeofdate(String shapeofdate) {
        this.shapeofdate = shapeofdate;
    }

    public String getNameofbank() {
        return nameofbank;
    }

    public void setNameofbank(String nameofbank) {
        this.nameofbank = nameofbank;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getFormoftender() {
        return formoftender;
    }

    public void setFormoftender(String formoftender) {
        this.formoftender = formoftender;
    }

    public String getNegotiation() {
        return negotiation;
    }

    public void setNegotiation(String negotiation) {
        this.negotiation = negotiation;
    }

    public String getAcceptance() {
        return acceptance;
    }

    public void setAcceptance(String acceptance) {
        this.acceptance = acceptance;
    }

    public String getLetterno() {
        return letterno;
    }

    public void setLetterno(String letterno) {
        this.letterno = letterno;
    }

    public String getDt() {
        return dt;
    }

    public void setDt(String dt) {
        this.dt = dt;
    }

    public String getActualdateofworkstarted() {
        return actualdateofworkstarted;
    }

    public void setActualdateofworkstarted(String actualdateofworkstarted) {
        this.actualdateofworkstarted = actualdateofworkstarted;
    }

    public String getCompletionofworkdate() {
        return completionofworkdate;
    }

    public void setCompletionofworkdate(String completionofworkdate) {
        this.completionofworkdate = completionofworkdate;
    }

    public String getStipulateddate() {
        return stipulateddate;
    }

    public void setStipulateddate(String stipulateddate) {
        this.stipulateddate = stipulateddate;
    }

    public String getTimeextention1() {
        return timeextention1;
    }

    public void setTimeextention1(String timeextention1) {
        this.timeextention1 = timeextention1;
    }

    public String getTimeextention2() {
        return timeextention2;
    }

    public void setTimeextention2(String timeextention2) {
        this.timeextention2 = timeextention2;
    }

    public String getPayement_status() {
        return payement_status;
    }

    public void setPayement_status(String payement_status) {
        this.payement_status = payement_status;
    }

                                                        
}
