/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package datamanager;

/**
 *
 * @author akshay
 */
public class BGDetails {
    
    String payment_sheet_id = "";
    String bg_no = "";
    String bg_date = "";
    String name_of_bank = "";
    String amount = "";
    String valid_date = "";
    String amount_type = "";
    String agreement_id = "";

    public String getPayment_sheet_id() {
        return payment_sheet_id;
    }

    public void setPayment_sheet_id(String payment_sheet_id) {
        this.payment_sheet_id = payment_sheet_id;
    }

    public String getBg_no() {
        return bg_no;
    }

    public void setBg_no(String bg_no) {
        this.bg_no = bg_no;
    }

    public String getBg_date() {
        return bg_date;
    }

    public void setBg_date(String bg_date) {
        this.bg_date = bg_date;
    }

    public String getName_of_bank() {
        return name_of_bank;
    }

    public void setName_of_bank(String name_of_bank) {
        this.name_of_bank = name_of_bank;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getValid_date() {
        return valid_date;
    }

    public void setValid_date(String valid_date) {
        this.valid_date = valid_date;
    }

    public String getAmount_type() {
        return amount_type;
    }

    public void setAmount_type(String amount_type) {
        this.amount_type = amount_type;
    }

    public String getAgreement_id() {
        return agreement_id;
    }

    public void setAgreement_id(String agreement_id) {
        this.agreement_id = agreement_id;
    }
}

