/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package datamanager;

/**
 *
 * @author akshay
 */
public class PaymentLogFile {
 
    String log_file_id="";
    String new_payment="";
    String old_payment="";
    String payment_type="";
    String agreementid="";
    String payment_sheet_id="";
    String _date="";

    public String getLog_file_id() {
        return log_file_id;
    }

    public void setLog_file_id(String log_file_id) {
        this.log_file_id = log_file_id;
    }

    public String getNew_payment() {
        return new_payment;
    }

    public void setNew_payment(String new_payment) {
        this.new_payment = new_payment;
    }

    public String getOld_payment() {
        return old_payment;
    }

    public void setOld_payment(String old_payment) {
        this.old_payment = old_payment;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public String getAgreementid() {
        return agreementid;
    }

    public void setAgreementid(String agreementid) {
        this.agreementid = agreementid;
    }

    public String getPayment_sheet_id() {
        return payment_sheet_id;
    }

    public void setPayment_sheet_id(String payment_sheet_id) {
        this.payment_sheet_id = payment_sheet_id;
    }

    public String getDate() {
        return _date;
    }

    public void setDate(String _date) {
        this._date = _date;
    }
    
    
}
