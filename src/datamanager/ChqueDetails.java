/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package datamanager;

/**
 *
 * @author akshay
 */
public class ChqueDetails {
    String net_cheque = "";
    String cheque_no = "";
    String cheque_date = "";
    String name_of_bank= "";
    String agreementid= "";

    public String getNet_cheque() {
        return net_cheque;
    }

    public void setNet_cheque(String net_cheque) {
        this.net_cheque = net_cheque;
    }

    public String getCheque_no() {
        return cheque_no;
    }

    public void setCheque_no(String cheque_no) {
        this.cheque_no = cheque_no;
    }

    public String getCheque_date() {
        return cheque_date;
    }

    public void setCheque_date(String cheque_date) {
        this.cheque_date = cheque_date;
    }

    public String getName_of_bank() {
        return name_of_bank;
    }

    public void setName_of_bank(String name_of_bank) {
        this.name_of_bank = name_of_bank;
    }

    public String getAgreementid() {
        return agreementid;
    }

    public void setAgreementid(String agreementid) {
        this.agreementid = agreementid;
    }
    
    
    
    
}

