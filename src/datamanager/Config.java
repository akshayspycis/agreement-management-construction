package datamanager;

import admin.AdminHome;
import admin.NewDepartment;
import admin.NewDivision;
import admin.ProductConfiguration;
import admin.ViewDepartment;
import admin.ViewDivision;
import admin.bank.NewBank;
import admin.bank.ViewBank;
import admin.customer.CustomerManagement;
import admin.customer.NewCustomer;
import admin.customer.ViewCustomer;
import classmanager.AgreementMgr;
import classmanager.BGDetailsMgr;
import classmanager.BOQEntryMgr;
import classmanager.BankDetailsMgr;
import classmanager.ConfigMgr;
import classmanager.CustomerProfileMgr;
import classmanager.DepartmentMgr;
import classmanager.DivisionMgr;
import classmanager.PartyRateMgr;
import classmanager.PaymentLogFileMgr;
import classmanager.PaymentSheetFormulaMgr;
import classmanager.PaymentSheetMgr;
import classmanager.TenderentryMgr;
import classmanager.TimePeriodMgr;
import com.toedter.calendar.JDateChooser;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import modules.Report.ReportManagement;
import modules.Search.BankSearch;
import modules.Search.DepartmentSearch;
import modules.Search.DivisionSearch;
import modules.Search.Search;
import modules.Search.TimeSearch;
import modules.agreement.AgreementDetails;
import modules.agreement.AgreementManagement;
import modules.agreement.NewAgreement;
import modules.agreement.ViewAgreement;
import modules.agreement.payment.ManualPayment;
import modules.agreement.payment.NewPayment;
import modules.agreement.payment.ReturnPayment;
import modules.agreement.payment.ViewPayment;
import modules.agreement.payment.ViewReturnPayment;
import modules.agreement.payment.bg.BG;
import modules.agreement.payment.bg.BGDetailsMgmt;
import modules.tenderentry.NewBOQEntry;
import modules.tenderentry.NewPartyNameEntry;
import modules.tenderentry.NewTenderEntry;
import modules.tenderentry.ViewBOQEntry;
import modules.tenderentry.ViewPartyNameEntry;
import modules.tenderentry.newViewTenderEntry;

/**
 *
 * @author AMS
 */
public class Config {

    //database variables
    public static Connection conn = null;
    public static PreparedStatement pstmt = null;
    public static Statement stmt = null;
    public static ResultSet rs = null;
    public static String sql = null;
    public static boolean check= false;
    public static int time =2000;
    
    //product variables
    public static ArrayList<Department> configdepartment = null;
    public static ArrayList<Division> configdivision = null;
    public static ArrayList<String> configtenderentry = null;
    public static ArrayList<String> configpaymentsheet = null;
    public static ArrayList<BOQEntry> party_wise_boq = null;
    public static ArrayList<Agreement> configagreement = null;
    public static ArrayList<CustomerProfile> customerprofile = null;
    public static ArrayList<BankDetails> bank_details = null;
    public static ArrayList<PaymentSheetFormula>  payment_sheet_formula= null;
    public static ArrayList<String>  time_period= null;
    
    //system
    
    //class managers
    public static ConfigMgr configmgr = null;   
    public static DepartmentMgr departmentmgr = null;   
    public static DivisionMgr divisionmgr = null;   
    public static TenderentryMgr tenderentrymgr = null;   
    public static BOQEntryMgr boqentrymgr = null;   
    public static AgreementMgr agreementmgr = null;   
    public static PaymentSheetMgr paymentsheetmgr = null;   
    public static CustomerProfileMgr customerprofilemgr = null;
    public static PartyRateMgr partyratemgr = null;
    public static BankDetailsMgr bankdetailsmgr = null;
    public static PaymentSheetFormulaMgr payment_sheet_formula_mgr = null;
    public static TimePeriodMgr time_period_mgr = null;
    public static PaymentLogFileMgr payment_log_file_mgr = null;
    public static BGDetailsMgr bg_details_mgr = null;
    
    //forms
    public static AdminHome adminhome = null;
    public static ProductConfiguration productconfiguration = null;
    public static NewTenderEntry newtenderentry = null;
    public static NewBOQEntry newboqentry = null;
    public static ViewBOQEntry viewboqentry = null;
    public static newViewTenderEntry newviewtenderentry = null;
    public static NewDepartment newdepartment = null;
    public static ViewDepartment viewdepartment = null;
    public static NewDivision newdivision = null;
    public static ViewDivision viewdivision = null;
    public static NewAgreement newagreement = null;
    public static ViewAgreement viewagreement = null;
    public static CustomerManagement customermanagement=null;
    public static NewCustomer newcustomer=null;
    public static ViewCustomer viewcustomer =null;
    public static NewPartyNameEntry newpartynameentry =null;
    public static ViewPartyNameEntry viewpartynameentry =null;
    public static Search search =null;
    public static TimeSearch time_search =null;
    public static BankSearch bank_search =null;
    public static DepartmentSearch department_search =null;
    public static DivisionSearch division_search =null;
    public static NewBank newbank =null;
    public static ViewBank viewbank =null;
    public static NewPayment new_payment =null;
    public static ViewPayment view_payment =null;
    public static ManualPayment manual_payment =null;
    public static ReportManagement report_management =null;
    public static ReturnPayment return_payment =null;
    public static ViewReturnPayment view_return_payment =null;
    public static AgreementManagement agreement_management =null;
    public static AgreementDetails agreement_details =null;
    public static BGDetailsMgmt bg_detials_mgmt =null;
    public static BG bg =null;
    // for thread .........................................
    public static int thread_dep = 0;
    public static int thread_div = 0;
    
}

