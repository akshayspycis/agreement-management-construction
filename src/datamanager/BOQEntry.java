
package datamanager;


public class BOQEntry {
    String boqentryid =null;
    String tenderentryid =null;
    String sno =null;
    String item =null;
    String quantites =null;
    String unit =null;
    String quotedrateoftender =null;
    String amountofquotedrate =null;
    String party_id =null;

	public String getBoqentryid() {
		return boqentryid;
	}

	public void setBoqentryid(String boqentryid) {
		this.boqentryid = boqentryid;
	}

	public String getTenderentryid() {
		return tenderentryid;
	}

	public void setTenderentryid(String tenderentryid) {
		this.tenderentryid = tenderentryid;
	}

	public String getSno() {
		return sno;
	}

	public void setSno(String sno) {
		this.sno = sno;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public String getQuantites() {
		return quantites;
	}

	public void setQuantites(String quantites) {
		this.quantites = quantites;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getQuotedrateoftender() {
		return quotedrateoftender;
	}

	public void setQuotedrateoftender(String quotedrateoftender) {
		this.quotedrateoftender = quotedrateoftender;
	}

	public String getAmountofquotedrate() {
		return amountofquotedrate;
	}

	public void setAmountofquotedrate(String amountofquotedrate) {
		this.amountofquotedrate = amountofquotedrate;
	}

	public String getParty_id() {
		return party_id;
	}

	public void setParty_id(String party_id) {
		this.party_id = party_id;
	}
    

       
    
}
