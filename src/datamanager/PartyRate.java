/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package datamanager;

/**
 *
 * @author akki
 */
public class PartyRate {
    
String party_rate_id = ""    ;
String customerid = ""    ;
String rate = ""    ;
String per = ""    ;
String type = ""    ;
String total = ""    ;
String tenderentryid = ""    ;

  public String getParty_rate_id() {
    return party_rate_id;
  }

  public void setParty_rate_id(String party_rate_id) {
    this.party_rate_id = party_rate_id;
  }

  public String getCustomerid() {
    return customerid;
  }

  public void setCustomerid(String customerid) {
    this.customerid = customerid;
  }

  public String getRate() {
    return rate;
  }

  public void setRate(String rate) {
    this.rate = rate;
  }

  public String getPer() {
    return per;
  }

  public void setPer(String per) {
    this.per = per;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getTotal() {
    return total;
  }

  public void setTotal(String total) {
    this.total = total;
  }

  public String getTenderentryid() {
    return tenderentryid;
  }

  public void setTenderentryid(String tenderentryid) {
    this.tenderentryid = tenderentryid;
  }
    
}
