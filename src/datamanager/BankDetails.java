/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package datamanager;

/**
 *
 * @author akki
 */
public class BankDetails {
	String bankdetails_id = "";
	String bank_name = "";
	String address = "";
	String locality = "";
	String city = "";
	String pincode = "";
	String state = "";

	public String getBankdetails_id() {
		return bankdetails_id;
	}

	public void setBankdetails_id(String bankdetails_id) {
		this.bankdetails_id = bankdetails_id;
	}

	public String getBank_name() {
		return bank_name;
	}

	public void setBank_name(String bank_name) {
		this.bank_name = bank_name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLocality() {
		return locality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

			
}
