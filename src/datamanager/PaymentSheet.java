/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package datamanager;

import java.util.ArrayList;


public class PaymentSheet {

    String payment_sheet_id=null;
    String running_bills=null;
    String _date=null;
    String work_done=null;
    String sd_cash=null;
    String sd_bg=null;
    String pg_cash=null;
    String pg_bg=null;
    String income_tax=null;
    String cost_of_bill=null;
    String commercial_tax=null;
    String labour_welfare=null;
    String royalty_deduction=null;
    String advance_deduction=null;
    String time_deduction=null;
    String time_extention=null;
    String withheld_dedution=null;
    String net_cheque=null;
    String cheque_no=null;
    String cheque_date=null;
    String name_of_bank=null;
    String remarks=null;
    String status=null;
    String agreementid=null;
    
    ArrayList<BGDetails> bg =null;

    public String getPayment_sheet_id() {
        return payment_sheet_id;
    }

    public void setPayment_sheet_id(String payment_sheet_id) {
        this.payment_sheet_id = payment_sheet_id;
    }

    public String getRunning_bills() {
        return running_bills;
    }

    public void setRunning_bills(String running_bills) {
        this.running_bills = running_bills;
    }

    public String getDate() {
        return _date;
    }

    public void setDate(String _date) {
        this._date = _date;
    }

    public String getWork_done() {
        return work_done;
    }

    public void setWork_done(String work_done) {
        this.work_done = work_done;
    }

    public String getSd_cash() {
        return sd_cash;
    }

    public void setSd_cash(String sd_cash) {
        this.sd_cash = sd_cash;
    }

    public String getSd_bg() {
        return sd_bg;
    }

    public void setSd_bg(String sd_bg) {
        this.sd_bg = sd_bg;
    }

    public String getPg_cash() {
        return pg_cash;
    }

    public void setPg_cash(String pg_cash) {
        this.pg_cash = pg_cash;
    }

    public String getPg_bg() {
        return pg_bg;
    }

    public void setPg_bg(String pg_bg) {
        this.pg_bg = pg_bg;
    }

    public String getIncome_tax() {
        return income_tax;
    }

    public void setIncome_tax(String income_tax) {
        this.income_tax = income_tax;
    }

    public String getCost_of_bill() {
        return cost_of_bill;
    }

    public void setCost_of_bill(String cost_of_bill) {
        this.cost_of_bill = cost_of_bill;
    }

    public String getCommercial_tax() {
        return commercial_tax;
    }

    public void setCommercial_tax(String commercial_tax) {
        this.commercial_tax = commercial_tax;
    }

    public String getLabour_welfare() {
        return labour_welfare;
    }

    public void setLabour_welfare(String labour_welfare) {
        this.labour_welfare = labour_welfare;
    }

    public String getRoyalty_deduction() {
        return royalty_deduction;
    }

    public void setRoyalty_deduction(String royalty_deduction) {
        this.royalty_deduction = royalty_deduction;
    }

    public String getAdvance_deduction() {
        return advance_deduction;
    }

    public void setAdvance_deduction(String advance_deduction) {
        this.advance_deduction = advance_deduction;
    }

    public String getTime_deduction() {
        return time_deduction;
    }

    public void setTime_deduction(String time_deduction) {
        this.time_deduction = time_deduction;
    }

    public String getTime_extention() {
        return time_extention;
    }

    public void setTime_extention(String time_extention) {
        this.time_extention = time_extention;
    }

    public String getWithheld_dedution() {
        return withheld_dedution;
    }

    public void setWithheld_dedution(String withheld_dedution) {
        this.withheld_dedution = withheld_dedution;
    }

    public String getNet_cheque() {
        return net_cheque;
    }

    public void setNet_cheque(String net_cheque) {
        this.net_cheque = net_cheque;
    }

    public String getCheque_no() {
        return cheque_no;
    }

    public void setCheque_no(String cheque_no) {
        this.cheque_no = cheque_no;
    }

    public String getCheque_date() {
        return cheque_date;
    }

    public void setCheque_date(String cheque_date) {
        this.cheque_date = cheque_date;
    }

    public String getName_of_bank() {
        return name_of_bank;
    }

    public void setName_of_bank(String name_of_bank) {
        this.name_of_bank = name_of_bank;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAgreementid() {
        return agreementid;
    }

    public void setAgreementid(String agreementid) {
        this.agreementid = agreementid;
    }

    public ArrayList<BGDetails> getBg() {
        return bg;
    }

    public void setBg(ArrayList<BGDetails> bg) {
        this.bg = bg;
    }
    

    
             
}
