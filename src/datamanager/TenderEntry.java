
package datamanager;


public class TenderEntry {
 
    String tenderentryid =null;
    String departmentid =null;
    String divisionid =null;
    String contactno =null;
    String nitno =null;
    String nitdate =null;
    String issuedby =null;
    String pac =null;
    String totalamount =null;
    String emrs =null;
    String eminshapeof =null;
    String emno =null;
    String emdate =null;
    String embankname =null;
    String emperiod =null;
    String timealloted =null;
    String purchaseoftender =null;
    String submissionoftender =null;
    String openingoftender =null;
    String nameofwork =null;
    String percentage =null;
    String tenderdate =null;
    String contactno_r =null;
    String rs =null;
    String status =null;

    public String getTenderentryid() {
        return tenderentryid;
    }

    public void setTenderentryid(String tenderentryid) {
        this.tenderentryid = tenderentryid;
    }

    public String getDepartmentid() {
        return departmentid;
    }

    public void setDepartmentid(String departmentid) {
        this.departmentid = departmentid;
    }

    public String getDivisionid() {
        return divisionid;
    }

    public void setDivisionid(String divisionid) {
        this.divisionid = divisionid;
    }

    public String getContactno() {
        return contactno;
    }

    public void setContactno(String contactno) {
        this.contactno = contactno;
    }

    public String getNitno() {
        return nitno;
    }

    public void setNitno(String nitno) {
        this.nitno = nitno;
    }

    public String getNitdate() {
        return nitdate;
    }

    public void setNitdate(String nitdate) {
        this.nitdate = nitdate;
    }

    public String getIssuedby() {
        return issuedby;
    }

    public void setIssuedby(String issuedby) {
        this.issuedby = issuedby;
    }

    public String getPac() {
        return pac;
    }

    public void setPac(String pac) {
        this.pac = pac;
    }

    public String getTotalamount() {
        return totalamount;
    }

    public void setTotalamount(String totalamount) {
        this.totalamount = totalamount;
    }

    public String getEmrs() {
        return emrs;
    }

    public void setEmrs(String emrs) {
        this.emrs = emrs;
    }

    public String getEminshapeof() {
        return eminshapeof;
    }

    public void setEminshapeof(String eminshapeof) {
        this.eminshapeof = eminshapeof;
    }

    public String getEmno() {
        return emno;
    }

    public void setEmno(String emno) {
        this.emno = emno;
    }

    public String getEmdate() {
        return emdate;
    }

    public void setEmdate(String emdate) {
        this.emdate = emdate;
    }

    public String getEmbankname() {
        return embankname;
    }

    public void setEmbankname(String embankname) {
        this.embankname = embankname;
    }

    public String getEmperiod() {
        return emperiod;
    }

    public void setEmperiod(String emperiod) {
        this.emperiod = emperiod;
    }

    public String getTimealloted() {
        return timealloted;
    }

    public void setTimealloted(String timealloted) {
        this.timealloted = timealloted;
    }

    public String getPurchaseoftender() {
        return purchaseoftender;
    }

    public void setPurchaseoftender(String purchaseoftender) {
        this.purchaseoftender = purchaseoftender;
    }

    public String getSubmissionoftender() {
        return submissionoftender;
    }

    public void setSubmissionoftender(String submissionoftender) {
        this.submissionoftender = submissionoftender;
    }

    public String getOpeningoftender() {
        return openingoftender;
    }

    public void setOpeningoftender(String openingoftender) {
        this.openingoftender = openingoftender;
    }

    public String getNameofwork() {
        return nameofwork;
    }

    public void setNameofwork(String nameofwork) {
        this.nameofwork = nameofwork;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getTenderdate() {
        return tenderdate;
    }

    public void setTenderdate(String tenderdate) {
        this.tenderdate = tenderdate;
    }

    public String getContactno_r() {
        return contactno_r;
    }

    public void setContactno_r(String contactno_r) {
        this.contactno_r = contactno_r;
    }

    public String getRs() {
        return rs;
    }

    public void setRs(String rs) {
        this.rs = rs;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

       
}
