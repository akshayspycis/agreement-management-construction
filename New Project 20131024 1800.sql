-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.0.13-rc-nt


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema shapersconstruction1
--

CREATE DATABASE IF NOT EXISTS shapersconstruction1;
USE shapersconstruction1;

--
-- Definition of table `agreement`
--

DROP TABLE IF EXISTS `agreement`;
CREATE TABLE `agreement` (
  `agreementid` int(10) unsigned NOT NULL auto_increment,
  `agreementno` varchar(200) default NULL,
  `agreementdate` varchar(200) default NULL,
  `workorderno` varchar(200) default NULL,
  `workordernodate` varchar(200) default NULL,
  `sdrs` varchar(200) default NULL,
  `shapeof` varchar(200) default NULL,
  `shapeofdate` varchar(200) default NULL,
  `nameofbank` varchar(200) default NULL,
  `period` varchar(200) default NULL,
  `formoftender` varchar(200) default NULL,
  `negotiation` varchar(200) default NULL,
  `acceptance` varchar(200) default NULL,
  `letterno` varchar(200) default NULL,
  `dt` varchar(200) default NULL,
  `actualdateofworkstarted` varchar(200) default NULL,
  `completionofworkdate` varchar(200) default NULL,
  `stipulateddate` varchar(200) default NULL,
  `timeextention1` varchar(200) default NULL,
  `timeextention2` varchar(200) default NULL,
  PRIMARY KEY  (`agreementid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `agreement`
--

/*!40000 ALTER TABLE `agreement` DISABLE KEYS */;
INSERT INTO `agreement` (`agreementid`,`agreementno`,`agreementdate`,`workorderno`,`workordernodate`,`sdrs`,`shapeof`,`shapeofdate`,`nameofbank`,`period`,`formoftender`,`negotiation`,`acceptance`,`letterno`,`dt`,`actualdateofworkstarted`,`completionofworkdate`,`stipulateddate`,`timeextention1`,`timeextention2`) VALUES 
 (1,'420','31-10-2013','52','25-10-2013','500','552','26-10-2013','sbi','050','ssasd','sdfg','SDF','555255','rf','04-10-2013','10-10-2013','16-10-2013','5','5'),
 (2,'786','16-10-2013','4fghfdgh','24-10-2013','5ffdgh','0fdghdfh','24-10-2013','sboihfgh','5hdfhh','fghfhf05','0hgfg','5','0','50','09-10-2013','10-10-2013','11-10-2013','5','0'),
 (3,'5','18-10-2013','5','11-10-2013','5','5','11-10-2013','5','5','5','5','56','5','5','04-10-2013','04-10-2013','23-10-2013','5','5');
/*!40000 ALTER TABLE `agreement` ENABLE KEYS */;


--
-- Definition of table `boqentry`
--

DROP TABLE IF EXISTS `boqentry`;
CREATE TABLE `boqentry` (
  `boqentryid` int(10) unsigned NOT NULL auto_increment,
  `tenderentryid` varchar(200) default NULL,
  `sno` varchar(200) default NULL,
  `item` varchar(200) default NULL,
  `quantites` varchar(200) default NULL,
  `unit` varchar(200) default NULL,
  `rateofsqr` varchar(200) default NULL,
  `quotedrateoftender` varchar(200) default NULL,
  `amountofsor` varchar(200) default NULL,
  `amountofquotedrate` varchar(200) default NULL,
  PRIMARY KEY  (`boqentryid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `boqentry`
--

/*!40000 ALTER TABLE `boqentry` DISABLE KEYS */;
INSERT INTO `boqentry` (`boqentryid`,`tenderentryid`,`sno`,`item`,`quantites`,`unit`,`rateofsqr`,`quotedrateoftender`,`amountofsor`,`amountofquotedrate`) VALUES 
 (23,'16','1','23','4','5','6','5','24.0','20.0'),
 (24,'17','1','5','45','65','455','444','20475.0','19980.0'),
 (25,'17','1','5','665','556','482.06','5.69','320569.9','3783.85'),
 (26,'18','1','aj','5','6','5','6','25.0','30.0'),
 (27,'18','2','kk','8','9','89','88','712.0','704.0'),
 (28,'20','1','dasd','5','5','55','56','275.0','280.0'),
 (29,'21','1','121','12','10','121','121','1452.0','1452.0'),
 (30,'22','1','121','12','10','121','121','1452.0','1452.0'),
 (31,'23','1','121','12','10','121','121','1452.0','1452.0'),
 (69,'6','1','2','2','2','2','2','4.0','4.0'),
 (84,'4','1','22','223','23','223','2323','49729.0','518029.0'),
 (85,'4','2','3','2','6','26','29','52.0','58.0'),
 (90,'5','1','2','2','2','2','2','4.0','4.0'),
 (91,'5','2','2','2','2','2','22','4.0','44.0'),
 (92,'7','1','2','3','1','3','1','9.0','3.0'),
 (93,'7','5','6','5','6','5','6','25.0','30.0'),
 (100,'8','1','2','3','4','5','5','15.0','15.0'),
 (101,'8','2','1','2','22','2','22','4.0','44.0'),
 (104,'11','1','2','3','3','3','3','9.0','9.0'),
 (105,'10','1','2','3','3','3','35','9.0','105.0'),
 (106,'10','1','2','3','555','56','566','168.0','1698.0'),
 (107,'12','1','2','3','3','3','3','9.0','9.0'),
 (108,'12','2','bod','5','5','45','65','225.0','325.0'),
 (109,'12','3','3','3','3','3','3','9.0','9.0'),
 (110,'12','4','5','5','5','5','5','25.0','25.0'),
 (111,'1','1','2','3','5','+6','4','18.0','12.0'),
 (112,'1','2','3','2','3','2','3','4.0','6.0'),
 (116,'2','1','2','3','2','3','1','9.0','3.0'),
 (117,'2','5','6','5','6','5','66','25.0','330.0'),
 (118,'2','dkj','klk','505','50','505','550','255025.0','277750.0');
/*!40000 ALTER TABLE `boqentry` ENABLE KEYS */;


--
-- Definition of table `department`
--

DROP TABLE IF EXISTS `department`;
CREATE TABLE `department` (
  `departmentid` int(10) unsigned NOT NULL auto_increment,
  `departmentname` varchar(200) character set utf8 collate utf8_bin default NULL,
  `abbreviation` varchar(200) character set utf8 collate utf8_bin default NULL,
  PRIMARY KEY  (`departmentid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `department`
--

/*!40000 ALTER TABLE `department` DISABLE KEYS */;
INSERT INTO `department` (`departmentid`,`departmentname`,`abbreviation`) VALUES 
 (261,0x484A4B4C48484A4A4A,0x48),
 (262,0x484A4A4A204A4A4A4A4A,0x484A),
 (263,0x4A4A4A204A4A4A204A4A61,0x4A4A4A283229),
 (264,0x4A4A204A4A204A4A,0x4A4A4A283129),
 (265,0x4A4A4B206B,0x4A4B283129),
 (266,0x4B20,0x4B),
 (267,0x4B204B,0x4B4B),
 (268,0x4B4B204B4B61,0x4B4B283229),
 (269,0x4B4B494949,0x4B283129),
 (270,0x4B4B4B49494949494C4C4C4C4C,0x4B283229),
 (271,0x4B4B494B494B494B49,0x4B283329),
 (272,0x414B53484159,0x41),
 (273,0x4141616173732073,0x4153),
 (275,0x4120414B414B,0x41283229),
 (277,0x4B4B4B4B204B204B44,0x4B4B4B283129),
 (278,0x41424F555420424C4F424B204A494E20484949204A4948494A204E4E4A4E4A4E204E4A20,0x41424A484A4E4E),
 (279,0x414B41484159204A494E,0x414A),
 (280,0x414B41484159204A494E4249474745535420,0x414A283129),
 (281,0x4245434155534520484F5720,0x4248),
 (282,0x4E4E204E204E4B4A494A494A204B4A,0x4E4E4E4B),
 (283,0x414B53484159204A41494E2042494C414E49,0x414A42),
 (284,0x4B414D414C412042484149204B49204A414920484F,0x4B424B4A48);
/*!40000 ALTER TABLE `department` ENABLE KEYS */;


--
-- Definition of table `division`
--

DROP TABLE IF EXISTS `division`;
CREATE TABLE `division` (
  `divisionid` int(10) unsigned NOT NULL auto_increment,
  `departmentid` varchar(200) default NULL,
  `divisionname` varchar(200) default NULL,
  PRIMARY KEY  (`divisionid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `division`
--

/*!40000 ALTER TABLE `division` DISABLE KEYS */;
INSERT INTO `division` (`divisionid`,`departmentid`,`divisionname`) VALUES 
 (55,'261','sdfd'),
 (58,'261','ka'),
 (59,'261','akkksdfs sdfsdf sdf'),
 (60,'261','asaaaaaaaaaa zdfsdf dsfgdf'),
 (61,'284','akshay'),
 (62,'284','jaina'),
 (63,'284','kahsta');
/*!40000 ALTER TABLE `division` ENABLE KEYS */;


--
-- Definition of table `supplierprofile`
--

DROP TABLE IF EXISTS `supplierprofile`;
CREATE TABLE `supplierprofile` (
  `supplierid` int(10) unsigned NOT NULL auto_increment,
  `suppliername` varchar(200) default NULL,
  `gender` varchar(200) default NULL,
  `contactno` varchar(200) default NULL,
  `organization` varchar(200) default NULL,
  `tinno` varchar(200) default NULL,
  `address` varchar(200) default NULL,
  `locality` varchar(200) default NULL,
  `city` varchar(200) default NULL,
  `pincode` varchar(200) default NULL,
  `state` varchar(200) default NULL,
  `email` varchar(200) default NULL,
  `fax` varchar(200) default NULL,
  `website` varchar(200) default NULL,
  `other` varchar(200) default NULL,
  PRIMARY KEY  (`supplierid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supplierprofile`
--

/*!40000 ALTER TABLE `supplierprofile` DISABLE KEYS */;
/*!40000 ALTER TABLE `supplierprofile` ENABLE KEYS */;


--
-- Definition of table `tenderentry`
--

DROP TABLE IF EXISTS `tenderentry`;
CREATE TABLE `tenderentry` (
  `tenderentryid` int(10) unsigned NOT NULL auto_increment,
  `departmentid` varchar(200) default NULL,
  `divisionid` varchar(200) default NULL,
  `contactno` varchar(200) default NULL,
  `nitno` varchar(200) default NULL,
  `nitdate` varchar(200) default NULL,
  `issuedby` varchar(200) default NULL,
  `pac` varchar(200) default NULL,
  `totalamount` varchar(200) default NULL,
  `emrs` varchar(200) default NULL,
  `eminshapeof` varchar(200) default NULL,
  `emno` varchar(200) default NULL,
  `emdate` varchar(200) default NULL,
  `embankname` varchar(200) default NULL,
  `emperiod` varchar(200) default NULL,
  `purchaseoftender` varchar(200) default NULL,
  `submissionoftender` varchar(200) default NULL,
  `openingoftender` varchar(200) default NULL,
  `nameofwork` varchar(200) default NULL,
  `timealloted` varchar(200) default NULL,
  `percentage` varchar(200) default NULL,
  `tenderdate` varchar(200) default NULL,
  `contactno_r` varchar(200) default NULL,
  `rs` varchar(200) default NULL,
  PRIMARY KEY  USING BTREE (`tenderentryid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tenderentry`
--

/*!40000 ALTER TABLE `tenderentry` DISABLE KEYS */;
INSERT INTO `tenderentry` (`tenderentryid`,`departmentid`,`divisionid`,`contactno`,`nitno`,`nitdate`,`issuedby`,`pac`,`totalamount`,`emrs`,`eminshapeof`,`emno`,`emdate`,`embankname`,`emperiod`,`purchaseoftender`,`submissionoftender`,`openingoftender`,`nameofwork`,`timealloted`,`percentage`,`tenderdate`,`contactno_r`,`rs`) VALUES 
 (1,'261','55','8817919016','52','25-10-2013','akshay jain','50','500','45','akshay','25','17-10-2013','sbi','1 month','19-10-2013','19-10-2013','18-10-2013','bilani bedi','50','50','','8817919016','EX'),
 (2,'261','58','8888888888','50','12-10-2013','akshay jain','8','500','500','akshy','520','11-10-2013','sbi','5','11-10-2013','11-10-2013','17-10-2013','bilani','55','8','24-10-2013','8817919016','EX'),
 (3,'284','63','8817919016','4780','12-10-2013','545','50','500','5000','akshay bilani','50420','17-10-2013','state bank of india','500','25-10-2013','19-10-2013','17-10-2013','tera mera sath ','1 month','50','24-10-2013','88179190166','EX');
/*!40000 ALTER TABLE `tenderentry` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
